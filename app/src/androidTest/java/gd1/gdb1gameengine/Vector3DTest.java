package gd1.gdb1gameengine;

import android.test.*;
import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.Assert;

import java.util.Vector;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineMainActivity;
import gd1.gdb1gameengine.GameObjects.Vector3D;

/**
 * Created by Shaun on 25/10/2016.
 */

public class Vector3DTest extends ActivityInstrumentationTestCase2<GameEngineMainActivity>
{


    public Vector3DTest()
    {
        super(GameEngineMainActivity.class);
    }

    @SmallTest
    public void lengthIsCorrect() throws Exception
    {
        float coordX = 5.0f;
        float coordY = 5.0f;
        float coordZ = 5.0f;
        Vector3D vector = new Vector3D(coordX,coordY,coordZ);

        Assert.assertEquals(11.18f,vector.length(),0.01f);

    }

    @SmallTest
    public void normaliseIsCorrect() throws Exception
    {
        float coordX = 2.0f;
        float coordY = 2.0f;
        float coordZ = 2.0f;
        Vector3D vector = new Vector3D(coordX,coordY,coordZ);
        vector.normalize();
        Assert.assertEquals(1,vector.length());
    }

    @SmallTest
    public void setIsCorrect() throws Exception
    {
        float coordX = 2.0f;
        float coordY = 2.0f;
        float coordZ = 2.0f;
        Vector3D vector = new Vector3D(coordX,coordY,coordZ);

        vector.set(4,4,4);

        Assert.assertEquals(4.0f,vector.x);
        Assert.assertEquals(4.0f,vector.y);
        Assert.assertEquals(4.0f,vector.z);

    }
    @SmallTest
    public void setWithVectorIsCorrect() throws Exception
    {
        float coordX = 2.0f;
        float coordY = 2.0f;
        float coordZ = 2.0f;
        Vector3D vector = new Vector3D(coordX,coordY,coordZ);
        Vector3D vector2 = new Vector3D(4,4,4);

        vector.set(vector2);

        Assert.assertEquals(4.0f,vector.x);
        Assert.assertEquals(4.0f,vector.y);
        Assert.assertEquals(4.0f,vector.z);

    }

    @SmallTest
    public void multiplyIsCorrect() throws Exception
    {
        float coordX = 2.0f;
        float coordY = 2.0f;
        float coordZ = 2.0f;
        Vector3D vector = new Vector3D(coordX,coordY,coordZ);

        vector.multiply(2);

        Assert.assertEquals(4.0f,vector.x);
        Assert.assertEquals(4.0f,vector.y);
        Assert.assertEquals(4.0f,vector.z);

    }

    @SmallTest
    public void negateIsCorrect() throws Exception
    {
        float coordX = 2.0f;
        float coordY = 2.0f;
        float coordZ = 2.0f;
        Vector3D vector = new Vector3D(coordX,coordY,coordZ);

        vector.negate();


        Assert.assertEquals(-2.0f,vector.x);
        Assert.assertEquals(-2.0f,vector.y);
        Assert.assertEquals(-2.0f,vector.z);
    }


    @SmallTest
    public void dotProductIsCorrect() throws Exception
    {
        float coordX = 2.0f;
        float coordY = 2.0f;
        float coordZ = 2.0f;
        Vector3D vector = new Vector3D(coordX,coordY,coordZ);

        Assert.assertEquals(12,vector.dotProduct(vector));


    }

    @SmallTest
    public void crossProductIsCorrect() throws Exception
    {
        float coordX = 2.0f;
        float coordY = 2.0f;
        float coordZ = 2.0f;
        Vector3D vector = new Vector3D(coordX,coordY,coordZ);

        vector.crossProduct(vector);

        Assert.assertEquals(0,vector.x);
        Assert.assertEquals(0,vector.y);
        Assert.assertEquals(0,vector.z);
    }

    @SmallTest
    public void addIsCorrect() throws Exception
    {
        float coordX = 2.0f;
        float coordY = 2.0f;
        float coordZ = 2.0f;
        Vector3D vector = new Vector3D(coordX,coordY,coordZ);

        vector=Vector3D.add(vector,vector);

        Assert.assertEquals(4.0f,vector.x);
        Assert.assertEquals(4.0f,vector.y);
        Assert.assertEquals(4.0f,vector.z);

    }


}
