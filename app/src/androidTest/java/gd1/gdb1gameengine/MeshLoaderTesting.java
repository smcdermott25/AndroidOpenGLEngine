package gd1.gdb1gameengine;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineMainActivity;
import gd1.gdb1gameengine.GameObjects.Mesh;
import gd1.gdb1gameengine.Graphics.MeshLoader;


public class MeshLoaderTesting extends ActivityInstrumentationTestCase2<GameEngineMainActivity>
{
    private MeshLoader loader = new MeshLoader();

    public MeshLoaderTesting()
    {
        super(GameEngineMainActivity.class);
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    @SmallTest
    public void testMeshNotNull()throws Exception
    {
        Mesh testMesh = loader.parseObj(getActivity().getBaseContext(),R.raw.cube);
        assertNotNull(testMesh);
    }

    @SmallTest
    public void testVerticeLengthCube()throws Exception
    {
        Mesh testMesh = loader.parseObj(getActivity().getBaseContext(),R.raw.cube);
         //   assertNotNull(testMesh);
        assertEquals(108,testMesh.mVertices.length);

    }
    @SmallTest
    public void testVerticeLengthBear()throws Exception
    {
        Mesh testMesh = loader.parseObj(getActivity().getBaseContext(),R.raw.bear);

        assertEquals(11736,testMesh.mVertices.length);

    }
    @SmallTest
    public void testVerticefinalVerticeBear()throws Exception
    {
        Mesh testMesh = loader.parseObj(getActivity().getBaseContext(),R.raw.bear);
        assertEquals(7.524788f,testMesh.mVertices[testMesh.mVertices.length-1]);

    }
    @SmallTest
    public void testLengthOfArray()throws Exception
    {
        Mesh testMesh = loader.parseObj(getActivity().getBaseContext(),R.raw.testfileforparser);
        //assertEquals(0.1f,testMesh.mTextures[0]);

       // Vector3D test = new Vector3D(0.1f,0.2f,0f);
        assertEquals(72,testMesh.mTextures.length);
       // assertEquals(0.1f,loader.parser.mTextures.get(1).y);
       //  assertEquals(0.3f,testMesh.mTextures[2]);
       // assertEquals(0.0001f,testMesh.mTextures[2]);
       // assertEquals(0.6654f,testMesh.mTextures[10]);

    }

    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }


}
