package gd1.gdb1gameengine;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineMainActivity;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Vector3D;

/**
 * Created by rjlfi on 27/10/2016.
 */

public class LightingTest extends ActivityInstrumentationTestCase2<GameEngineMainActivity>
{

    private GameEngineMainActivity gameActivity;
    private Vector3D lightPosition = new Vector3D(0.8f, 13f, -3f);
    private Vector3D lightColor = new Vector3D(0.5f, 0.5f, 0.5f);

    public LightingTest()
    {
        super(GameEngineMainActivity.class);
    }


    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        gameActivity = getActivity();

    }

    @SmallTest
    public void testLightCreation() throws Exception
    {
        LightSource light = new LightSource(lightColor);
        assertNotNull(light);

    }

    @SmallTest
    public void testLightColour() throws Exception
    {
        LightSource light = new LightSource(lightColor);
        assertEquals(light.getLightColor().x, lightColor.x);
        assertEquals(light.getLightColor().y, lightColor.y);
        assertEquals(light.getLightColor().y, lightColor.y);

    }

    @SmallTest
    public void testLightPosition() throws Exception
    {
        LightSource light = new LightSource(lightColor);
        light.setLightPosition(lightPosition);
        assertEquals(light.getLightPosition().x, lightPosition.x);
        assertEquals(light.getLightPosition().y, lightPosition.y);
        assertEquals(light.getLightPosition().z, lightPosition.z);

    }

    @SmallTest
    public void testSettingsColor() throws Exception
    {
        LightSource light = new LightSource(lightColor);
        light.setLightColor(new Vector3D(0.1f, 0.9f, 0.2f));
        assertEquals(light.getLightColor().x, 0.1f);
        assertEquals(light.getLightColor().y, 0.9f);
        assertEquals(light.getLightColor().z, 0.2f);
    }

    @SmallTest
    public void testLightAmbientSetting() throws Exception
    {
        LightSource light = new LightSource(lightColor);
        assertEquals(0.1f, light.getAmbientLightLevel());
    }


    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }


}
