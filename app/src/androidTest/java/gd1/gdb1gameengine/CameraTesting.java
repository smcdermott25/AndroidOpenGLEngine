package gd1.gdb1gameengine;

import android.opengl.Matrix;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineMainActivity;
import gd1.gdb1gameengine.GameObjects.Camera;
import gd1.gdb1gameengine.GameObjects.Vector3D;

/**
 * Created by rjlfi on 27/10/2016.
 */

public class CameraTesting extends ActivityInstrumentationTestCase2<GameEngineMainActivity>
{
    private Vector3D cameraPosition = new Vector3D(0, 0, -5);
    private Vector3D cameraLook = new Vector3D(0, 0, 0);
    private Vector3D cameraUp = new Vector3D(0, 1, 0);

    private int ratio = 4 / 3;
    private int nearClipping = 1;
    private int farClipping = 100;

    private boolean perspective = true;
    private boolean orthographic = false;

    public CameraTesting()
    {
        super(GameEngineMainActivity.class);
    }


    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    @SmallTest
    public void testCameraCreationPerspective() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);
        assertNotNull(camera);
    }

    @SmallTest
    public void testCameraCreationOrthographic() throws Exception
    {
        Camera camera = new Camera(orthographic, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);
        assertNotNull(camera);
    }

    @SmallTest
    public void testProjectionMatrixIsCorrect() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        float check[] = new float[16];
        Matrix.frustumM(check, 0, -ratio, ratio, -1, 1, nearClipping,
                farClipping);
        for (int i = 0; i < 16; i++)
        {
            assertEquals(check[i], camera.getProjectionMatrix()[i], 0);
        }
    }


    @SmallTest
    public void testViewMatrixisCorrect() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        float check[] = new float[16];
        Matrix.setLookAtM(check, 0, cameraPosition.x, cameraPosition.y,
                cameraPosition.z, cameraLook.x, cameraLook.y, cameraLook.z, cameraUp.x,
                cameraUp.y, cameraUp.z);
        for (int i = 0; i < 16; i++)
        {
            assertEquals(check[i], camera.getViewMatrix()[i], 0);
        }
    }

    @SmallTest
    public void testProjectionViewMatrixIsCorrect() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        float testProjection[] = new float[16];
        Matrix.frustumM(testProjection, 0, -ratio, ratio, -1, 1, nearClipping,
                farClipping);

        float testView[] = new float[16];
        Matrix.setLookAtM(testView, 0, cameraPosition.x, cameraPosition.y,
                cameraPosition.z, cameraLook.x, cameraLook.y, cameraLook.z, cameraUp.x,
                cameraUp.y, cameraUp.z);

        float testProjectionView[] = new float[16];
        Matrix.multiplyMM(testProjectionView, 0, testProjection, 0, testView, 0);
        for (int i = 0; i < 16; i++)
        {
            assertEquals(testProjectionView[i], camera.getProjectionViewMatrix()[i], 0);
        }
    }

    @SmallTest
    public void testRotateAroundPoint() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        // Rotate around target with a radius of 10
        float radius = 10;

        // Testing that the radius remains the same after 360 rotations
        for (int i = 0; i < 360; i++)
        {
            camera.rotateAroundTarget(radius);

            // Calculate the radius of the camera to it's target squared
            float zComponent = camera.getCameraPosition().z - camera.getCameraTarget().z;
            float xComponent = camera.getCameraPosition().x - camera.getCameraTarget().x;

            float radiusTest = (zComponent * zComponent) + (xComponent * xComponent);
            assertEquals(radius * radius, radiusTest, 0.1);
        }
    }

    @SmallTest
    public void testMovingRightPosition() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        // x position before
        float xPosition = camera.getCameraPosition().x;

        // Move right once
        camera.moveRight();
        assertEquals(xPosition - 0.1f, camera.getCameraPosition().x, 0.1f);
    }

    @SmallTest
    public void testMovingRightTarget() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        // x target position before
        float xTarget = camera.getCameraTarget().x;

        // Move right once
        camera.moveRight();
        assertEquals((xTarget - 0.1f), camera.getCameraTarget().x, 0.1f);
    }

    @SmallTest
    public void testMovingLeftPosition() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        // x position before
        float xPosition = camera.getCameraPosition().x;

        // Move left once
        camera.moveLeft();
        assertEquals((xPosition + 0.1f), camera.getCameraPosition().x, 0.1f);
    }

    @SmallTest
    public void testMovingLeftTarget() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        // x target position before
        float xTarget = camera.getCameraTarget().x;

        // Move left once
        camera.moveLeft();
        assertEquals((xTarget + 0.1f), camera.getCameraTarget().x, 0.1f);
    }

    @SmallTest
    public void testMovingUpPosition() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        // y position before
        float yPosition = camera.getCameraPosition().y;

        // Move up once
        camera.moveRight();
        assertEquals((yPosition + 0.1f), camera.getCameraPosition().y, 0.1f);
    }

    @SmallTest
    public void testMovingUpTarget() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        // y target position before
        float yTarget = camera.getCameraTarget().y;

        // Move up once
        camera.moveRight();
        assertEquals((yTarget + 0.1f), camera.getCameraTarget().y, 0.1f);
    }

    @SmallTest
    public void testMovingDownPosition() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        // y position position before
        float yPosition = camera.getCameraPosition().y;

        // Move down once
        camera.moveDown();
        assertEquals((yPosition - 0.1), camera.getCameraPosition().y, 0.1f);
    }

    @SmallTest
    public void testMovingDownTarget() throws Exception
    {
        Camera camera = new Camera(perspective, cameraPosition, cameraLook,
                cameraUp, ratio, nearClipping, farClipping);

        // y target position before
        float yTarget = camera.getCameraTarget().y;

        // Move down once
        camera.moveDown();
        assertEquals((yTarget - 0.1), camera.getCameraTarget().y, 0.1f);
    }






    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }


}
