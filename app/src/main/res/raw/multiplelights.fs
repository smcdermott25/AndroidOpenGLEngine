
precision mediump float;
struct light
{
    vec3 lightPosition;
    vec3 lightColour;
};

uniform vec3 objectColour;
uniform vec3 lightColours [4];
uniform vec3 lightPositions [4];
uniform vec3 viewPos;
uniform float ambientStrength;

varying vec3 normals;
varying vec3 fragPos;

float specularStrength = 0.5;

vec3 calculateAdditionOfLight(vec3 lightPos, vec3 lightColour)
{
    // Calculations done in world space

    // diffuse calculations
    vec3 normal = normalize(normals);
    vec3 lightDirection = normalize(lightPos - fragPos);

    float vectorDifference = max(dot(normal, lightDirection), 0.0);
    vec3 diffuse = vectorDifference * lightColour;

    vec3 viewDir = normalize(viewPos - fragPos);
    vec3 reflectDir = reflect(-lightDirection, normal);

    float specularComponent = pow(max(dot(viewDir, reflectDir), 0.0), 32.0);
    vec3  specular = specularStrength * specularComponent * lightColour;

    return (diffuse + specular);
}

void main()
{
    vec3 lightResult;

    for (int i =0; i < 4; i++)
    {
       lightResult += calculateAdditionOfLight(lightPositions[i], lightColours[i]);
    }

    gl_FragColor = vec4(lightResult * objectColour, 1.0);
}