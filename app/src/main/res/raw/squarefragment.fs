varying vec2 vTexturePosition;
uniform sampler2D texture;
precision mediump float;
void main()
{
    gl_FragColor = texture2D(texture, vTexturePosition);
}