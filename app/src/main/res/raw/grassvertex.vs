#version 300 es

uniform mat4 uMVPMatrix;
in vec4 vPosition;
in vec2 aTexturePosition;
out vec2 texturePosition2;

int random(int seed, int iterations)
{
    int value = seed;
    int i;

    // ^ is a bitwise exclusion OR operation
    // 1 ^ 1 = 0
    // 0 ^ 0 = 0
    // 0 ^ 1 = 1
    for (i = 0; i < iterations; i++)
    {
        value = ((value >> 7) ^ (value << 9)) * 1543454;
    }

    return value;
}

void main()
{
     texturePosition2 = vec2(aTexturePosition.x, 1.0f - aTexturePosition.y);

     vec4 offset = vec4(float(gl_InstanceID >> 3)  +10.0,
                        0.0f,
                        float((gl_InstanceID & 0xF) * 2) -3.0,
                        0.0f);

     gl_Position = uMVPMatrix * (vPosition + offset);
}