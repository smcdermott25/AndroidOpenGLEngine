precision mediump float;

uniform mat4 uMVPMatrix;
uniform mat4 uMMatrix;

uniform vec3 lightPos;
attribute vec3 position;
attribute vec3 normal;
attribute vec2 aTexturePosition;

varying vec2 vTexturePosition;
varying vec3 normals;
varying vec3 fragPos;


void main()
{
    // Passing position of vertex in camera space to be drawn
    gl_Position =  uMVPMatrix * vec4(position, 1.0);

    // Normals world space?
    normals = normal;

    vTexturePosition = vec2(aTexturePosition.x, 1.0-aTexturePosition.y);

    // World Space of vertex used in lighting calculation
    fragPos = vec3(uMMatrix * vec4(position, 1.0));
}