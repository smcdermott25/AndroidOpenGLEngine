precision mediump float;
uniform mat4 uMVPMatrix;
attribute vec4 vPosition;
attribute vec2 aTexturePosition;
varying vec2 vTexturePosition;
void main()
{
     vTexturePosition = vec2(aTexturePosition.x, 1.0 - aTexturePosition.y);
     gl_Position = uMVPMatrix * vPosition;
}
