
precision mediump float;
uniform vec3 objectColor;
uniform vec3 lightColor;
uniform float ambientStrength;
vec4 ambientVec4 = vec4(ambientStrength);
void main()
{
    gl_FragColor = vec4(lightColor, 1.0);
}