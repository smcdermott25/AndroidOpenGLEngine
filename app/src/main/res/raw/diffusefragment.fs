
precision mediump float;

uniform vec3 objectColor;
uniform vec3 lightColor;
uniform vec3 lightPos;
uniform mat4 lightMMatrix;
uniform vec3 viewPos;
uniform float ambientStrength;
uniform sampler2D normalMap;
uniform mat4 uNormalMatrix;
varying vec2 vTexturePosition;
uniform sampler2D texture;

varying vec3 normals;
varying vec3 fragPos;

void main()
{
   // lighting calculations done in world space

    //Normal Mapping
   vec3 normal = texture2D(normalMap, vTexturePosition).xyz;
   normal = normalize(normal * 2.0 - 1.0);

   // Use below for no normal mapping
  // vec3 normal = normalize(-normals);

    vec3 wCLightPos=vec3(lightMMatrix*vec4(lightPos,1));
   // diffuse calculations

   vec3 lightDirection = normalize(lightPos - fragPos);
    float distance =length(lightPos-fragPos);
    float vectorDifference = max(dot(normal, lightDirection), 0.0);
    vec4 color = texture2D(texture,vTexturePosition);
    vec3 diffuse = vectorDifference * lightColor;
    diffuse = diffuse *(1.0/(1.0 +(0.25*distance*distance)));
    // specular calculations
    // strength will change per object
    float specularStrength = 4.0;

    vec3 viewDir = normalize(viewPos - fragPos);
    vec3 reflectDir = reflect(-lightDirection, normal);

    float specularComponent = pow(max(dot(viewDir, reflectDir), 0.0), 32.0);
    vec3  specular = specularStrength * specularComponent * lightColor;


    vec3 lightResult = vec3(color) * (ambientStrength + diffuse + specular) ;

    gl_FragColor = vec4(lightResult, 1.0);
}