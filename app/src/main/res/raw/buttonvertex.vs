attribute vec4 vPosition;
attribute vec2 aTexturePosition;
varying vec2 vTexturePosition;
uniform mat4 uMVPMatrix;

void main()
{
  vTexturePosition = vec2(aTexturePosition.x, 1.0 - aTexturePosition.y);
  gl_Position = uMVPMatrix*vPosition;
}