
uniform mat4 uMVPMatrix;
uniform mat4 modelMatrix;


attribute vec3 position;
attribute vec3 normal;

varying vec3 normals;
varying vec3 fragPos;


void main()
{
    // Passing position of vertex in camera space to be drawn
    gl_Position =  uMVPMatrix * vec4(position, 1.0);

    // Normals world space?
    vec4 temp = modelMatrix * vec4(normal, 1.0);
    normals = vec3(temp.x, temp.y, temp.z);

    // World Space of vertex used in lighting calculation
    fragPos = vec3(modelMatrix * vec4(position, 1.0));
}
