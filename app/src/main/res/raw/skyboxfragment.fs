precision mediump ​float​;
uniform samplerCube u_TextureUnit;
out vec3 v_Position;
​
​void​ main()
{
        gl_FragColor = texture(u_TextureUnit, v_Position);
}