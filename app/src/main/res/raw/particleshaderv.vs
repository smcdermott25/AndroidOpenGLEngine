precision mediump float;
attribute vec2 position;

uniform mat4 MVPMatrix;

void main(){

	gl_Position = MVPMatrix * vec4(position.x,position.y, 0.0, 1.0);

}