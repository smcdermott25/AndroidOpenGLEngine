
precision mediump float;        // Set the default precision to medium. We don't need as high of a
                                // precision in the fragment shader.
uniform vec3 uLightPos;         // The position of the light in eye space.
uniform mat4 uLightMMatrix;
uniform vec3 uCamPosition;      // The position of the camera
uniform vec3 uLightColor;
uniform float uLightShininess;
uniform vec3 uObjectColor;

uniform mat4 uViewMatrix;

uniform float uAmbientStrength;

uniform vec3 uLightAmbient;

varying vec3 vPosition;         // Interpolated position for this fragment.
varying vec3 vWcPosition;
varying vec3 vNormal;           // Interpolated normal for this fragment.

// The entry point for our fragment shader.
void main()
{

///////////Diffuse

    vec3 lightDir = vec3(uLightMMatrix*vec4(uLightPos,1));
    // Get a lighting direction vector from the light to the vertex.
    vec3 lightVector = normalize(lightDir - vPosition);

    vec3 lightNormal = normalize(vNormal);
    // Calculate the dot product of the light vector and vertex normal. If the normal and light vector are
    // pointing in the same direction then it will get max illumination.
    float vectorDifference = max(dot(lightNormal, lightVector), 0.1);

    vec3 diffuse = vectorDifference * uLightColor;


///////////Specular

    //vec3 EyePos = normalize(uCamPosition);
    //vec3 camVector = vWcPosition-EyePos;
    //vec3 lightCamVector = lightDir + camVector;
//
    //vec3 lightCamVectorView = normalize(vec3(uViewMatrix * vec4(lightCamVector,1)));
//
    //float specDifference = pow(max(dot(lightCamVectorView,vNormal), 0.0), uLightShininess);
//
    //vec3 specular = specDifference * uLightColor;

    vec3 lightResult = uObjectColor * (diffuse + uAmbientStrength);

    // Multiply the color by the diffuse illumination level to get final output color.
    gl_FragColor = vec4(lightResult,1);
}