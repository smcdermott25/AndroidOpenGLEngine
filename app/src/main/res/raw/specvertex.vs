
uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat4 uMMatrix;
uniform mat4 uNormalMatrix;

attribute vec3 aPosition;

attribute vec3 aNormal;

varying vec3 vPosition;
varying vec3 vWcPosition;
varying vec4 vColor;
varying vec3 vNormal;

void main()
{
    // Transform the vertex into world space.
    vWcPosition = vec3 (uMMatrix * (vec4(aPosition,1)));

    // Transform the vertex into eye space.
    vPosition = vec3(uMVMatrix * (vec4(aPosition,1)));

    // Transform the normal's orientation into eye space.
    vNormal = normalize(vec3(uNormalMatrix * vec4(aNormal, 1)));

    // Multiply the vertex by the matrix to get the final point in normalized screen coordinates.
    gl_Position = uMVPMatrix * vec4(aPosition,1);

}