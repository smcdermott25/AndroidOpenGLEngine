#version 300 es

in vec2 texturePosition2;
uniform sampler2D texture1;
out vec4 colour;
// precision mediump float;

void main()
{
    colour = texture(texture1,texturePosition2);
    //colour = vec4(0.3f, 1.0f, 0.3f, 1.0f);
}