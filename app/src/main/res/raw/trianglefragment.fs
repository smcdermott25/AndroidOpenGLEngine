
precision mediump float;
uniform vec3 objectColor;
uniform vec3 lightColor;
uniform float ambientStrength;

void main()
{

    vec3 lightResult = lightColor * ambientStrength ;

   gl_FragColor = vec4(lightResult * objectColor, 1.0);
}
