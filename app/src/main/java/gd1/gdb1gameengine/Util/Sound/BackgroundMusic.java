package gd1.gdb1gameengine.Util.Sound;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.content.res.AssetManager;

import java.io.IOException;

import gd1.gdb1gameengine.Util.Logging.Logger;

/**
 * Created by awoods321 on 25/11/2016.
 *
 **/

public class BackgroundMusic implements MediaPlayer.OnCompletionListener
{
    private MediaPlayer mediaPlayer;
    private String assetName = null;
    private boolean playbackReady = false;
    private AssetManager assetManager;
    //for the resume method
    private int pauseTime;
    //to log to us with more info
    private Logger log;

    public BackgroundMusic(Context context)
    {
        assetManager = context.getAssets();

    }
    public void loadMusic(String name){
        assetName = name;

        mediaPlayer = new MediaPlayer();
        try
            {
                AssetFileDescriptor desciptor = assetManager.openFd(assetName);
                mediaPlayer.setDataSource(desciptor.getFileDescriptor(),desciptor.getStartOffset(),desciptor.getLength());
                playbackReady = true;
                mediaPlayer.prepare();
                //to ensure we can loop the music
                mediaPlayer.setOnCompletionListener(this);
             }
        catch(IOException e)
        {
//            log.info("Could not play file: " + assetName);
            e.printStackTrace();
        }
    }

    public void play()
    {
        if(currentlyPlaying()==true) return;
        try
        {
            synchronized (this) {
                if (playbackReady == false) {
                    mediaPlayer.prepare();
                    playbackReady = true;
                }
                mediaPlayer.start();
            }
        }
        catch (IllegalStateException e)
        {
//            log.info("The audio file: " + assetName + " has an illegal state");
            e.printStackTrace();
        }
        catch (IOException e)
        {
            log.info("The audio file: " + assetName + " cannot be played");
            e.printStackTrace();
        }
    }

    //pause() just pauses so it doesn't change the playbackReady boolean
    //but it does need a pauseTime for when you want to resume the music
    public void pause()
    {
        if (currentlyPlaying() == true)
        {
            mediaPlayer.pause();
        }
        pauseTime = mediaPlayer.getCurrentPosition();
    }

    //resume() uses the pauseTime variable above to resume at that time
    public void resume()
    {
        if(currentlyPlaying()== false && playbackReady == true)
        {
            mediaPlayer.seekTo(pauseTime);
            mediaPlayer.start();
        }
    }

    //stop just stops the music playing and updates the boolean, it hs no need for the current time
    public void stop()
    {
        if (currentlyPlaying() == true)
        {
            mediaPlayer.stop();
        }
            synchronized (this)
            {
                playbackReady = false;
            }

    }

    public boolean currentlyPlaying()
    {
        return mediaPlayer.isPlaying();
    }

    //setting playback to false once the track has ended
    public void onCompletion(MediaPlayer player)
    {
        synchronized (this) {
            playbackReady = false;
        }
    }

    //ensures the music loops
    public void loopMusic(boolean looping)
    {
        mediaPlayer.setLooping(looping);
    }
}


