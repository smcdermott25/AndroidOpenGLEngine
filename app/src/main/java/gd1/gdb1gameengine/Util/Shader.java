package gd1.gdb1gameengine.Util;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.IntBuffer;

import gd1.gdb1gameengine.GameObjects.Vector3D;

/**
 * Created by Shaun on 25/10/2016.
 */

public class Shader {
    private Context mContext;
    private int mFragmentShader;
    private int mVertexShader;
    private int mShaderProgram;
    private String mShaderName;

    public Shader(Context context, int VSResourceId, int FSResourceId, String shaderName)
    {
        mFragmentShader = 0;
        mVertexShader = 0;
        mShaderProgram = 0;
        mContext = context;
        mShaderName=shaderName;
        initShaderProgram(VSResourceId, FSResourceId);
    }

    private void initShaderProgram(int VSResourceId, int FSResourceId) //initialises the Shader program, calls the Vertex and Fragment shader initialisation, and links the program
    {
        mShaderProgram = GLES30.glCreateProgram();
        initVertexShader(VSResourceId);
        initFragmentShader(FSResourceId);
        GLES30.glLinkProgram(mShaderProgram);
        int[] linkStatus = new int[1];
        GLES30.glGetProgramiv(mShaderProgram, GLES30.GL_LINK_STATUS, linkStatus, 0);
        if (linkStatus[0] != GLES30.GL_TRUE)
        {
            String DebugInfo = GLES30.glGetProgramInfoLog(mShaderProgram);
            GLES30.glDeleteProgram(mShaderProgram);

            // Throw exception
            Log.d("DEBUG-SHADER LINK INFO ", "Shader name is : " + mShaderName + ": " +DebugInfo);
        }
    }

    private void initVertexShader(int ResourceId) //initialises Vertex shader and attaches it to the shader program
    {
        StringBuffer tempBuffer = readInShader(ResourceId);
        mVertexShader= GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
        GLES30.glShaderSource(mVertexShader,tempBuffer.toString());
        GLES30.glCompileShader(mVertexShader);
        IntBuffer CompileErrorStatus = IntBuffer.allocate(1);
        GLES30.glGetShaderiv(mVertexShader, GLES30.GL_COMPILE_STATUS, CompileErrorStatus);

        if (CompileErrorStatus.get(0) == 0)
        {
            Log.e("ERROR - VERTEX SHADER ","Object failed to compile vertex shader: " + mShaderName);
            Log.e("ERROR - VERTEX SHADER ", "Could not compile Vertex shader!! " + String.valueOf(ResourceId));
            Log.e("ERROR - VERTEX SHADER ", GLES30.glGetShaderInfoLog(mVertexShader));
            GLES30.glDeleteShader(mVertexShader);
            mVertexShader = 0;
        }
        else
        {
            GLES30.glAttachShader(mShaderProgram,mVertexShader);
            Log.d("DEBUG-V_SHADER ATTACHED", "In InitVertexShader()");
        }
    }

    private void initFragmentShader(int ResourceId) //initialises Fragment shader and attaches it to the shader program
    {
        StringBuffer tempBuffer = readInShader(ResourceId);
        mFragmentShader= GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
        GLES30.glShaderSource(mFragmentShader,tempBuffer.toString());
        GLES30.glCompileShader(mFragmentShader);
        IntBuffer CompileErrorStatus = IntBuffer.allocate(1);
        GLES30.glGetShaderiv(mFragmentShader, GLES30.GL_COMPILE_STATUS, CompileErrorStatus);

        if (CompileErrorStatus.get(0) == 0)
        {
            Log.e("ERROR-FRAGMENT SHADER ","Object failed to compile fragment shader: " + mShaderName);
            Log.e("ERROR-FRAGMENT SHADER ", "Could not compile Fragment shader file = " + String.valueOf(ResourceId));
            Log.e("ERROR-FRAGMENT SHADER ", GLES30.glGetShaderInfoLog(mFragmentShader));
            GLES30.glDeleteShader(mFragmentShader);
            mFragmentShader = 0;
        }
        else
        {
            GLES30.glAttachShader(mShaderProgram,mFragmentShader);
            Log.d("DEBUG-F_SHADER ATTACHED",
                    "In InitFragmentShader()");
        }
    }

    private StringBuffer readInShader(int ResourceId) //Reads the shader into a buffer
    {
        StringBuffer TempBuffer = new StringBuffer();
        InputStream inputStream = mContext.getResources().openRawResource(ResourceId);
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        try
        {
            String read = in.readLine();
            while (read != null)
            {
                TempBuffer.append(read + "\n");
                read = in.readLine();
            }
        }
        catch (IOException e)
        {
            //Send a ERROR log message and log the exception.
            Log.e("ERROR-SHADER READ ERROR", "Error in ReadInShader(): " + e.getLocalizedMessage());
        }
        return TempBuffer;
    }

    public void ActivateShader()
    {
        GLES30.glUseProgram(mShaderProgram);
    }

    public void DeActivateShader()
    {
        GLES30.glUseProgram(0);
    }

    public int getShaderProgram()
    {
        return mShaderProgram;
    }

    public int getVertexAttributeVariableLocation(String variable)
    {
        return (GLES30.glGetAttribLocation(mShaderProgram, variable));
    }

    public void setUniformVariable(String variable, float value) //set a Uniform Variable of type float in the shader to a new value
    {
        int location = GLES30.glGetUniformLocation(mShaderProgram,variable);
        GLES30.glUniform1f(location,value);
    }

    public void setUniformVariable(String variable, Vector3D value) //set a Uniform Variable of type vec3 in the shader to the value of a Vector3D
    {
        int location = GLES30.glGetUniformLocation(mShaderProgram,variable);
        GLES30.glUniform3f(location, value.x, value.y, value.z);
    }

    public void SetUniformVariable(String variable, float[] value) //set a uniform Variable of type vec3 in the shader to the first 3 values of a float array
    {
        int location = GLES20.glGetUniformLocation(mShaderProgram,variable);
        GLES20.glUniform3f(location, value[0], value[1], value[2]);
    }

    /*Variable is the name of the uniform mat4 in the shader.
    * common call:- mShader.setMatrix4Array("uModelViewMatrix", 1, false, ModelViewMatrix, 0)
    */
    public void setMatrix4Array(String variable, int count, boolean transpose, float[] value, int offset) //set a uniform variable of type mat4 in the shader to the value of a float[] Matrix
    {
        int location = GLES30.glGetUniformLocation(mShaderProgram,variable);
        GLES30.glUniformMatrix4fv (location, count, transpose, value, offset);
    }



}
