package gd1.gdb1gameengine.Util;

import android.opengl.GLES20;
import android.util.Log;
import android.view.MotionEvent;

import java.security.InvalidParameterException;
import java.util.ArrayList;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineMainActivity;
import gd1.gdb1gameengine.GameObjects.Camera;
import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.TrackingCamera;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.Graphics.ParticleEffects.Particle;
import gd1.gdb1gameengine.Graphics.ParticleEffects.ParticleEmitter;
import gd1.gdb1gameengine.SceneViews.CameraScene;
import gd1.gdb1gameengine.SceneViews.LightScene;
import gd1.gdb1gameengine.SceneViews.MulipleCameraScene;
import gd1.gdb1gameengine.SceneViews.NormalMapScene;
import gd1.gdb1gameengine.SceneViews.Scene1;
import gd1.gdb1gameengine.SceneViews.Scene2;
import gd1.gdb1gameengine.SceneViews.Scene3;
import gd1.gdb1gameengine.SceneViews.SceneLayouts;
import gd1.gdb1gameengine.Util.Sound.BackgroundMusic;
import gd1.gdb1gameengine.Util.UI.Button;
import gd1.gdb1gameengine.Util.UI.HudElement;

/**
 * Created by Shaun on 22/11/2016.
 */


/**
 * This class is intended to handle the updating and drawing of GameObjects and other entities in the game
 * such as cameras and lights. It should also be used for the exchange of data between objects, for instance
 * the view and projection matrices from the cameras need to be given to the GameObjects in order for them
 * to render properly, so that exchange should be made here. In the future things like collision detection
 * should also be carried out here.
 */
public class ObjectHandler
{

    private ArrayList<LightSource> mLightSources;
    private ArrayList<GameObject> mGameObjects;
    private ArrayList<Button> mButtons;
    private ArrayList<Button>mConstantButtons;
    private ArrayList<HudElement>mConstantHudElements;
    private ArrayList<HudElement>mHudElement;
    private ArrayList<ParticleEmitter> mParticleEmitters;
    private ArrayList<Camera> mCameras;
  //  private SceneLayouts s,s2,s3;

    private ArrayList<SceneLayouts> scenes;

    private Camera mActiveCamera;
    private MeshWithTexture mSkybox;
    private int mCurrentScene =0;
    private float mWidth,mHeight;
    private double lastTime;
    private BackgroundMusic bgm;
    private Scene1 s;

    public ObjectHandler(float screenWidth, float screenHeight)
    {
        mLightSources = new ArrayList<>();
        mGameObjects = new ArrayList<>();
        mParticleEmitters = new ArrayList<>();
        mCameras = new ArrayList<>();
        mButtons = new ArrayList<>();
        scenes = new ArrayList<>();
        mHudElement = new ArrayList<>();
        mConstantButtons = new ArrayList<>();
        mConstantHudElements = new ArrayList<>();
        mWidth = screenWidth;
        mHeight = screenHeight;
        mCurrentScene = 0;
        lastTime = System.currentTimeMillis();

    }

    //called to update all Objects in the game
    public void update()
    {
        // update camera
        if (mActiveCamera.getUp() == true)
        {
            mActiveCamera.moveUp();
        }

        if (mActiveCamera.getDown() == true)
        {
            mActiveCamera.moveDown() ;
        }

        if (mActiveCamera.getLeft() == true)
        {
            mActiveCamera.moveLeft();
        }

        if (mActiveCamera.getRight() == true)
        {
            mActiveCamera.moveRight();
        }


        for (int i = 0; i < mCameras.size(); i++)
        {
            if (mCameras.get(i) instanceof TrackingCamera)
            {
                ((TrackingCamera) mCameras.get(i)).update();
            }
        }

        if (mGameObjects.size() != 0)
        {
            for (int i = 0; i < mGameObjects.size(); i++)
            {

                if (mGameObjects.get(i) != null)
                {
                    mGameObjects.get(i).update(mActiveCamera.getViewMatrix(), mActiveCamera.getProjectionMatrix());
                }
            }

        }
        for (int i = 0; i < mLightSources.size(); i++)
        {
            mLightSources.get(i).update(mActiveCamera.getViewMatrix(), mActiveCamera.getProjectionMatrix());
        }



        searchAndDestroy();

    }

    public void draw()
    {
        // store on stack for each game object to use
        float[] viewMatrix = getActiveCamera().getViewMatrix();
        float[] projectionMatrix = getActiveCamera().getProjectionMatrix();
        float[] ortho = getActiveCamera().getOrthoMatrix();

        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        if (mSkybox != null) mSkybox.draw(viewMatrix, projectionMatrix);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        for (GameObject gameObject : getGameObjects())
        {
            if (gameObject != null)
            {
                gameObject.draw(viewMatrix, projectionMatrix);
            }
        }



        for (ParticleEmitter particleEmitter : mParticleEmitters)
        {

            if (particleEmitter != null)
            {
                for (Particle particle : particleEmitter.getParticleList()) {
                        particle.draw(viewMatrix, projectionMatrix);
                }
            }
        }
        /*

        for (int i = 0; i < mParticleEmitters.size(); i++)
        {
            for (int j = 0; j < mParticleEmitters.get(i).getParticleList().size(); j++)
            {
                mParticleEmitters.get(i).getParticleList().get(j).update(mActiveCamera.getViewMatrix(), mActiveCamera.getProjectionMatrix());
                //  mParticleEmitters.get(i).getParticleList().get(j).update();
            }
            if (System.currentTimeMillis() - lastTime > 1000 )
            {
             ////   if(mParticleEmitters.get(i).getParticleList().size()<10)
              //  {
                    mParticleEmitters.get(i).generateParticles(new Vector3D(0, 0, 0));
               // }

            }

        }

  */
        for (LightSource lightSource : mLightSources)
        {
            if (lightSource != null)
            {
                lightSource.draw(viewMatrix, projectionMatrix);
            }
        }

        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        for (Button button : getButtons())
        {
            if (button != null)
            {
              //  button.draw(ortho, projectionMatrix);
            }
        }

        for (Button button : mConstantButtons)
        {
            if (button != null)
            {
              //  button.draw(ortho, projectionMatrix);
            }
        }

        for (HudElement element : mHudElement)
        {
            if (element != null)
            {
                element.draw(ortho, projectionMatrix);
            }
        }
        for (HudElement element : mConstantHudElements)
        {
            if (element != null)
            {
                element.draw(ortho, projectionMatrix);
            }
        }

    }


    public void addGameObject(GameObject gameObject)
    {
        mGameObjects.add(gameObject);
    }

    public void addButton(Button button)
    {
        mButtons.add(button);
    }

    public void addParticleEmitter(ParticleEmitter particleEmitter)
    {
        mParticleEmitters.add(particleEmitter);
    }

    public void addLightSource(LightSource lightSource)
    {
        mLightSources.add(lightSource);
    }

    public void addCamera(Camera camera)
    {
        mCameras.add(camera);
        if (mCameras.size() == 1)
        {
            mActiveCamera = camera;
        }
    }

    public void setActiveCamera(Camera camera)
    {
        mActiveCamera = camera;
    }

    public void setActiveCamera(int index)
    {
        if (index >= mCameras.size())
        {
            throw new InvalidParameterException("Index is out of range of camera array list");
        }
        mActiveCamera = mCameras.get(index);
    }

    public void setSkybox(MeshWithTexture skybox)
    {
        mSkybox = skybox;
    }

    private void searchAndDestroy()
    {
        // for (int i = 0; i < mGameObjects.size(); i++)
        // {
        ////   if (mGameObjects.get(i).isForDestroy())
        //  {
        //      mGameObjects.remove(i);
        //   }
        // }

        for (int i = 0; i < mParticleEmitters.size(); i++) {
            for (int j = 0; j < mParticleEmitters.get(i).getParticleList().size(); j++)
            {
                if(mParticleEmitters.get(i).getParticleList().get(j).isForDestroy())
                {
                    mParticleEmitters.get(i).getParticleList().remove(j);
                }
            }

        }

        for (int i = 0; i < mLightSources.size(); i++)
        {
            if (mLightSources.get(i).isForDestroy())
            {
                mLightSources.remove(i);
            }
        }
    }

    public Camera getActiveCamera()
    {
        return mActiveCamera;
    }

    public ArrayList<GameObject> getGameObjects()
    {
        return mGameObjects;
    }

    public ArrayList<Button> getButtons()
    {
        return mButtons;
    }

    public MeshWithTexture getSkybox()
    {
        return mSkybox;
    }

    public ArrayList<LightSource> getLightSources()
    {
        return mLightSources;
    }

    public void empty()
    {

        mLightSources = new ArrayList<>(1);
        mGameObjects = new ArrayList<>(1);
        mCameras = new ArrayList<>(1);
        mButtons = new ArrayList<>();
        mParticleEmitters = new ArrayList<>(1);
    }

    public void loadScene(int scene)
    {
        System.out.println("Button press correct");
        scenes.get(scene).setmHeight(mHeight);
        scenes.get(scene).setmWidth(mWidth);
        mGameObjects = scenes.get(scene).getGameObjects();
        mParticleEmitters = scenes.get(scene).getParticleEmitters();
        mHudElement = scenes.get(scene).getHudElements();
        mButtons = scenes.get(scene).getButtons();
        mCameras = scenes.get(scene).getCameras();
        mActiveCamera = mCameras.get(0);
        mLightSources = scenes.get(scene).getLightSources();
        mSkybox = scenes.get(scene).getSkybox();

        GameEngineMainActivity.manager.getInputManager().empty();

        if(scene==0)
        {

            mConstantButtons = s.getConstantButtons();
            mConstantHudElements = s.getmConstanthud();
        }


            for (Button a : scenes.get(scene).getButtons())
            {
                GameEngineMainActivity.manager.getInputManager().addButton(a);
            }


            for (Button a : mConstantButtons)
            {
                GameEngineMainActivity.manager.getInputManager().addButton(a);
            }
            //  mSkybox = scenes.get(scene);


    }

    public void setHeight(float mHeight)
    {
        this.mHeight = mHeight;
    }

    public void setWidth(float mWidth)
    {
        this.mWidth = mWidth;

        for (int i =0;i<scenes.size();i++){
        for (Camera camera :scenes.get(i).getCameras())
        {
            camera.setCameraRatio( mWidth /  mHeight, 1f, 100f);
           // camera.calculateNewOrtho( mWidth /  mHeight, -1f, 1f);

        }}
    }

    public void setUpScreens()
    {

        bgm = GameEngineMainActivity.manager.getBgm();
        bgm.loadMusic("audio/memories.mp3");
        bgm.play();
       // bgm.stop();
         s = new Scene1(mWidth, mHeight,this);
        scenes.add(s);
        SceneLayouts s2 = new Scene2(mWidth, mHeight,this);
        scenes.add(s2);
        SceneLayouts  s3 = new Scene3(mWidth, mHeight,this);
        scenes.add(s3);
        SceneLayouts s4 = new NormalMapScene(mWidth, mHeight,this);
        scenes.add(s4);

        SceneLayouts s5 = new MulipleCameraScene(mWidth, mHeight,this);
        scenes.add(s5);

        SceneLayouts s6 = new LightScene(mWidth, mHeight,this);
        scenes.add(s6);

        SceneLayouts s7 = new CameraScene(mWidth, mHeight,this);
        scenes.add(s7);
      //  scenes.get(0).createButtons();


        mConstantButtons = scenes.get(0).getButtons();
        mConstantHudElements = scenes.get(0).getHudElements();

    }

    public void buttonCheck(String buttonName, MotionEvent event)
    {
        System.out.println("Button accepted");
      //  scenes.get(mCurrentScene).ButtonFunctionality(buttonName, event);
        switch (event.getAction() & MotionEvent.ACTION_MASK)
        {


            case MotionEvent.ACTION_UP:

                switch (buttonName)
                {//addin responses for each button
                    case "bear":
                        loadScene(6);
                        // scenes.get(0).createButtons();
                        mCurrentScene = 6;
                        bgm.stop();
                        bgm.loadMusic("audio/beartime.mp3");
                        bgm.play();
                        // change = true;
                        break;

                    case "light":
                        loadScene(5);
                        //  scenes.get(2).createButtons();
                        mCurrentScene = 5;
                        bgm.stop();
                        bgm.loadMusic("audio/goinghigher.mp3");
                        bgm.play();
                        // change = true;
                        break;


                    case "end":
                        loadScene(1);
                        //  scenes.get(1).createButtons();
                        mCurrentScene = 1;
                        bgm.stop();
                        bgm.loadMusic("audio/time.mp3");
                        bgm.play();
                        break;

                    case "photoshoot":
                        loadScene(4);
                        //  scenes.get(1).createButtons();
                        mCurrentScene = 4;

                        break;

                    case "grass":
                        loadScene(2);
                        //  scenes.get(1).createButtons();
                        mCurrentScene = 2;

                        break;
                    case "normals":
                        loadScene(3);
                        //  scenes.get(1).createButtons();
                        mCurrentScene = 3;
                        break;

                    case "music":
                        Log.d("Touch","Music button");


                        if (bgm.currentlyPlaying())
                        {

                            bgm.pause();
                        } else
                        {

                            bgm.resume();
                        }


                        break;

                }

            default:
                scenes.get(mCurrentScene).buttonFunctionality(buttonName, event);
                break;
        }

    }

}
