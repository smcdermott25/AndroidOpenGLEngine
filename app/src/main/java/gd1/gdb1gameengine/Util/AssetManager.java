package gd1.gdb1gameengine.Util;

import android.content.Context;
import android.view.MotionEvent;

import java.util.HashMap;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView;
import gd1.gdb1gameengine.GameObjects.Mesh;
import gd1.gdb1gameengine.GameObjects.Texture;
import gd1.gdb1gameengine.Graphics.MeshLoader;
import gd1.gdb1gameengine.Util.Sound.BackgroundMusic;

/**
 * Created by 40101817 on 24/10/2016.
 */
public class AssetManager
{

    private Context c;
    private MeshLoader meshLoader;
    private InputManager inputManager;

    public BackgroundMusic getBgm()
    {
        return bgm;
    }

    //creating Hashmaps
    private HashMap<String, Mesh> mesh;
    private HashMap<String, Texture> texture;
    private BackgroundMusic bgm;


    public AssetManager(Context c)
    {

        this.c = c;
        meshLoader = new MeshLoader();
        inputManager = new InputManager();
        mesh = new HashMap<>();
        bgm = new BackgroundMusic(c);
    }


    //storing meshes
    public boolean add(String assetName, Mesh asset)
    {
        if (mesh.containsKey(assetName))
        {
            return false;
        }
        // mesh.put(assetName, asset);
        return true;
    }

    //storing textures
    public boolean add(String assetName, Texture assetPath)
    {
        if (texture.containsKey(assetName))
        {
            return false;
        }
        texture.put(assetName, assetPath);
        return true;
    }


    //Andrew i am reworking some methods - Ryan
    /*
    private boolean deployMesh(String assetName, int assetPath) {
        boolean loadSuccessful =true;
     //   Mesh mesh = new Mesh(this, GameEngineSurfaceView.context, assetName, assetPath);
     //   loadSuccessful = add(assetName, mesh);
        return loadSuccessful;
    }
*/
    public Mesh deployMesh(String name, int assetPath)
    {
        if (!mesh.isEmpty())
        {
            if (mesh.containsKey(name))
            {
                return mesh.get(name);
            } else
            {

                //  if (mesh.containsKey(name)) {
                //   return mesh.get(assetPath);
                //  }else {

                Mesh m = meshLoader.parseObj(c, assetPath);
                mesh.put(name, m);
                return m;
            }


        } else
        {

            Mesh m = meshLoader.parseObj(c, assetPath);
            mesh.put(name, m);
            return m;
        }
    }

    //  Mesh mesh = new Mesh(this, GameEngineSurfaceView.context, assetName, assetPath);
    //   loadSuccessful = add(assetName, mesh);
    //  return loadSuccessful;

/*
    //write getMesh method, then repeat for textures etc
    public Mesh getMesh(String assetName, int assetPath)  {
        if(mesh.containsKey(assetName)) {
            return mesh.get(assetName);
        }
        else{
            if(deployMesh(assetName, assetPath)) {
                return mesh.get(assetName);
            }
            else {
                return null;
            }
        }
        }
*/

    public void handleInput(MotionEvent motionEvent, GameEngineSurfaceView view)
    {
        inputManager.handleInput(motionEvent, view);
    }

    public InputManager getInputManager()
    {
        return inputManager;
    }


}
