package gd1.gdb1gameengine.Util;

import android.view.MotionEvent;

import java.util.ArrayList;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView;
import gd1.gdb1gameengine.Util.UI.Button;

/**
 * Created by Ryan on 02/11/2016.
 *
 * This class processes a touch event and decides if it was within the boundaries of a button
 * If the event happened within a button the button name and the type of event is passed on to
 * the renderer so that the buttons functionality can be called.
 */

public class InputManager
{
    private ArrayList<Button>buttons ;//All the buttons that this manager is responsible for
    public InputManager()
    {
        buttons = new ArrayList<>();//Initialize arrayList of type Button
    }


    public void addButton(Button newButton)//This is called in the renderer
    {
        buttons.add(newButton);
    }

    public void empty(){buttons = new ArrayList<>();}
    public void handleInput(MotionEvent motionEvent,GameEngineSurfaceView view)
    {
        //The position of the touch is temporarily stored
        float x = motionEvent.getX();
        float y = motionEvent.getY();

        //The touch coordinates are then converted to gl co-ordinates
        float theX = x*2.0f/view.getScreenWidth()-1;
        float theY = y*2.0f/view.getScreenHeight()-1;


        System.out.println("The x: "+ theX +" the y: "+ -theY);

        for(int a = 0;a<buttons.size();a++) {//for every button

            //Not that y is inversed due to 0,0 being in the middle of the screen for gl
            if (buttons.get(a).Contains(theX, -theY))
            {//searches to see if button location was where the finger was

                System.out.println("Button pressed");
                //this manager has now confirmed that a button has been pressed
                //the buttonFunctionality method in the renderer is now called using the
                //name of the button pressed
                view.coreSurfaceRenderer.getHandler().buttonCheck(buttons.get(a).getName(),motionEvent);

            }
        }

    }




}
