package gd1.gdb1gameengine.Util.Logging;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.logging.ConsoleHandler;

import static gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView.context;

/**
 * Created by 40101817 on 24/10/2016.
 */
public class Logger
{

    private static java.util.logging.Logger logger;
    private MyFormatter formatter;
    private ConsoleHandler handler;

    //logToFile vars
    private FileOutputStream fos = null;
    private BufferedWriter writer;
    private boolean loggingToFile = false;
    private boolean logging = false;


    public Logger()
    {
        logger = java.util.logging.Logger.getLogger("MyLog");
        logger.setUseParentHandlers(false);
        formatter = new MyFormatter();
        handler = new ConsoleHandler();
        handler.setFormatter(formatter);
        logger.addHandler(handler);
    }

    public void info(String message)
    {
        if (logging)
        {
            logger.info(message);
            if(loggingToFile)
            {
                writeToFile(formatter.getFinalLog());
            }
        }
    }

    public void warning(String message)
    {
        if (logging)
        {
            logger.warning(message);
            if(loggingToFile)
            {
                writeToFile(formatter.getFinalLog());
            }
        }
    }

    public void severe(String message)
    {
        if (logging)
        {
            logger.severe(message);
            if(loggingToFile)
            {
                writeToFile(formatter.getFinalLog());
            }
        }
    }

    public void logToFile()
    {
        try
        {
            fos = context.openFileOutput("log.txt", Context.MODE_PRIVATE);
            writer = new BufferedWriter(new OutputStreamWriter(fos));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    //logging on/off
    public void logging(boolean logging)
    {
        this.logging = logging;
    }

    //logToFile off switch
    public void logToFile(boolean loggingToFile)
    {
        this.loggingToFile = loggingToFile;
        if (true){
            logToFile();
        }
    }

    public void writeToFile(String data) {
        try
        {
            writer.write(data);
            writer.write("\n");
            writer.flush();
        }
        catch (IOException e)
        {
            Log.e("Exception", "File write failed: "+ e.toString());
        }
    }

    //Display log file
    public void displayRecordedLogs(Context c)
    {

        FileInputStream in = null;
        try
        {
            in = context.openFileInput("log.txt");
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        InputStreamReader inputStreamReader = new InputStreamReader(in);
        BufferedReader br = new BufferedReader(inputStreamReader);
        String line = "";
        while(line != null)
        {
            Log.d("Log ", line);
            try {
                line = br.readLine();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}

