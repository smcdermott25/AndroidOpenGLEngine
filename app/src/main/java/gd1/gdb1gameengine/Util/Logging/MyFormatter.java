package gd1.gdb1gameengine.Util.Logging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by awoods321 on 25/11/2016.
 */

public class MyFormatter extends Formatter
{

    private static final DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS");
    String finalLog;

    public String format(LogRecord record)
    {
        int thread = record.getThreadID();
        StringBuilder builder = new StringBuilder(1000);
        builder.append(df.format(new Date(record.getMillis())));
        builder.append("[").append(record.getSourceClassName()).append("]");
        builder.append(" Method = [").append(record.getSourceMethodName()).append("] - ");
        builder.append("Thread ID = [").append(thread).append("] - ");
        builder.append("[").append(record.getLevel()).append("] - ");
        builder.append(formatMessage(record));
        builder.append("\n");
        finalLog = builder.toString();
        return finalLog;
    }

    public String getFinalLog() {
        return finalLog;
    }

    public String getHead(Handler h)
    {
        return super.getHead(h);
    }

    public String getTail(Handler h)
    {
        return super.getTail(h);
    }

}
