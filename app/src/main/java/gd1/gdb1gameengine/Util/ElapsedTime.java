package gd1.gdb1gameengine.Util;

/**
 * Taken from Phill Hannah's GAGE Engine
 */

public class ElapsedTime {
    /**
     * Amount of time that has elapsed since the last frame
     */
    public double stepTime;

    /**
     * Amount of time that has elapsed since the game started (first frame)
     */
    public double totalTime;
}
