package gd1.gdb1gameengine.Util.UI;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;

/**
 * Created by Ryan on 02/12/2016.
 */

public class Element extends HudElement
{
    private FloatBuffer vertexBuffer;
    private int mColorHandle;
    static final int COORDS_PER_VERTEX = 3; // number of coordinates per vertex in this array

    private int vertexCount;
    private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex
    private float height;//height of button

    //private final int mProgram;
    private boolean draw;
    private Vector3D colour;
    private int mPositionHandle;
    private float colour1[] = {0.63671875f, 0.76953125f, 0.22265625f, 1.0f};

    public void setColour(Vector3D colour) {
        this.colour = colour;
        mShader.setUniformVariable("objectColor", colour);
    }

    public Element(int meshRef, String name)
    {
        super(R.raw.trianglevertex, R.raw.testfragment, meshRef,name);

        colour = new Vector3D(0.544f, 0.4564f, 0.4564f);

        mShader.ActivateShader();
        mShader.setUniformVariable("objectColor", colour);
        loadTexture();

        vertexCount = mesh.mVertices.length / COORDS_PER_VERTEX;//amount of vertexes


        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(mesh.mVertices.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(mesh.mVertices);
        vertexBuffer.position(0);

    }

    @Override
    public void loadTexture()
    {

    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {

    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {
        mShader.ActivateShader();
        forButtons(viewMatrix);


        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mShader.getShaderProgram());

        int mMVPMatrixHandle = GLES20.glGetUniformLocation(mShader.getShaderProgram(), "uMVPMatrix");
        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(mShader.getShaderProgram(), "vPosition");

        // get handle to fragment shader's vColor member
        mColorHandle = GLES20.glGetUniformLocation(mShader.getShaderProgram(), "vColor");


        // Enable a handle to the button vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Prepare the button coordinate data
        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);



        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, this.getMVPMatrix(), 0);
        // Set color for drawing the button

        GLES20.glUniform4fv(mColorHandle, 1, colour1, 0);
        // Draw the button
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }
}
