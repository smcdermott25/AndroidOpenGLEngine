package gd1.gdb1gameengine.Util.UI;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import gd1.gdb1gameengine.GameObjects.Texture;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;

/**
 * Created by Ryan on 30/11/2016.
 */

public class ButtonImages extends HudElement
{


    private FloatBuffer vertexBuffer;
    private int mColorHandle;
    static final int COORDS_PER_VERTEX = 3; // number of coordinates per vertex in this array

    private int vertexCount;
    private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex
    private float height;//height of button


    private Vector3D colour;
    private int mPositionHandle;
    private boolean draw = true;




    public ButtonImages( int meshRef,int textureFile, String meshName, Button button )
    {
        super(R.raw.texturetrianglevertex, R.raw.texturetrianglefragment, meshRef, meshName);
        colour = new Vector3D(0.544f, 0.4564f, 0.4564f);
        this.textureFile = textureFile;
        mShader.ActivateShader();
        mShader.setUniformVariable("objectColor", colour);
        loadTexture();


        this.translate(button.getLocation());
        vertexCount = mesh.mVertices.length / COORDS_PER_VERTEX;//amount of vertexes



        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(mesh.mVertices.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(mesh.mVertices);
        vertexBuffer.position(0);

    }



    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {

    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {
        if (draw)
        {

            mShader.ActivateShader();
            forButtons(viewMatrix);

            // Use texture
            texture.useTexture(mShader.getShaderProgram());
            // Add program to OpenGL ES environment
            GLES20.glUseProgram(mShader.getShaderProgram());

            int mMVPMatrixHandle = GLES20.glGetUniformLocation(mShader.getShaderProgram(), "uMVPMatrix");
            // get handle to vertex shader's vPosition member
            mPositionHandle = GLES20.glGetAttribLocation(mShader.getShaderProgram(), "vPosition");

            // Enable a handle to the button vertices
            GLES20.glEnableVertexAttribArray(mPositionHandle);

            // Prepare the button coordinate data
            GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                    GLES20.GL_FLOAT, false,
                    vertexStride, vertexBuffer);


            // Pass the projection and view transformation to the shader
            GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, this.getMVPMatrix(), 0);
            // Set color for drawing the button

            // Draw the button
            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

            // Disable vertex array
            GLES20.glDisableVertexAttribArray(mPositionHandle);
        }

    }

    @Override
    public void loadTexture()
    {

        if(mesh.mTextures!=null)
        {
            texture = new Texture(mesh.mTextures,
                    textureFile);

        }

    }

    public void setDraw(boolean draw)
    {
        this.draw = draw;
    }
}
