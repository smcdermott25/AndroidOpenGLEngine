package gd1.gdb1gameengine.Util.UI;

import android.opengl.Matrix;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineMainActivity;
import gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView;
import gd1.gdb1gameengine.GameObjects.Mesh;
import gd1.gdb1gameengine.GameObjects.Orientation;
import gd1.gdb1gameengine.GameObjects.Texture;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.Util.Shader;

/**
 * Created by Ryan on 30/11/2016.
 */

public abstract class HudElement
{

    protected Mesh mesh;
    protected Texture texture;
    protected Shader mShader;
    private float[] mOrtho;
    private float[] mMVPMatrix;
    protected int textureFile;
    protected Orientation orientation;


    public float[] getMVPMatrix()
    {
        return mMVPMatrix;
    }

    public HudElement(int vertexShader, int fragmentShader, int meshRef, String meshName)
    {
        orientation = new Orientation();
        mOrtho = new float[16];
        mMVPMatrix = new float[16];

        if (meshName != null)
        {

            mesh = GameEngineMainActivity.manager.deployMesh(meshName, meshRef);
        }
        mShader = new Shader(GameEngineSurfaceView.context, vertexShader, fragmentShader, "HudElement");

    }


    public HudElement(int vertexShader, int fragmentShader)
    {
        this(vertexShader, fragmentShader, 0, null);
    }

    public abstract void loadTexture();


    public abstract void update(float[] viewMatrix, float[] projectionMatrix);


    public abstract void draw(float[] viewMatrix, float[] projectionMatrix);

    public void forButtons(float[] orthoMatrix)
    {

        Matrix.multiplyMM(mMVPMatrix, 0, orthoMatrix, 0, orientation.getModelMatrix(), 0);
        mOrtho = orthoMatrix;

    }

    public float[] fortouchevent(float [] test)
    {

         float [] tester = new float[3];
        Matrix.multiplyMV(tester, 0, mOrtho, 0, test, 0);

       // float tester = new float[];

        return  tester;

    }


    public void scale(Vector3D scale)
    {
        orientation.setScale(scale);
    }

    public void translate(Vector3D vector)
    {
        orientation.setPosition(vector);
    }

    public Orientation getOrientation()
    {
        return orientation;
    }


}
