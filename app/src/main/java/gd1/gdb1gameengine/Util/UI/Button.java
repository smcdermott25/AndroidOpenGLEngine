package gd1.gdb1gameengine.Util.UI;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import gd1.gdb1gameengine.GameObjects.Texture;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;


/**
 * Created by Ryan on 02/11/2016.
 * <p>
 * I would like to discuss with the group how we want to do buttons.
 * Currently this class is used to create and draw a rectangle that we use as a button
 * This may suffice if we texture it
 */

public class Button extends HudElement
{
    public void setDraw(boolean draw)
    {
        this.draw = draw;
    }

    private FloatBuffer vertexBuffer;
    private int mColorHandle;
    static final int COORDS_PER_VERTEX = 3; // number of coordinates per vertex in this array
    private float squareCoords[] = new float[18];//2 triangles each with 3 co ordinates
    private final int vertexCount = squareCoords.length / COORDS_PER_VERTEX;//amount of vertexes
    private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex
    private float height;//height of button
    private float width;//width of button

    //private final int mProgram;
    private boolean draw;
    private Vector3D colour;
    private int mPositionHandle;

    private Vector3D location;
    private float openGlcorrection = 0.42f;

    // Set colour with red, green, blue and alpha (opacity) values
    float colour1[] = {0.63671875f, 0.76953125f, 0.22265625f, 1.0f};

    private String name;//used to ID the button


    //a rectangle can be fully defined with the top left and bottom right points
    public Button(String name, Vector3D topLeft, float width, float height)
    {
        super(R.raw.buttonvertex, R.raw.texturetrianglefragment);
        draw = true;
        loadTexture();

        this.name = name;//assign the button its name
        // height = bottomRight.y - topLeft.y;//assign the button its height
        this.height = height;
        this.width = width;

        mShader.ActivateShader();


        location = new Vector3D(((topLeft.x + width) - (width / 2)), (topLeft.y - height) + (height / 2), 0);


        //Top left point x,y,z
        squareCoords[0] = topLeft.x;
        squareCoords[1] = topLeft.y;
        squareCoords[2] = 0;

        //Bottom left;
        squareCoords[3] = topLeft.x;
        squareCoords[4] = topLeft.y - height;
        squareCoords[5] = 0;

        //Bottom right
        squareCoords[6] = topLeft.x + width;
        squareCoords[7] = topLeft.y - height;
        squareCoords[8] = 0;

        //Top left
        squareCoords[9] = topLeft.x;
        squareCoords[10] = topLeft.y;
        squareCoords[11] = 0;

        //Bottom right
        squareCoords[12] = topLeft.x + width;
        squareCoords[13] = topLeft.y - height;
        squareCoords[14] = 0;

        //Top right
        squareCoords[15] = topLeft.x + width;
        squareCoords[16] = topLeft.y;
        squareCoords[17] = 0;


        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(squareCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(squareCoords);
        vertexBuffer.position(0);


    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {

    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {
        if (draw)
        {

            mShader.ActivateShader();
            forButtons(viewMatrix);

            // Use texture
            texture.useTexture(mShader.getShaderProgram());
            // Add program to OpenGL ES environment
            GLES20.glUseProgram(mShader.getShaderProgram());

            int mMVPMatrixHandle = GLES20.glGetUniformLocation(mShader.getShaderProgram(), "uMVPMatrix");
            // get handle to vertex shader's vPosition member
            mPositionHandle = GLES20.glGetAttribLocation(mShader.getShaderProgram(), "vPosition");

            // Enable a handle to the button vertices
            GLES20.glEnableVertexAttribArray(mPositionHandle);

            // Prepare the button coordinate data
            GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                    GLES20.GL_FLOAT, false,
                    vertexStride, vertexBuffer);

            // get handle to fragment shader's vColor member
            mColorHandle = GLES20.glGetUniformLocation(mShader.getShaderProgram(), "vColor");

            // Pass the projection and view transformation to the shader
            GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, this.getMVPMatrix(), 0);
            // Set color for drawing the button
            GLES20.glUniform4fv(mColorHandle, 1, colour1, 0);

            // Draw the button
            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

            // Disable vertex array
            GLES20.glDisableVertexAttribArray(mPositionHandle);
        }
    }

    @Override
    public void loadTexture()
    {
        texture = new Texture(new float[]

                {
                        0.0f, 0.0f,
                        .3f, 0.0f,
                        0.3f, 0.3f,
                        0.3f, 0.3f,
                        .3f, 0.0f,
                        0.0f, 0.0f,
                },
                R.drawable.texturetest);
    }

    public boolean Contains(float x, float y)
    //used to check if a button has been pressed
    {//x and y of touch is passed in
       // float [] vector = new float[]{x,y,0};

       // vector = fortouchevent(vector);
      //  Log.d("Touch event X: "+ vector[0],"The y: "+vector[1]);

if(squareCoords[1]>0){height =0;}
        if(x>= squareCoords[0]&&x<=squareCoords[6]&&(y-height)<squareCoords[1]&&(y-height)>squareCoords[7])
        {
            //if the co-ordinates are contained within the button
            return true;
        }

        return false;
    }

    public String getName()
    {
        return name;
    }

    public Vector3D getLocation()
    {
        return location;
    }
}
