package gd1.gdb1gameengine.Graphics;

/**
 * Created by Shaun on 24/11/2016.
 */

public class Material {

    private float[] mEmissive;
    private float[] mAmbient;
    private float[] mDiffuse;
    private float[] mSpecular;
    private float mAlpha;

    public Material(float[] emissive, float[] ambient,
                    float[] diffuse, float[] specular, float alpha)
    {
        mEmissive=emissive;
        mAmbient=ambient;
        mDiffuse=diffuse;
        mSpecular=specular;
        mAlpha=alpha;
    }

    public float[] getEmissive()
    {
        return mEmissive;
    }

    public void setEmissive(float[] mEmissive)
    {
        this.mEmissive = mEmissive;
    }

    public float[] getAmbient()
    {
        return mAmbient;
    }

    public void setAmbient(float[] mAmbient)
    {
        this.mAmbient = mAmbient;
    }

    public float[] getDiffuse()
    {
        return mDiffuse;
    }

    public void setDiffuse(float[] mDiffuse)
    {
        this.mDiffuse = mDiffuse;
    }

    public float[] getSpecular()
    {
        return mSpecular;
    }

    public void setSpecular(float[] mSpecular)
    {
        this.mSpecular = mSpecular;
    }

    public float getAlpha()
    {
        return mAlpha;
    }

    public void setAlpha(float mAlpha)
    {
        this.mAlpha = mAlpha;
    }


}
