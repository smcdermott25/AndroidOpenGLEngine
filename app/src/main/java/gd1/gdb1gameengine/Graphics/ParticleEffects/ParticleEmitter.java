package gd1.gdb1gameengine.Graphics.ParticleEffects;

import java.util.ArrayList;
import java.util.List;

import gd1.gdb1gameengine.GameObjects.Vector3D;

/**
 * Created by awoods321 on 01/12/2016.
 */

public class ParticleEmitter
{
    public static List<Particle> particles = new ArrayList<>();
    float particlesPerSecond;
    float speed;
    float gravityEffect;
    float lifeLength;

    public ParticleEmitter(float particlesPerSecond, float speed, float gravityEffect, float lifeLength)
    {

        this.particlesPerSecond = particlesPerSecond;
        this.speed = speed;
        this.gravityEffect = gravityEffect;
        this.lifeLength = lifeLength;
    }

    public void generateParticles(Vector3D location)
    {

        int count = (int)Math.floor(particlesPerSecond);
        float partialParticle = particlesPerSecond % 1;

        for(int i =0; i<count; i++)
        {
            emitParticle(location);
        }
        if (Math.random() < partialParticle)
        {
            emitParticle(location);
        }
    }

    public void emitParticle(Vector3D spawnPoint)
    {
        float dirX = (float) Math.random() * 2f - 1f;
        float dirZ = (float) Math.random() * 2f - 1f;
        Vector3D velocity = new Vector3D(dirX, 1, dirZ);
        velocity.normalize();
        velocity.multiply(speed);
        Vector3D temp = spawnPoint;

        Particle p = new Particle(temp, velocity, gravityEffect, lifeLength, 0, 1);
        p.scaleObject(new Vector3D(0.008f,0.008f,0.008f));
        addParticle(p);
        //p.setDraw(true);
    }

    public static void addParticle(Particle particle)
    {
        particles.add(particle);
    }

    public List<Particle> getParticleList()
    {
        return particles;
    }
}

