package gd1.gdb1gameengine.Graphics.ParticleEffects;

import java.nio.FloatBuffer;

import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.ElapsedTime;
import gd1.gdb1gameengine.Util.Logging.Logger;

/**
 * Created by awoods321 on 29/11/2016.
 */

public class Particle extends GameObject{

    private Vector3D position;
    private Vector3D velocity;
    private float gravity = -50;
    private float gravityEffect;
    private float lifeLength;
    private float rotation;
    private float scale;
    boolean draw;
    ElapsedTime time;
    Logger log;

    private int vertexHandle;
    private FloatBuffer vertexBuffer;


    private float particleTime = 0;

    private float[] vertices= new float []
            {
                    // vertex positions     vertex Normals
                    -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
                    0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
                    0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
                    0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
                    -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
                    -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f,

            };

    private final int vertexCount = vertices.length / 6;
    private final int vertexStride = 6 * 4; // 4 bytes per floating point number / vertex


    public Particle(Vector3D position, Vector3D velocity, float gravityEffect, float lifeLength, float rotation, float scale) {
        super(R.raw.texturetrianglevertex, R.raw.testfragment,R.raw.cube,"cube","Particle");

        this.position = position;
        this.velocity = velocity;
        this.gravityEffect = gravityEffect;
        this.lifeLength = lifeLength;
        this.rotation = rotation;
        this.scale = scale;

        scaleObject(new Vector3D(0.008f,0.008f,0.008f));

        Vector3D colour = new Vector3D(0.544f, 0.4564f, 0.4564f);

        time = new ElapsedTime();
        mShader.ActivateShader();
        mShader.setUniformVariable("objectColor", colour);



        //adding this particle to the emitter
      //  ParticleEmitter.addParticle(this);

    }

    public Vector3D getPosition() {
        return position;
    }

    public float getRotation() {
        return rotation;
    }

    public float getScale() {
        return scale;
    }



    @Override
    public void loadTexture() {

    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix) {

        scaleObject(new Vector3D(0.001f,0.001f,0.001f));

       // Log.d("Update of particle: "," here");
        //velocity.y = gravity * gravityEffect * (float)time.stepTime;
        //Vector3D change = velocity;
       //change.multiply((float)time.stepTime);
      // position = Vector3D.add(change, position);
        //particleTime += (float)time.stepTime;
        if(particleTime < lifeLength)
        {
            this.isForDestroy();
        };


    }

   // public void update() {

   // }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {

        // Load program
        mShader.ActivateShader();

        // Load program
        mShader.ActivateShader();

        generateMatrices(viewMatrix, projectionMatrix);

        //  mShader.setUniformVariable("ambientStrength",0.1f);

        // Draw mesh
        mesh.draw(getMVPMatrix(), mShader.getShaderProgram());



    }
}
