package gd1.gdb1gameengine.Graphics;

import java.util.ArrayList;

import gd1.gdb1gameengine.GameObjects.Mesh;
import gd1.gdb1gameengine.GameObjects.Vector3D;

/**
 * Created by Ryan on 24/10/2016.
 */
//This class parses the information from an arraylist of strings into arraylists of various primitive types that can
//later be used by open gl to plot a mesh.


public class Parser

{
    public ArrayList<Vector3D> mVertices;//holds all vertices  x,y,z
    public ArrayList<Vector3D> mNormals;//holds the normals of a face
    public ArrayList<Vector3D> mTextures;//holds the texture co-ordinates of a face


    public ArrayList<int[]> mFaceVerticeReference;
    public ArrayList<int[]> mFaceTextureReference;
    public ArrayList<int[]> mFaceNormalReference;
    private boolean girl = false;
    float[] mMeshVerticesCoordinates;
    float[] mMeshVerticesTextures;
    float[] mMeshVerticesNormals;
    int mFacesNormals[];
    int mFacesTexCoords[];


    public Mesh parse(ArrayList<String> input)
    {
        //this method converts an arrayList of strings into usable data types
        //Initialize all ArrayLists

        mVertices = new ArrayList<>();
        mNormals = new ArrayList<>();
        mTextures = new ArrayList<>();
        mFaceVerticeReference = new ArrayList<>();
        mFaceNormalReference = new ArrayList<>();
        mFaceTextureReference = new ArrayList<>();


        for (int i = 0; i < input.size(); i++)
        {
            //For every string in the file

            //if the line in the fle is not null and is not empty
            if (input.get(i) != null && input.get(i).length() != 0)
            {

                //Check the first letter in the string

                //if it is v we are dealing with vertice references
                if (input.get(i).startsWith("v "))
                {
                    //Call the loader method
                    //inputs: current line, the vertices ArrayList
                    loader(" "+input.get(i) , mVertices);
                }

                //if it is vt we are dealing with texture references
                if (input.get(i).startsWith("vt"))
                {
                    loader(input.get(i), mTextures);
                }
                //if it is vn we are dealing with normal references
                if (input.get(i).startsWith("vn"))
                {
                    //Call the loader method
                    //inputs: current line, the normals ArrayList
                    loader(input.get(i), mNormals);
                }
                //if it is f we are dealing with face references
                if (input.get(i).startsWith("f"))
                {
                    //Call the faces method
                    //inputs: current line
                    faces(input.get(i));

                }

            }
        }


        //Call the method that uses the face info to create an array
        //of relevant vertices that can later be passed to a buffer
        //to be drawn
        // createBuffers();
        processingFaces();

        Mesh m = new Mesh(mMeshVerticesCoordinates,mMeshVerticesTextures,mMeshVerticesNormals);
        return m;
    }

    private Vector3D loader(String line, ArrayList arrayList)
    {
        float temp1 = 0, temp2 = 0, temp3 = 0;
        //space is two to account for the first letter of a line and the space after it
        //count is used to decide how far we are in to a line
        int space = 2, count = 1;
        boolean start = false;
        for (int a = 1; a < line.length(); a++)
        {
            if (!start)
            {
                if (line.charAt(a) == ' ' & line.charAt(a + 1) != ' ')
                {
                    start = true;
                    space = a + 1;
                }
            }
        }
        //       System.out.println("Start = "+space);
        for (int i = space; i < line.length(); i++)
        {//for the line length excluding the first two values increment i


            //if the current char is a space or is the end of the line
            if (line.charAt(i) == ' ' || (i + 1) == line.length())
            {
                if (count == 1)
                {//if it is the first value in the line

                    //parse the value into the first temp variable
                    temp1 = Float.parseFloat(line.substring(space, i));

                } else if (count == 2)
                {//if it is the second value in the line
                    //parse the value into the second temp variable
                    if((i+1)==line.length()){
                        temp2 = Float.parseFloat(line.substring(space, line.length()));
                    }else {
                    temp2 = Float.parseFloat(line.substring(space, i));}

                } else if(arrayList!=mTextures)
                {  //if it is the third value in the line

                    //parse the value into the third temp variable
                    temp3 = Float.parseFloat(line.substring(space, line.length()));
                }

                count++;//increase the count each time a value is recorded
                //space is a reference to the start of a value
                //the start of the next value is the end of the last
                space = i;
            }

        }
        //Note the devisions here are neccesary as there is no scaling yet
        float[] array = {temp1, temp2, (temp3)};//create an array of the temp values
        Vector3D vertice = new Vector3D(temp1, temp2, temp3);
        //these values represent x,y,z of a vertex or normal
        arrayList.add(vertice);//add the array to the arraylist the method has been called with

        return vertice;
    }

    private void faces(String line)
    {


        int space = 2, count = 1;

        int startOfData = 2;
        int[] texture = new int[3];
        int[] normal = new int[3];
        int[] vertex = new int[3];
        boolean start = true;



        if (!line.contains("/"))
        {

            for (int i = startOfData; i < line.length(); i++)
            {

                if (line.charAt(i) == ' ' || (i + 1) == line.length() || line.charAt(i) == '/')
                {

                    switch (count)
                    {
                        case 1:
                            vertex[0] = Integer.parseInt(line.substring((space), i));
                            break;
                        case 2:
                            vertex[1] = Integer.parseInt(line.substring((space + 1), i));
                            break;
                        case 3:
                            vertex[2] = Integer.parseInt(line.substring((space + 1), line.length()));
                            break;
                    }

                    count++;
                    space = i;

                }

            }

        } else
        {
            for (int i = startOfData; i < line.length(); i++)
            {
                if (line.charAt(i) == ' ' || (i + 1) == line.length() || line.charAt(i) == '/')
                {


                    switch (count)
                    {

                        case 1:
                            vertex[0] = Integer.parseInt(line.substring((space), i));
                            break;
                        case 2:

                            if(mTextures.size()!=0){
                            texture[0] = Integer.parseInt(line.substring((space+1), i));}
                            break;
                        case 3:
                            normal[0] = Integer.parseInt(line.substring((space+1), i));
                            break;

                        case 4:
                            vertex[1] = Integer.parseInt(line.substring((space + 1), i));
                            break;
                        case 5:
                            if(mTextures.size()!=0)
                            {
                                texture[1] = Integer.parseInt(line.substring((space + 1), i));
                            }
                            break;
                       case 6:
                           normal[1] = Integer.parseInt(line.substring((space+1), i));
                           break;
                        case 7:
                            vertex[2] = Integer.parseInt(line.substring((space + 1), i));
                            break;
                        case 8:
                            if(mTextures.size()!=0){
                                texture[2] = Integer.parseInt(line.substring((space + 1), i));
                            }
                            break;
                        case 9:
                            if (line.charAt(line.length()-1)!=' ')
                            {
                                normal[2] = Integer.parseInt(line.substring((space + 1), line.length()));
                            } else
                            {
                                normal[2] = Integer.parseInt(line.substring((space + 1), line.length()-1));
                            }
                            break;
                    }
                    count++;
                    space = i;



                    if(line.charAt(i)== ' ')
                    {

                    }

                }

            }
            mFaceTextureReference.add(texture);
            mFaceNormalReference.add(normal);
        }


        mFaceVerticeReference.add(vertex);


    }

    public void processingFaces()
    {



        int coordinates = (mFaceVerticeReference.size() * 9);
        mMeshVerticesCoordinates = new float[coordinates];
        if(mFaceTextureReference.size()!=0)
        {
            mMeshVerticesNormals = new float[coordinates];
            mMeshVerticesTextures = new float[mFaceTextureReference.size()*6];
        }

//        meshVerticesNormals = new float[facesNormals.length * 3];
//        meshVerticesTextures = new float[facesTexCoords.length * 2];

        // }
        int i = 0;
        int a =0;
        int j = 0;
        int faceCoordinate;
        while (i != (coordinates))
        {
            faceCoordinate = 0;

            while (faceCoordinate != 3)
            {
                mMeshVerticesCoordinates[i] = mVertices.get((mFaceVerticeReference.get(j)[faceCoordinate]) - 1).x;
                if(mFaceNormalReference.size()!=0)
                {
                 mMeshVerticesNormals[i] = mNormals.get((mFaceNormalReference.get(j)[faceCoordinate])-1).x;
                    if(a<mMeshVerticesTextures.length)
                    {
                        if(mTextures.size()!=0)
                        {
                            mMeshVerticesTextures[a] = mTextures.get((mFaceTextureReference.get(j)[faceCoordinate]) - 1).x;
                        }
                    }
                }

                i++;
                a++;
                mMeshVerticesCoordinates[i] = mVertices.get((mFaceVerticeReference.get(j)[faceCoordinate]) - 1).y;
                if(mFaceNormalReference.size()!=0)
                {
                    mMeshVerticesNormals[i] = mNormals.get((mFaceNormalReference.get(j)[faceCoordinate])-1).y;
                    if(a<mMeshVerticesTextures.length)
                    {
                        if(mTextures.size()!=0)
                        {
                            mMeshVerticesTextures[a] = mTextures.get((mFaceTextureReference.get(j)[faceCoordinate]) - 1).y;
                        }
                    }
                }

                i++;
                a++;
                mMeshVerticesCoordinates[i] = mVertices.get((mFaceVerticeReference.get(j)[faceCoordinate]) - 1).z;
                if(mFaceNormalReference.size()!=0)
                {
                    mMeshVerticesNormals[i] = mNormals.get((mFaceNormalReference.get(j)[faceCoordinate])-1).z;
                   // mMeshVerticesTextures[i] = mTextures.get((mFaceTextureReference.get(j)[faceCoordinate])-1).z;
                }

                i++;
                faceCoordinate++;
            }
            j++;
        }
    }

}
