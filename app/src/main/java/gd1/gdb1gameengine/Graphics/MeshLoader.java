package gd1.gdb1gameengine.Graphics;

import android.content.Context;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import gd1.gdb1gameengine.GameObjects.Mesh;
import gd1.gdb1gameengine.R;

/**
 * Created by Ryan on 24/10/2016.
 *
 * This class loads a mesh in then form of an obj file, converts it into an array of strings and passes it to the parser file
 * to be translated from strings into arrays of various primitives.....see parser class
 */

//TODO discuss structure of Mesh Loading on Monday 31st Oct meeting, including need for multiple MeshLoaders
public class MeshLoader
{

  //  File objFile;
    public Parser parser ;
    MTLReader reader;
//    FileReader fileReader;
    BufferedReader bufferedReader;




    public Mesh parseObj(Context context, int fileName)
    {
        parser = new Parser();//initialise a parser
        reader = new MTLReader();


        reader.parse(loadFile(context, R.raw.beartextureinfo));

        return loadMesh(loadFile(context, fileName));

    }


    private ArrayList<String> loadFile(Context context, int fileName)
    {
        ArrayList<String>separatedTextFile = new ArrayList<String>();
        try
        {
            InputStream iS = context.getResources().openRawResource(fileName);
            bufferedReader = new BufferedReader(new InputStreamReader(iS));

            String line = "";
            while (line != null)
            {//if the line is null the reader has reached the end of the file
                line = bufferedReader.readLine();// line is a temp value, the line read by the bufferReader
                separatedTextFile.add(line);//add the new line to are array of lines
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (bufferedReader != null)
                    bufferedReader.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
        System.out.println("Mesh has been loaded in");


        return separatedTextFile;
    }

    private Mesh loadMesh(ArrayList<String> separatedTextFile)
    {
       return parser.parse(separatedTextFile);//call the parse method of the parser using the ArrayList of Lines in the file
    }


}
