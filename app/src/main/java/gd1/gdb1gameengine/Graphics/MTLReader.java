package gd1.gdb1gameengine.Graphics;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Ryan on 22/11/2016.
 */

public class MTLReader
{

    private BufferedReader bufferedReader;
    private ArrayList<String> separatedTextFile;
    private final static String mMTL_NEWMTL = "newmtl";
    private final static String mMTL_KA = "Ka";
    private final static String mMTL_KD = "Kd";
    private final static String mMTL_KS = "Ks";
    private final static String mMTL_ILLUM = "illum";
    private final static String mMTL_D = "d";
    private final static String mMTL_NS = "Ns";
    private final static String mMTL_MAP_KD = "map_Kd";
    private final static String mMTL_MAP_D = "map_d";

    public MTLReader()
    {
        separatedTextFile = new ArrayList<>();
    }

    public void read(Context context, int fileName) throws IOException
    {
        try
        {

            InputStream iS = context.getResources().openRawResource(fileName);
            bufferedReader = new BufferedReader(new InputStreamReader(iS));
            separatedTextFile = new ArrayList<String>();
            String line = "";
            while (line != null)
            {//if the line is null the reader has reached the end of the file
                line = bufferedReader.readLine();// line is a temp value, the line read by the bufferReader
                separatedTextFile.add(line);//add the new line to are array of lines
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (bufferedReader != null)
                    bufferedReader.close();
            } catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }

        this.parse(separatedTextFile);
    }

    public void parse(ArrayList<String> file)
    {
        int line = 0;

        while (line < file.size())
        {
            if (file.get(line) != null && file.get(line).length() != 0)
            {
                if (file.get(line).startsWith(mMTL_KA))
                {

                } else if (file.get(line).startsWith(mMTL_D))
                {

                } else if (file.get(line).startsWith(mMTL_ILLUM))
                {

                } else if (file.get(line).startsWith(mMTL_KD))
                {

                } else if (file.get(line).startsWith(mMTL_KS))
                {

                } else if (file.get(line).startsWith(mMTL_MAP_D))
                {

                } else if (file.get(line).startsWith(mMTL_MAP_KD))
                {

                } else if (file.get(line).startsWith(mMTL_NEWMTL))
                {

                } else if (file.get(line).startsWith(mMTL_NS))
                {


                } else
                {

                }


            }
            line++;
        }

    }
}