package gd1.gdb1gameengine.SceneViews;

import android.view.MotionEvent;

import gd1.gdb1gameengine.GameObjects.Camera;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithTexture;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.ObjectHandler;
import gd1.gdb1gameengine.Util.UI.Button;
import gd1.gdb1gameengine.Util.UI.ButtonImages;

/**
 * Created by Ryan on 05/12/2016.
 */

public class MulipleCameraScene extends SceneLayouts
{
    private  MeshWithTexture woman;
    public MulipleCameraScene(float width, float height, ObjectHandler objectHandler)
    {
        super(width, height,objectHandler);
    }



    @Override
    public void createGameObjects()
    {

        woman = new MeshWithTexture(R.raw.woman, R.drawable.womantest, "woman");
        woman.scaleObject(new Vector3D(0.01f, 0.01f, 0.01f));
        woman.translate(new Vector3D(0f, 0f, 0.0f));
        woman.setDraw(true);

        mGameObjects.add(woman);

    }

    @Override
    public void createParticleEmitters()
    {

    }

    @Override
    public void createLightObjects()
    {

    }

    @Override
    public void createCameras()
    {
        Camera camera = new Camera(true, new Vector3D(2f, 0f, 1.5f), new Vector3D(0, 0, 0), new Vector3D(0, 1, 0), (float) 100 / (float) 100,
                .01f, 100f);

        Camera camera2 = new Camera(true, new Vector3D(0f, 3f, 3f), new Vector3D(0, 0, 0), new Vector3D(0, 1, 0), (float) 100 / (float) 100,
                .01f, 100f);

        Camera camera3 = new Camera(true, new Vector3D(-1f, 0f, -2f), new Vector3D(0, 0, 0), new Vector3D(0, 1, 0), (float) 100 / (float) 100,
                .01f, 100f);

        Camera camera4 = new Camera(true, new Vector3D(-2f, 0f, 5f), new Vector3D(0, 0, 0), new Vector3D(0, 1, 0), (float) 100 / (float) 100,
                .01f, 100f);
        mCameras.add(camera);
        mCameras.add(camera2);
        mCameras.add(camera3);
        mCameras.add(camera4);


    }

    @Override
    public void createSkybox()
    {

    }

    @Override
    public void createHudElements()
    {

    }

    @Override
    public void createButtons()
    {

        mButtons.add(new Button("camera", new Vector3D(0.7f, 0.62f, 0), 0.25f, 0.15f));
        mButtons.add(new Button("camera1", new Vector3D(0.7f, 0.42f, 0), 0.25f, 0.15f));
        mButtons.add(new Button("camera2", new Vector3D(0.7f, 0.22f, 0), 0.25f, 0.15f));
        mButtons.add(new Button("camera3", new Vector3D(0.7f, 0.02f, 0), 0.25f, 0.15f));
        mButtons.add(new Button("rotate", new Vector3D(0.7f, -0.6f, 0), 0.25f, 0.15f));

        ButtonImages cameraButton1 = new ButtonImages(R.raw.circularbutton, R.drawable.camerabutton, "circle", mButtons.get(0));
        cameraButton1.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        cameraButton1.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        cameraButton1.getOrientation().addRotation(90);
        mHudElements.add(cameraButton1);

        ButtonImages cameraButton2 = new ButtonImages(R.raw.circularbutton, R.drawable.camerabutton, "circle", mButtons.get(1));
        cameraButton2.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        cameraButton2.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        cameraButton2.getOrientation().addRotation(90);
        mHudElements.add(cameraButton2);


        ButtonImages cameraButton3 = new ButtonImages(R.raw.circularbutton, R.drawable.camerabutton, "circle", mButtons.get(2));
        cameraButton3.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        cameraButton3.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        cameraButton3.getOrientation().addRotation(90);
        mHudElements.add(cameraButton3);


        ButtonImages cameraButton4 = new ButtonImages(R.raw.circularbutton, R.drawable.camerabutton, "circle", mButtons.get(3));
        cameraButton4.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        cameraButton4.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        cameraButton4.getOrientation().addRotation(90);
        mHudElements.add(cameraButton4);


        ButtonImages rotate = new ButtonImages(R.raw.circularbutton, R.drawable.button, "circle", mButtons.get(4));
        rotate.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        rotate.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        rotate.getOrientation().addRotation(90);
        mHudElements.add(rotate);



    }

    @Override
    public void buttonFunctionality(String buttonName, MotionEvent event)
    {
        switch (event.getAction() & MotionEvent.ACTION_MASK)
        {
            case MotionEvent.ACTION_UP:

                switch (buttonName)
                {//addin responses for each button
                    case "camera":

                        mObjectHandler.setActiveCamera(mCameras.get(0));

                        break;

                    case "camera1":

                        mObjectHandler.setActiveCamera(mCameras.get(1));

                        break;

                    case "camera2":

                        mObjectHandler.setActiveCamera(mCameras.get(2));

                        break;

                    case "camera3":

                        mObjectHandler.setActiveCamera(mCameras.get(3));

                        break;

                    case "rotate":
                        woman.getOrientation().setRotationAxis(new Vector3D(0,1,0));
                        if(woman.isRotate()){

                            woman.setRotate(false);
                        }else
                        {
                            woman.setRotate(true);
                        }
                        break;
                }
        }
    }
}
