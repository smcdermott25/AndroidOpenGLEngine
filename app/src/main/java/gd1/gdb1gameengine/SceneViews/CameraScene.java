package gd1.gdb1gameengine.SceneViews;

import android.view.MotionEvent;

import gd1.gdb1gameengine.GameObjects.InstancedObjects.Grass;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.Ground;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.TrackingCamera;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.ObjectHandler;
import gd1.gdb1gameengine.Util.UI.Button;
import gd1.gdb1gameengine.Util.UI.ButtonImages;

/**
 * Created by Ryan on 28/11/2016.
 */

public class CameraScene extends SceneLayouts
{


    private TrackingCamera trackingCamera;
    private Ground ground, grass;
    private Grass grassStrands;
    private MeshWithTexture bear;
    private Vector3D bearPrev;

    public CameraScene(float width, float height , ObjectHandler objectHandler)
    {
        super(width, height, objectHandler);
    }


    @Override
    public void createGameObjects()
    {
        bear = new MeshWithTexture(R.raw.bear,R.drawable.bear, "bear");
        bearPrev = new Vector3D(0.08f, 0.08f, 0.08f);

        // bearPrev = new Vector3D(0.065f, 0.065f, 0.065f);
        bear.scaleObject(bearPrev);
        bear.translate(new Vector3D(0f, 0f, 0.0f));
        bear.getOrientation().setRotationAxis(new Vector3D(0,1,0));
        bear.getOrientation().addRotation(90);
        bear.setDraw(true);

        ground = new Ground(R.drawable.redcon);
        ground.translate(new Vector3D(0.0f, -0.1f, 0.0f));
        ground.setDraw(true);

        grass = new Ground(R.drawable.grassg);
        grass.getOrientation().setScale(new Vector3D(0.4f, 0.4f, 0.4f));
        grass.setDraw(true);

        grassStrands  = new Grass(R.raw.grass, R.drawable.grasstexture, "Grass");
        grassStrands.scaleObject(new Vector3D(0.1f, 0.1f, 0.2f));
        grassStrands.translate(new Vector3D(-1.0f, 0.1f, -1.0f));
        grassStrands.setDraw(true);


        mGameObjects.add(bear);
        mGameObjects.add(ground);
        mGameObjects.add(grass);
        mGameObjects.add(grassStrands);


    }

    @Override
    public void createParticleEmitters() {

    }

    @Override
    public void createLightObjects()
    {

    }

    @Override
    public void createCameras()
    {
        trackingCamera = new TrackingCamera(true, bear, new Vector3D(0, 2.0f, -2.5f), (float) 100/ (float) 100,
                1f, 100f);

        trackingCamera.setRotate(false);

      mCameras.add(trackingCamera);
    }

    @Override
    public void createSkybox()
    {

        mSkybox = new MeshWithTexture(R.raw.skybox,R.drawable.rocky_skybox, "skybox");
        mSkybox.scaleObject(new Vector3D(15f, 15f, 15f));
        mSkybox.translate(new Vector3D(0.0f, 0.0f, 0.0f));
        mSkybox.setDraw(true);

    }

    @Override
    public void createHudElements()
    {

    }

    @Override
    public void createButtons()
    {
        mButtons.add(new Button("down", new Vector3D(0f, -0.6f, 0), 0.25f, 0.15f));
        mButtons.add(new Button("up", new Vector3D(0f, -0.4f, 0), 0.25f, 0.15f));
        mButtons.add(new Button("left", new Vector3D(-.7f, -0.5f, 0), 0.25f, 0.15f));
        mButtons.add(new Button("right", new Vector3D(.7f, -0.5f, 0), 0.25f, 0.15f));


        ButtonImages up = new ButtonImages(R.raw.circularbutton, R.drawable.button, "circle", mButtons.get(0));
        up.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        up.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        up.getOrientation().addRotation(90);
        mHudElements.add(up);

        ButtonImages down = new ButtonImages(R.raw.circularbutton, R.drawable.button, "circle", mButtons.get(1));
        down.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        down.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        down.getOrientation().addRotation(90);
        mHudElements.add(down);

        ButtonImages left = new ButtonImages(R.raw.circularbutton, R.drawable.button, "circle", mButtons.get(2));
        left.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        left.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        left.getOrientation().addRotation(90);
        mHudElements.add(left);

        ButtonImages right = new ButtonImages(R.raw.circularbutton, R.drawable.button, "circle", mButtons.get(3));
        right.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        right.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        right.getOrientation().addRotation(90);
        mHudElements.add(right);


    }

    @Override
    public void buttonFunctionality(String buttonName, MotionEvent event)
    {
        switch (event.getAction() & MotionEvent.ACTION_MASK)
        {

            case MotionEvent.ACTION_DOWN:
                switch (buttonName)
                {
                    case "up":
                        trackingCamera.setUp(true);
                        break;

                    case "down":
                        trackingCamera.setDown(true);
                        break;

                    case "left":
                        trackingCamera.setLeft(true);
                        break;

                    case "right":
                        trackingCamera.setRight(true);
                        break;

                    default:
                        break;
                }
                break;


            case MotionEvent.ACTION_UP:

                switch (buttonName)
                {//addin responses for each button


                    case "up":
                        trackingCamera.setUp(false);
                        break;

                    case "down":
                        trackingCamera.setDown(false);
                        break;

                    case "left":
                        trackingCamera.setLeft(false);
                        break;

                    case "right":
                        trackingCamera.setRight(false);
                        break;

                }




    }

    }

}
