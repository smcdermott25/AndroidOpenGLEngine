package gd1.gdb1gameengine.SceneViews;

import android.util.Log;
import android.view.MotionEvent;

import gd1.gdb1gameengine.GameObjects.Camera;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.PlaneNormalMap;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.ObjectHandler;
import gd1.gdb1gameengine.Util.UI.Button;
import gd1.gdb1gameengine.Util.UI.ButtonImages;

public class NormalMapScene extends SceneLayouts {

    private Camera camera;
    private LightSource light;
    private PlaneNormalMap plane;
    //private ParticleEmitter particleEmitter;

    public NormalMapScene(float width, float height, ObjectHandler objectHandler) {
        super( width, height,objectHandler);
    }


    @Override
    public void createGameObjects() {

        // Create a white light
        light = new LightSource(new Vector3D(1f, 1f, .8f));
        light.setLightPosition(new Vector3D(0, 0, 0f));

        light.scaleObject(new Vector3D(0.002f, 0.002f, 0.002f));
        //light.translate(new Vector3D(0.2f, 0f, -0.1f));

        light.setDraw(true);
      //  light.setMove(true);
       // light.setLightColor(new Vector3D(0.3f,0.4f,0.6f));
        mLightSources.add(light);

        plane = new PlaneNormalMap(light, R.drawable.bricktexture, new Vector3D(0, 0, 0));
        plane.scaleObject(new Vector3D(2, 2, 2));
        //plane.translate(new Vector3D(0,0,1));
        plane.setDraw(true);
        mGameObjects.add(plane);

    }





    @Override
    public void createLightObjects() {



    }

    @Override
    public void createCameras() {
        camera = new Camera(true, new Vector3D(4f, 0f, 0.3f), plane.getPosition(), new Vector3D(-2, 0, 0), (float) 100 / (float) 100,
                .01f, 100f);

        mCameras.add(camera);
        plane.updateCameraPosition(camera.getCameraPosition());
    }


    @Override
    public void createSkybox() {
        mSkybox = new MeshWithTexture(R.raw.skybox, R.drawable.castle_skybox, "skybox");
        mSkybox.scaleObject(new Vector3D(15f, 15f, 15f));
        mSkybox.translate(new Vector3D(0.0f, 0.0f, 0.0f));
        mSkybox.getOrientation().setRotationAxis(new Vector3D(1,0,0));
        mSkybox.getOrientation().addRotation(90);
        mSkybox.setDraw(true);
    }

    @Override
    public void createHudElements() {


    }

    @Override
    public void createParticleEmitters() {


    }

    @Override
    public void createButtons() {
        mButtons.add(new Button("move", new Vector3D(0.7f, 0.62f, 0), 0.25f, 0.15f));


        ButtonImages move = new ButtonImages(R.raw.circularbutton, R.drawable.button, "circle", mButtons.get(0));
        move.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        move.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        move.getOrientation().addRotation(90);
        mHudElements.add(move);

    }

    @Override
    public void buttonFunctionality(String buttonName, MotionEvent event) {


        Log.d("touch of button: ", buttonName);
        switch (event.getAction() & MotionEvent.ACTION_MASK)
        {


            case MotionEvent.ACTION_UP:

                switch (buttonName)
                {//addin responses for each button

                    case "move":

                        if (light.isMove())
                        {
                            light.setMove(false);

                        } else
                        {
                            light.setMove(true);
                        }

                        break;
                }
        }
    }
}
