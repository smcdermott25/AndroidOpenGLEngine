package gd1.gdb1gameengine.SceneViews;

import android.view.MotionEvent;

import java.util.ArrayList;

import gd1.gdb1gameengine.GameObjects.Camera;
import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithTexture;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.Graphics.ParticleEffects.ParticleEmitter;
import gd1.gdb1gameengine.Util.ObjectHandler;
import gd1.gdb1gameengine.Util.UI.Button;
import gd1.gdb1gameengine.Util.UI.HudElement;

/**
 * Created by Ryan on 28/11/2016.
 */

public abstract class SceneLayouts
{
    protected ArrayList<LightSource> mLightSources;
    protected ArrayList<GameObject> mGameObjects;
    protected ArrayList<Button> mButtons;
    public ArrayList<ParticleEmitter> mParticleEmitters;
    protected ArrayList<Camera> mCameras;
    protected MeshWithTexture mSkybox;
    protected ArrayList<HudElement> mHudElements;
    protected float mWidth,mHeight;
    protected ObjectHandler mObjectHandler;
    protected ArrayList<Button>mConstantButtons;
    protected ArrayList<HudElement>mConstanthud;


    public SceneLayouts(float width, float height, ObjectHandler mObjectHandler)
    {

        mLightSources = new ArrayList<>();
        mGameObjects = new ArrayList<>();
        mButtons = new ArrayList<>();
        mParticleEmitters = new ArrayList<>();
        mCameras = new ArrayList<>();
        mHudElements = new ArrayList<>();
        mWidth = width;
        mHeight = height;
        this.mObjectHandler = mObjectHandler;
        mConstanthud = new ArrayList<>();
        mConstantButtons = new ArrayList<>();



        createGameObjects();
        createParticleEmitters();
        createCameras();
        createLightObjects();
        createSkybox();
        createHudElements();
        createButtons();


    }



    public abstract void createGameObjects();

    public abstract void createParticleEmitters();

    public abstract void createLightObjects();

    public abstract void createCameras();

    public abstract void createSkybox();

    public abstract void createHudElements();

    public abstract void createButtons();

    public abstract void buttonFunctionality(String buttonName, MotionEvent event);


    public ArrayList<Camera> getCameras()
    {
        return mCameras;
    }

    public ArrayList<ParticleEmitter> getParticleEmitters(){ return mParticleEmitters; }

    public ArrayList<LightSource> getLightSources()
    {
        return mLightSources;
    }

    public ArrayList<GameObject> getGameObjects()
    {
        return mGameObjects;
    }

    public ArrayList<Button> getButtons()
    {
        return mButtons;
    }

    public MeshWithTexture getSkybox() { return mSkybox; }

    public ArrayList<HudElement> getHudElements()
    {
        return mHudElements;
    }

    public void setmHeight(float mHeight)
    {
        this.mHeight = mHeight;
    }

    public void setmWidth(float mWidth)
    {
        this.mWidth = mWidth;
    }
}
