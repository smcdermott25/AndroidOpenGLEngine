package gd1.gdb1gameengine.SceneViews;

import android.util.Log;
import android.view.MotionEvent;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineMainActivity;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.Square;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.TrackingCamera;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.ObjectHandler;
import gd1.gdb1gameengine.Util.UI.Button;

public class Scene4 extends SceneLayouts
{
    private MeshWithTexture bear;
    TrackingCamera trackingCamera;
    Square square;
    LightSource light;

    public Scene4(float width, float height, ObjectHandler objectHandler)
    {
        super(width, height, objectHandler);
    }



    @Override
    public void createGameObjects()
    {
        bear = new MeshWithTexture(R.raw.bear,R.drawable.bear, "bear");
        bear.scaleObject(new Vector3D(0.25f,0.25f,0.25f));
        bear.translate(new Vector3D(0f, 0f, 0.0f));
        bear.setDraw(true);
        mGameObjects.add(bear);




    }

    @Override
    public void createParticleEmitters() {

    }

    @Override
    public void createLightObjects()
    {

        light = new LightSource(new Vector3D(0.3f, 0.5f, 1.0f));
        light.setLightPosition(new Vector3D(0f,10f,-2f));
        light.scaleObject(new Vector3D(0.05f,0.05f,0.05f));
        light.setDraw(true);
        mLightSources.add(light);
        Log.d("Debug", "width and height are: " + super.mWidth + " " + super.mHeight);
        square = new Square((int)super.mWidth, (int)super.mHeight, mGameObjects, light);
        square.scaleObject(new Vector3D(4f,4f,4f));
        square.translate(new Vector3D(-8f,0f,-8f));
        square.setDraw(true);
        mGameObjects.add(square);


        for (LightSource a:mLightSources)
        {
            a.setDraw(true);

        }

    }

    @Override
    public void createCameras()
    {
        trackingCamera  = new TrackingCamera(true, bear, new Vector3D(0f,15f,15f), (float)100/(float) 100, 1f,100f);
        mCameras.add(trackingCamera);


    }

    @Override
    public void createSkybox()
    {
        /*
        int[] texturesToLoad = new int [6];

        texturesToLoad[0] = R.drawable.frozen_ft;
        texturesToLoad[1] = R.drawable.frozen_bk;
        texturesToLoad[2] = R.drawable.frozen_lf;
        texturesToLoad[3] = R.drawable.frozen_rt;
        texturesToLoad[4] = R.drawable.frozen_up;
        texturesToLoad[5] = R.drawable.frozen_dn;

        mObjectHandler.setSkybox(new Skybox(R.raw.skyboxvertex, R.raw.skyboxfragment, texturesToLoad));
        */
    }

    @Override
    public void createHudElements() {

    }

    @Override
    public void createButtons()
    {

        //  buttonsToDraw.add(  new Button("top",new float[]{ 0.6f,  -0.38f}, new float[]{0.7f, -0.48f}));
        //   mButtons.add(new Button("bottom", new Vector3D(0.6f, -0.55f,0), new Vector3D(0.7f, -0.65f,0)));
        //   mButtons.add(new Button("left", new Vector3D(0.43f, -0.48f,0), new Vector3D(0.57f, -0.55f,0)));
        mButtons.add(new Button("right", new Vector3D(0.7f, -0.48f,0),0,0));

        // mButtons.add(new Button("light", new Vector3D(-0.9f, -0.8f,0), new Vector3D(-0.8f, -0.9f,0)));
        // mButtons.add(new Button("model", new Vector3D(-0.75f, -0.8f,0), new Vector3D(-0.65f, -0.9f,0)));
        //  mButtons.add(new Button("texture", new Vector3D(-0.6f, -0.8f,0), new Vector3D(-.5f, -0.9f,0)));

        //  mButtons.add(new Button("grow", new Vector3D(0.75f, -0.8f,0), new Vector3D(0.85f, -0.9f,0)));
        //  mButtons.add(new Button("spin", new Vector3D(0.35f, -0.8f,0), new Vector3D(.45f, -0.9f,0)));

        //  mButtons.add(new Button("move", new Vector3D(0.7f, -0.65f,0), new Vector3D(.86f, -0.72f,0)));

        for (Button a:mButtons)
        {

            GameEngineMainActivity.manager.getInputManager().addButton(a);
        }


    }

    @Override
    public void buttonFunctionality(String buttonName, MotionEvent event)
    {

    }

}