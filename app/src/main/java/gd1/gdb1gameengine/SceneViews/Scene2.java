package gd1.gdb1gameengine.SceneViews;

import android.os.SystemClock;
import android.view.MotionEvent;

import java.util.Random;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView;
import gd1.gdb1gameengine.GameObjects.Camera;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithoutTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MultipleLitCube;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.TrackingCamera;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.Triangle;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.Graphics.ParticleEffects.ParticleEmitter;
import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.AssetManager;
import gd1.gdb1gameengine.Util.ObjectHandler;
import gd1.gdb1gameengine.Util.UI.Button;

/**
 * Created by Ryan on 28/11/2016.
 */

public class Scene2 extends SceneLayouts
{

    private Triangle triangle;
    private Camera camera;
    private TrackingCamera trackingCamera;
    private LightSource light;
    private LightSource lightTwo;
    private LightSource lightThree;
    private LightSource lightFour;
    private AssetManager manager;
    private MultipleLitCube cube;
    private Random random = new Random();
    private GameEngineSurfaceView gameEngineSurfaceView;

    double time = SystemClock.currentThreadTimeMillis();
    double time2 = SystemClock.currentThreadTimeMillis();
    Button button;
    boolean rotatingLeft;
    boolean rotatingRight;
    private MeshWithTexture bear, bear1, cube1;
    private ParticleEmitter particleEmitter;
    private MeshWithoutTexture cube2;
    private boolean grow, rotate, moving;
    private Vector3D bearPrev;
    private float multiplier = 1;
    private float multiplier1 = 1;


    public Scene2(float width, float height, ObjectHandler objectHandler)
    {
        super(width, height,objectHandler);

    }



    @Override
    public void createGameObjects()
    {
        cube1 = new MeshWithTexture(R.raw.skybox,R.drawable.group_photo, "Tcube");
        cube1.setDraw(true);
        cube1.scaleObject(new Vector3D(0.7f, 0.7f, 0.7f));
        cube1.translate(new Vector3D(0.9f, 8f, 1.0f));
        cube1.setColour(new Vector3D(.1f, .4f, .4f));
        cube1.getOrientation().setRotationAxis(new Vector3D(1,0,1));
        cube1.setRotate(true);

        mGameObjects.add(mSkybox);
        mGameObjects.add(cube1);

    }

    @Override
    public void createParticleEmitters() {
     //   particleEmitter = new ParticleEmitter(50, 25, 0.3f, 4);

    //    mParticleEmitters.add(particleEmitter);
    }
    @Override
    public void createLightObjects ()
    {

        }

    @Override
    public void createCameras()
    {
        trackingCamera = new TrackingCamera(true, cube1, new Vector3D(0, 2.0f, -2.5f), (float) 100/ (float) 100,
                1f, 100f);
        mCameras.add(trackingCamera);

    }

    @Override
    public void createSkybox()
    {
        mSkybox = new MeshWithTexture(R.raw.skybox,R.drawable.dark_skybox, "skybox");
        mSkybox.scaleObject(new Vector3D(15f, 15f, 15f));
        mSkybox.translate(new Vector3D(0.0f, 0.0f, 0.0f));
        mSkybox.setDraw(true);
    }

    @Override
    public void createHudElements()
    {

    }

    @Override
    public void createButtons()
    {

    }
    @Override
    public void buttonFunctionality(String buttonName, MotionEvent event)
    {

    }

}

