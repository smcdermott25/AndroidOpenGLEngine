package gd1.gdb1gameengine.SceneViews;

import android.os.SystemClock;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.Random;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.Ground;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithoutTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MultipleLitCube;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.TrackingCamera;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Skybox;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.Graphics.ParticleEffects.ParticleEmitter;
import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.AssetManager;
import gd1.gdb1gameengine.Util.ObjectHandler;
import gd1.gdb1gameengine.Util.UI.Button;
import gd1.gdb1gameengine.Util.UI.ButtonImages;
import gd1.gdb1gameengine.Util.UI.Element;
import gd1.gdb1gameengine.Util.UI.HudElement;

/**
 * Created by Ryan on 28/11/2016.
 */

public class Scene1 extends SceneLayouts
{

    private TrackingCamera trackingCamera;
    private Skybox skybox;
    private LightSource light;
    private LightSource lightTwo;
    private LightSource lightThree;
    private LightSource lightFour;
    private AssetManager manager;
    private MultipleLitCube cube;
    private Random random = new Random();
    private GameEngineSurfaceView gameEngineSurfaceView;

    double time = SystemClock.currentThreadTimeMillis();
    double time2 = SystemClock.currentThreadTimeMillis();
    Button button;
    boolean rotatingLeft;
    boolean rotatingRight;
    private MeshWithTexture bear, bear1,cube5;
    private Ground ground;
    private MeshWithoutTexture cube1;
    private boolean grow, rotate, moving;
    private Vector3D bearPrev;
    private float multiplier = 1;
    private float multiplier1 = 1;
    private ButtonImages cube2;





    private ParticleEmitter particleEmitter;


    public Scene1(float width, float height, ObjectHandler objectHandler)
    {

        super(width,height,objectHandler);


    }



    @Override
    public void createGameObjects()
    {

        bear = new MeshWithTexture(R.raw.bear, R.drawable.bear , "bear");
        bear.scaleObject(new Vector3D(0.08f, 0.08f, 0.08f));
        bear.translate(new Vector3D(0f, 0f, 0.0f));
        bear.setDraw(true);

        ground = new Ground(R.drawable.snow);
        ground.translate(new Vector3D(0.0f, -0.1f, 0.0f));
        ground.setDraw(true);

        mGameObjects.add(bear);
        mGameObjects.add(ground);


    }


    @Override
    public void createLightObjects()
    {

    }

    @Override
    public void createCameras()
    {
        trackingCamera = new TrackingCamera(true, bear, new Vector3D(0, 2.0f, -2.5f), (float) 100 / (float) 100,
                1f, 100f);
        trackingCamera.setRotate(false);
        mCameras.add(trackingCamera);
        //     mObjectHandler.addCamera(trackingCamera);
    }

    @Override
    public void createSkybox()
    {
        mSkybox = new MeshWithTexture(R.raw.skybox, R.drawable.ice_skybox, "skybox");
        mSkybox.scaleObject(new Vector3D(15f, 15f, 15f));
        mSkybox.translate(new Vector3D(0.0f, 0.0f, 0.0f));
        mSkybox.setDraw(true);
    }

    @Override
    public void createHudElements()
    {
        Element bottomPanel = new Element(R.raw.lightbutton, "lightyi");
        bottomPanel.translate(new Vector3D(0, -0.9f, 0f));
        bottomPanel.setColour(new Vector3D(0, 0, 0));
        bottomPanel.scale(new Vector3D(.2f, 0, 0.02f));
        bottomPanel.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        bottomPanel.getOrientation().addRotation(90);
        getmConstanthud().add(bottomPanel);

    }

    @Override
    public void createParticleEmitters()
    {
        //  particleEmitter = new ParticleEmitter(1, 25, 0.3f, 4);
        // mParticleEmitters.add(particleEmitter);

    }

    @Override
    public void createButtons()
    {

        mConstantButtons.add(new Button("bear", new Vector3D(0.42f, -0.82f, 0), 0.25f, 0.15f));
        mConstantButtons.add(new Button("end", new Vector3D(0.72f, -0.82f, 0), 0.25f, 0.15f));
        mConstantButtons.add(new Button("light", new Vector3D(-0.7f, -0.82f, 0), 0.25f, 0.15f));
        mConstantButtons.add(new Button("music", new Vector3D(0.75f, 0.82f, 0), 0.15f, 0.15f));
        mConstantButtons.add(new Button("photoshoot", new Vector3D(.12f, -0.82f, 0), 0.25f, 0.15f));
        mConstantButtons.add(new Button("grass", new Vector3D(-0.99f, -0.82f, 0), 0.25f, 0.15f));
        mConstantButtons.add(new Button("normals", new Vector3D(-0.4f, -0.82f, 0), 0.25f, 0.15f));
        mButtons.add(new Button("rotate", new Vector3D(0.7f, -0.6f, 0), 0.25f, 0.15f));
        mButtons.add(new Button("scale", new Vector3D(0.7f, -0.4f, 0), 0.25f, 0.15f));


        ButtonImages cube2 = new ButtonImages(R.raw.lightbutton, R.drawable.bearbutton, "button", mConstantButtons.get(0));
        cube2.scale(new Vector3D(0.025f, 0f, 0.015f));
        cube2.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        cube2.getOrientation().addRotation(90);
        mConstanthud.add(cube2);

        ButtonImages lightButton = new ButtonImages(R.raw.lightbutton, R.drawable.lightbutton, "button", mConstantButtons.get(2));
        lightButton.scale(new Vector3D(0.025f, 0.015f, 0.015f));
        lightButton.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        lightButton.getOrientation().addRotation(90);
        mConstanthud.add(lightButton);

        ButtonImages modelButton = new ButtonImages(R.raw.lightbutton, R.drawable.theendbutton, "button", mConstantButtons.get(1));
        modelButton.scale(new Vector3D(0.025f, 0.015f, 0.015f));
        modelButton.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        modelButton.getOrientation().addRotation(90);
        mConstanthud.add(modelButton);

        ButtonImages grassButton = new ButtonImages(R.raw.lightbutton, R.drawable.grassbutton, "button", mConstantButtons.get(5));
        grassButton.scale(new Vector3D(0.025f, 0.015f, 0.015f));
        grassButton.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        grassButton.getOrientation().addRotation(90);
        mConstanthud.add(grassButton);

        ButtonImages normalButton = new ButtonImages(R.raw.lightbutton, R.drawable.normalbutton, "button", mConstantButtons.get(6));
        normalButton.scale(new Vector3D(0.025f, 0.015f, 0.015f));
        normalButton.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        normalButton.getOrientation().addRotation(90);
        mConstanthud.add(normalButton);

        ButtonImages photoshoot = new ButtonImages(R.raw.lightbutton, R.drawable.camerabutton, "button", mConstantButtons.get(4));
        photoshoot.scale(new Vector3D(0.025f, 0.015f, 0.015f));
        photoshoot.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        photoshoot.getOrientation().addRotation(90);
        mConstanthud.add(photoshoot);

        ButtonImages musicButton = new ButtonImages(R.raw.circularbutton, R.drawable.musicbutton, "circle", mConstantButtons.get(3));
        musicButton.scale(new Vector3D(0.09f, 0.09f, 0.09f));
        musicButton.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        musicButton.getOrientation().addRotation(90);
        mConstanthud.add(musicButton);

        ButtonImages rotate = new ButtonImages(R.raw.circularbutton, R.drawable.button, "circle", mButtons.get(0));
        rotate.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        rotate.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        rotate.getOrientation().addRotation(90);
        mHudElements.add(rotate);

        ButtonImages scale = new ButtonImages(R.raw.circularbutton, R.drawable.button, "circle", mButtons.get(1));
        scale.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        scale.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        scale.getOrientation().addRotation(90);
        mHudElements.add(scale);


    }

    @Override
    public void buttonFunctionality(String buttonName, MotionEvent event)
    {
        switch (event.getAction() & MotionEvent.ACTION_MASK)
        {
            case MotionEvent.ACTION_UP:

                switch (buttonName)
                {//addin responses for each button


                    case "rotate":
                        bear.getOrientation().setRotationAxis(new Vector3D(0,1,0));
                        if(bear.isRotate()){


                            bear.setRotate(false);
                        }else
                        {
                            bear.setRotate(true);
                        }
                        break;

                    case "scale":
                       if(bear.isScale()){
                           bear.setScale(false);
                       }else
                       {
                           bear.setScale(true);
                       }
                        break;
                }
        }
    }

    public ArrayList<Button> getConstantButtons()
    {
        return mConstantButtons;
    }

    public ArrayList<HudElement> getmConstanthud()
    {
        return mConstanthud;
    }
}
