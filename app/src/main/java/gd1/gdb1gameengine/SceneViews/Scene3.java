package gd1.gdb1gameengine.SceneViews;

import android.os.SystemClock;
import android.view.MotionEvent;

import java.util.Random;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView;
import gd1.gdb1gameengine.GameObjects.Camera;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.Grass;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.Ground;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithoutTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MultipleLitCube;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.TrackingCamera;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.Triangle;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.AssetManager;
import gd1.gdb1gameengine.Util.ObjectHandler;
import gd1.gdb1gameengine.Util.UI.Button;

/**
 * Created by Ryan on 28/11/2016.
 */

public class Scene3 extends SceneLayouts
{

    private Triangle triangle;
    private Camera camera;
    private TrackingCamera trackingCamera;
    private LightSource light, lightTwo, lightThree, lightFour;
    private Ground ground, grass;
    private Grass grassStrands;
    private AssetManager manager;
    private MultipleLitCube cube;
    private Random random = new Random();
    private GameEngineSurfaceView gameEngineSurfaceView;

    double time = SystemClock.currentThreadTimeMillis();
    double time2 = SystemClock.currentThreadTimeMillis();
    Button button;
    boolean rotatingLeft;
    boolean rotatingRight;
    private MeshWithTexture deer, bear1,cube1;
    private MeshWithoutTexture cube2;
    private boolean grow, rotate, moving;
    private Vector3D deerPrev;
    private float multiplier = 1;
    private float multiplier1 = 1;


    public Scene3(float width, float height , ObjectHandler objectHandler)
    {
        super(width, height, objectHandler);
    }


    @Override
    public void createGameObjects()
    {
        deer = new MeshWithTexture(R.raw.deer,R.drawable.deertexture, "deer");
        deerPrev = new Vector3D(0.06f, 0.06f, 0.06f);

        // bearPrev = new Vector3D(0.065f, 0.065f, 0.065f);
        deer.scaleObject(deerPrev);
        deer.translate(new Vector3D(0f, 0f, 0.0f));
        deer.setDraw(true);

        ground = new Ground(R.drawable.redcon);
        ground.translate(new Vector3D(0.0f, -0.1f, 0.0f));
        ground.setDraw(true);

        grass = new Ground(R.drawable.grassg);
        grass.getOrientation().setScale(new Vector3D(0.4f, 0.4f, 0.4f));
        grass.setDraw(true);

        grassStrands  = new Grass(R.raw.grass, R.drawable.grasstexture, "Grass");
        grassStrands.scaleObject(new Vector3D(0.1f, 0.1f, 0.2f));
        grassStrands.translate(new Vector3D(-1.0f, 0.1f, -1.0f));
        grassStrands.setDraw(true);

        mGameObjects.add(cube);
        mGameObjects.add(deer);
        mGameObjects.add(ground);
        mGameObjects.add(grass);
        mGameObjects.add(grassStrands);


    }

    @Override
    public void createParticleEmitters() {

    }

    @Override
    public void createLightObjects()
    {

    }

    @Override
    public void createCameras()
    {
        trackingCamera = new TrackingCamera(true, deer, new Vector3D(0, 2.0f, -2.5f), (float) 100/ (float) 100,
                1f, 100f);

      mCameras.add(trackingCamera);
    }

    @Override
    public void createSkybox()
    {

        mSkybox = new MeshWithTexture(R.raw.skybox,R.drawable.rocky_skybox, "skybox");
        mSkybox.scaleObject(new Vector3D(15f, 15f, 15f));
        mSkybox.translate(new Vector3D(0.0f, 0.0f, 0.0f));
        mSkybox.setDraw(true);

    }

    @Override
    public void createHudElements()
    {

    }

    @Override
    public void createButtons()
    {



    }

    @Override
    public void buttonFunctionality(String buttonName, MotionEvent event)
    {

    }

}
