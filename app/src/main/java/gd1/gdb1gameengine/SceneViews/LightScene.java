package gd1.gdb1gameengine.SceneViews;

import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;

import java.util.Random;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView;
import gd1.gdb1gameengine.GameObjects.Camera;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.Grass;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.Ground;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MeshWithoutTexture;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.MultipleLitCube;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.TrackingCamera;
import gd1.gdb1gameengine.GameObjects.InstancedObjects.Triangle;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.AssetManager;
import gd1.gdb1gameengine.Util.ObjectHandler;
import gd1.gdb1gameengine.Util.UI.Button;
import gd1.gdb1gameengine.Util.UI.ButtonImages;

/**
 * Created by Ryan on 05/12/2016.
 */

public class LightScene extends SceneLayouts
{

    private Triangle triangle;
    private Camera camera;
    private TrackingCamera trackingCamera;
    private LightSource light, lightTwo, lightThree, lightFour;
    private Ground ground, grass;
    private Grass grassStrands;
    private AssetManager manager;
    private MultipleLitCube cube;
    private Random random = new Random();
    private GameEngineSurfaceView gameEngineSurfaceView;

    double time = SystemClock.currentThreadTimeMillis();
    double time2 = SystemClock.currentThreadTimeMillis();
    Button button;
    boolean rotatingLeft;
    boolean rotatingRight;
    private MeshWithTexture bear, bear1, cube1;
    private MeshWithoutTexture cube2;
    private boolean grow, rotate, moving;
    private Vector3D bearPrev;
    private float multiplier = 1;
    private float multiplier1 = 1;

    public LightScene(float width, float height, ObjectHandler mObjectHandler)
    {
        super(width, height, mObjectHandler);
    }



    @Override
    public void createGameObjects()
    {
        bear = new MeshWithTexture(R.raw.bear, R.drawable.bear, "bear");
        bearPrev = new Vector3D(0.0f, 0.01f, 0.01f);

        // bearPrev = new Vector3D(0.065f, 0.065f, 0.065f);
        bear.scaleObject(bearPrev);
        bear.translate(new Vector3D(0f, 0f, 0.0f));


        mGameObjects.add(bear);

    }

    @Override
    public void createParticleEmitters()
    {

    }

    @Override
    public void createLightObjects()
    {

        // Create a white light
        light = new LightSource(new Vector3D(0f, .255f, .127f));
        lightTwo = new LightSource(new Vector3D(.255f, .255f, 0f));
        lightThree = new LightSource(new Vector3D(0.88f, .09f, .09f));
        lightFour = new LightSource(new Vector3D(1.f, .09f, .80f));


        light.scaleObject(new Vector3D(0.002f, 0.002f, 0.002f));
        light.translate(new Vector3D(0.5f, 1.1f, -1.1f));

        lightTwo.scaleObject(new Vector3D(0.002f, 0.002f, 0.002f));
        lightTwo.translate(new Vector3D(0.1f, 1.1f, -1.1f));

        lightThree.scaleObject(new Vector3D(0.002f, 0.002f, 0.002f));
        lightThree.translate(new Vector3D(0.1f, 1.1f, -1.1f));

        lightFour.scaleObject(new Vector3D(0.002f, 0.002f, 0.002f));
        lightFour.translate(new Vector3D(0.1f, 1.1f, -1.1f));

        light.setDraw(true);
        lightTwo.setDraw(true);
        lightThree.setDraw(true);
        lightFour.setDraw(true);
        mLightSources.add(light);
        mLightSources.add(lightTwo);
        mLightSources.add(lightThree);
        mLightSources.add(lightFour);

        cube = new MultipleLitCube(new LightSource[]{light, lightTwo, lightThree, lightFour},
                trackingCamera);
        cube.scaleObject(new Vector3D(0.002f, 0.002f, 0.002f));
        cube.getOrientation().setScale(new Vector3D(1f, 1f, 1f));
        cube.getOrientation().setPositionY(1.0f);
        cube.setDraw(true);
        // cube.setRotate(true);
        mGameObjects.add(cube);

    }

    @Override
    public void createCameras()
    {
        trackingCamera = new TrackingCamera(true, bear, new Vector3D(0, 2.0f, -2.5f), (float) 100 / (float) 100,
                1f, 100f);

        mCameras.add(trackingCamera);
    }

    @Override
    public void createSkybox()
    {
        mSkybox = new MeshWithTexture(R.raw.skybox, R.drawable.dark_skybox, "skybox");
        mSkybox.scaleObject(new Vector3D(15f, 15f, 15f));
        mSkybox.translate(new Vector3D(0.0f, 0.0f, 0.0f));
        mSkybox.setDraw(true);
    }

    @Override
    public void createHudElements()
    {


    }

    @Override
    public void createButtons()
    {
        mButtons.add(new Button("camera", new Vector3D(0.7f, 0.62f, 0), 0.25f, 0.15f));
        mButtons.add(new Button("camera1", new Vector3D(0.7f, 0.42f, 0), 0.25f, 0.15f));

        ButtonImages cameraButton1 = new ButtonImages(R.raw.circularbutton, R.drawable.button, "circle", mButtons.get(0));
        cameraButton1.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        cameraButton1.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        cameraButton1.getOrientation().addRotation(90);
        mHudElements.add(cameraButton1);

        ButtonImages cameraButton2 = new ButtonImages(R.raw.circularbutton, R.drawable.button, "circle", mButtons.get(1));
        cameraButton2.scale(new Vector3D(0.14f, 0.09f, 0.09f));
        cameraButton2.getOrientation().setRotationAxis(new Vector3D(1, 0, 0));
        cameraButton2.getOrientation().addRotation(90);

        cameraButton2.getOrientation().setRotationAxis(new Vector3D(0, 1, 0));
        cameraButton2.getOrientation().addRotation(2);
        mHudElements.add(cameraButton2);
    }

    @Override
    public void buttonFunctionality(String buttonName, MotionEvent event)
    {
        Log.d("touch of button: ", buttonName);
        switch (event.getAction() & MotionEvent.ACTION_MASK)
        {


            case MotionEvent.ACTION_UP:

                switch (buttonName)
                {//addin responses for each button

                    case "camera":

                        if (cube.isMove())
                        {
                            cube.setMove(false);

                        } else
                       {
                            cube.setMove(true);
                       }



                        break;

                    case "camera1":

                        if (cube.isRotate())
                        {
                            cube.setRotate(false);

                        } else
                        {
                            cube.setRotate(true);
                        }


                        break;
                }
        }
    }
}
