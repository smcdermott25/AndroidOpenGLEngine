package gd1.gdb1gameengine.CoreGameEngine;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import gd1.gdb1gameengine.Util.AssetManager;
import gd1.gdb1gameengine.Util.Logging.Logger;
import gd1.gdb1gameengine.Util.Sound.BackgroundMusic;

public class GameEngineMainActivity extends Activity
{

    private GLSurfaceView coreSurfaceView;
    //TODO Discuss if encapsulation of AssetManager is required
    public static AssetManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        final ActivityManager activityManager =
                (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configurationInfo =
                activityManager.getDeviceConfigurationInfo();
        final boolean supportsEs2 =
                configurationInfo.reqGlEsVersion >= 0x20000
                        || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1
                        && (Build.FINGERPRINT.startsWith("generic")
                        || Build.FINGERPRINT.startsWith("unknown")
                        || Build.MODEL.contains("google_sdk")
                        || Build.MODEL.contains("Emulator")
                        || Build.MODEL.contains("Android SDK built for x86")));
        if (supportsEs2) {
            // Request an OpenGL ES 2.0 compatible context.
            manager = new AssetManager(this);
            coreSurfaceView = new GameEngineSurfaceView(this);
            setContentView(coreSurfaceView);
        } else {
            Toast.makeText(this, "This device does not support OpenGL ES 2.0.",
            Toast.LENGTH_LONG).show();
            return;
        }


    }
}
