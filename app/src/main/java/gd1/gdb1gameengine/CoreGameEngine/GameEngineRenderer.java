package gd1.gdb1gameengine.CoreGameEngine;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import gd1.gdb1gameengine.Util.ObjectHandler;


/**
 * Created by 40101817 on 24/10/2016.
 */
public class GameEngineRenderer implements GLSurfaceView.Renderer
{

    private ObjectHandler handler;
    private GameEngineSurfaceView gameEngineSurfaceView;
    private boolean rendererCreationComplete = false;

    GameEngineRenderer(GameEngineSurfaceView mSurfaceView)
    {
        super();
        gameEngineSurfaceView = mSurfaceView;
    }


    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig)
    {
        Log.d("Debug", "width and height IN RENDERER are: " + gameEngineSurfaceView.getWidth() + " " + gameEngineSurfaceView.getHeight());
        handler = new ObjectHandler((float)gameEngineSurfaceView.getWidth(),(float)gameEngineSurfaceView.getHeight());
        handler.setUpScreens();
        handler.loadScene(0);
      //  scene = new Scene1(handler);
        GLES20.glClearColor(1.0f, 0.8f, 0.2f, 1.0f);
       // bgm = new BackgroundMusic(gameEngineSurfaceView.getContext(), "audio/mariachi.mp3");
//        bgm.play();
        rendererCreationComplete = true;
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height)
    {
        if (!rendererCreationComplete)
        {
            return;
        }
        if (handler.getActiveCamera() == null)
        {
            return;
        }

       // handler.getActiveCamera().setCameraRatio((float) width / (float) height, 1f, 100f);

        handler.setHeight((float) height);
        handler.setWidth((float) width);

        GLES20.glViewport(0, 0, width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl10)
    {

        if (!rendererCreationComplete)
        {
            return;
        }
        if (handler.getActiveCamera() == null)
        {
            return;
        }

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

       handler.draw();
        /*This must be the very last thing to happen in onDraw() - never move this*/
        gameEngineSurfaceView.notifyDrawCompleted();
    }

    public void update()
    {

        if (!rendererCreationComplete)
        {
            return;
        }
        if (handler.getActiveCamera() == null)
        {
            return;
        }
        if (handler.getActiveCamera() == null)
        {
            return;
        }
        handler.update();
    }
    public ObjectHandler getHandler()
    {
        return handler;
    }

}
