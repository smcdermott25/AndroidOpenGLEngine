package gd1.gdb1gameengine.GameObjects;

import android.opengl.Matrix;
import android.util.Log;

/**
 * Created by 40101817 on 24/10/2016.
 */


public class Camera
{
    private float viewMatrix[];
    private float projectionMatrix[];
    private float projectionViewMatrix[];
    private float orthoMatrix[];

    private int offset = 0;

    private Vector3D cameraPosition;
    private Vector3D cameraTarget;
    private Vector3D upVector;

    private float speedOfRotation = 0.01f;
    private float rotation = 0;


    // booleans for controls
    private boolean up, down, left, right;
    private boolean rotating = true;


    public Camera(boolean perspective,
                  Vector3D cameraPosition,
                  Vector3D cameraTarget,
                  Vector3D upVector,
                  float ratio,
                  float nearClippingPlane,
                  float farClippingPlane) {

        viewMatrix = new float[16];
        projectionMatrix = new float[16];
        projectionViewMatrix = new float[16];
        orthoMatrix = new float[16];

        // Set camera variables
        this.cameraPosition = cameraPosition;
        this.cameraTarget = cameraTarget;
        this.upVector = upVector;

        // Set camera position, looking at and up variables in world space
        // e.g. set the view matrix
        Matrix.setLookAtM(viewMatrix, offset, cameraPosition.x, cameraPosition.y,
                cameraPosition.z, cameraTarget.x, cameraTarget.y, cameraTarget.z, upVector.x,
                upVector.y, upVector.z);


        Matrix.orthoM(orthoMatrix, 0, -ratio, ratio, -1f, 1f, -1f,
                1f);

        // Figure out what type of camera being used e.g. set the projection matrix
        if (perspective)
        {
            Log.d("Debug", "Ratio is : " + ratio);
            // Create a perspective camera
            Matrix.frustumM(projectionMatrix, 0, -ratio, ratio, -1, 1, nearClippingPlane,
                    farClippingPlane);
        } else {
            // Create an orthographic camera
            Matrix.orthoM(projectionMatrix, 0, -1.0f, 1.0f, -1.0f, 1.0f, nearClippingPlane,
                    farClippingPlane);
        }

        calculateProjectionViewMatrix();
    }

    // Accessor methods
    public Vector3D getCameraPosition()
    {
        return cameraPosition;
    }

    public Vector3D getCameraTarget()
    {
        return cameraTarget;
    }

    public Vector3D getUpVector()
    {
        return upVector;
    }

    public float[] getProjectionMatrix()
    {
        return projectionMatrix;
    }

    public float[] getViewMatrix()
    {
        return viewMatrix;
    }

    public float[] getOrthoMatrix()
    {
        return orthoMatrix;
    }

    public float[] getProjectionViewMatrix()
    {
        return projectionViewMatrix;
    }

    // Mutator methods
    public void calculateNewProjection(float ratio, float nearClippingPlane, float farClippingPlane)
    {
        Matrix.frustumM(projectionMatrix, 0, -ratio, ratio, -1, 1, nearClippingPlane,
                farClippingPlane);
    }

    public void calculateNewOrtho(float width, float height, float nearClippingPlane, float farClippingPlane)
    {

        Matrix.orthoM(orthoMatrix, 0, -width, width, -height, height, nearClippingPlane,
                farClippingPlane);
    }

    public void calculateProjectionViewMatrix()
    {
        Matrix.multiplyMM(projectionViewMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
    }

    public void setCameraPosition(Vector3D cameraPosition)
    {
        this.cameraPosition.x = cameraPosition.x;
        this.cameraPosition.y = cameraPosition.y;
        this.cameraPosition.z = cameraPosition.z;

        Matrix.setLookAtM(viewMatrix, offset, cameraPosition.x, cameraPosition.y,
                cameraPosition.z, cameraTarget.x, cameraTarget.y, cameraTarget.z, upVector.x,
                upVector.y, upVector.z);
    }


    public void rotateAroundTarget(float radius)
    {
        // Tied to the speed of execution at the minute - will need refactoring later
        rotation += speedOfRotation;
        float xComponent = (float) Math.sin(rotation) * radius;
        float zComponent = (float) Math.cos(rotation) * radius;

        setCameraPosition(new Vector3D(xComponent, cameraPosition.y,zComponent));
        calculateProjectionViewMatrix();
    }

    public void rotateAroundTargetY(float radius)
    {
        // Tied to the speed of execution at the minute - will need refactoring later
        rotation += speedOfRotation;
        float xComponent = (float) Math.sin(rotation) * radius;
        float zComponent = (float) Math.cos(rotation) * radius;

        setCameraPosition(new Vector3D(xComponent, cameraPosition.y,zComponent));
        calculateProjectionViewMatrix();
    }

    public void setCameraTarget(Vector3D newCameraTarget)
    {
        this.cameraTarget.x = newCameraTarget.x;
        this.cameraTarget.y = newCameraTarget.y;
        this.cameraTarget.z = newCameraTarget.z;

        Matrix.setLookAtM(viewMatrix, offset, cameraPosition.x, cameraPosition.y,
                cameraPosition.z, cameraTarget.x, cameraTarget.y, cameraTarget.z, upVector.x,
                upVector.y, upVector.z);
    }

    public void setCameraRatio(float ratio, float nearClippingPlane, float farClippingPlane)
    {
        calculateNewProjection(ratio, nearClippingPlane, farClippingPlane);
    }

    // Controls for camera
    public void moveLeft()
    {
        // Move camera position
        Vector3D movement = new Vector3D(cameraPosition.x + 0.1f, cameraPosition.y, cameraPosition.z);
        setCameraPosition(movement);

        // Change camera's target
        Vector3D target = new Vector3D(cameraTarget.x + 0.1f, cameraTarget.y, cameraTarget.z);
        setCameraTarget(target);

        calculateProjectionViewMatrix();
    }

    public void moveRight()
    {
        // Move camera position
        Vector3D movement = new Vector3D(cameraPosition.x - 0.1f, cameraPosition.y, cameraPosition.z);
        setCameraPosition(movement);

        // Change camera's target
        Vector3D target = new Vector3D(cameraTarget.x - 0.1f, cameraTarget.y, cameraTarget.z);
        setCameraTarget(target);

        calculateProjectionViewMatrix();
    }

    public void moveUp()
    {
        // Move camera position
        Vector3D movement = new Vector3D(cameraPosition.x, cameraPosition.y + 0.1f, cameraPosition.z);
        setCameraPosition(movement);

        // Change camera's target
        Vector3D target = new Vector3D(cameraTarget.x, cameraTarget.y + 0.1f, cameraTarget.z);
        setCameraTarget(target);

        calculateProjectionViewMatrix();
    }

    public void moveDown()
    {
        // Move camera position
        Vector3D movement = new Vector3D(cameraPosition.x, cameraPosition.y - 0.1f, cameraPosition.z);
        setCameraPosition(movement);

        // Change camera's target
        Vector3D target = new Vector3D(cameraTarget.x, cameraTarget.y - 0.1f, cameraTarget.z);
        setCameraTarget(target);

        calculateProjectionViewMatrix();
    }

    public void tiltLeft()
    {
        // Change camera's target
        Vector3D target = new Vector3D(cameraTarget.x - 0.01f, cameraTarget.y, cameraTarget.z);
        setCameraTarget(target);

        calculateProjectionViewMatrix();
    }

    public void tiltRight()
    {
        // Change camera's target
        Vector3D target = new Vector3D(cameraTarget.x + 0.01f, cameraTarget.y, cameraTarget.z);
        setCameraTarget(target);

        calculateProjectionViewMatrix();
    }

    public void tiltUp()
    {
        // Change camera's target
        Vector3D target = new Vector3D(cameraTarget.x, cameraTarget.y + 0.01f, cameraTarget.z);
        setCameraTarget(target);

        calculateProjectionViewMatrix();
    }

    public void tiltDown()
    {
        // Change camera's target
        Vector3D target = new Vector3D(cameraTarget.x, cameraTarget.y - 0.01f, cameraTarget.z);
        setCameraTarget(target);

        calculateProjectionViewMatrix();
    }

    public void setUp(boolean value)
    {
        up = value;
    }

    public boolean getUp()
    {
        return up;
    }

    public void setDown(boolean value)
    {
        down = value;
    }

    public boolean getDown()
    {
        return down;
    }

    public void setLeft(boolean value)
    {
        left = value;
    }

    public boolean getLeft()
    {
        return left;
    }

    public void setRight(boolean value)
    {
        right = value;
    }

    public boolean getRight()
    {
        return right;
    }

    public void setRotating(boolean value)
    {
        rotating = value;
    }

    public boolean getRotating()
    {
        return rotating;
    }

}


