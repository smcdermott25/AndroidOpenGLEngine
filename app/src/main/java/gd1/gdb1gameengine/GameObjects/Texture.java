package gd1.gdb1gameengine.GameObjects;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView;
import gd1.gdb1gameengine.R;


public class Texture {
    private int [] texture = new int[1];
    private int textureHandle;
    private int textureUniformHandle;
    private FloatBuffer textureBuffer;
    private int textureId;
    // get bear coords to use in texture buffer
    static float textureCoords[];
    public Texture(float textureCoords[], int textureId)
    {
        this.textureCoords = textureCoords;
        this.textureId = textureId;
        loadTexture(GameEngineSurfaceView.context);
    }
    public void loadTexture(Context parent)
    {
        System.out.println("TEXTURE ID: "+textureId);
        ByteBuffer tb = ByteBuffer.allocateDirect(
                textureCoords.length * 4 // 4 bytes per float
        );
        tb.order(ByteOrder.nativeOrder());
        textureBuffer = tb.asFloatBuffer();
        textureBuffer.put(textureCoords);
        textureBuffer.position(0);
        // Generate texture
        GLES20.glGenTextures(1, texture, 0);
        // Bind loaded texture
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture[0]);
        // Load image
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false; // no pre scaling

        Bitmap bitmap = BitmapFactory.decodeResource(parent.getResources(), textureId, options);
        // if texture coordinates are out of range
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_MIRRORED_REPEAT);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_MIRRORED_REPEAT);
        // Set filtering parameters for scaling
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        // Load the bitmap into the bound texture
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
        // generate mipmaps
        GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D);
        // Delete bitmap
        bitmap.recycle();
        // Unbind the texture
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }
    public void useTexture(int program)
    {

        //GLES20.glUseProgram(program);
       textureUniformHandle = GLES20.glGetUniformLocation(program, "texture");

        textureHandle = GLES20.glGetAttribLocation(program, "aTexturePosition");
        GLES20.glEnableVertexAttribArray(textureHandle);
        GLES20.glVertexAttribPointer(textureHandle, 2, GLES20.GL_FLOAT, false, 8, textureBuffer);
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture[0]);
        GLES20.glUniform1i(textureUniformHandle, 0);
    }
    public void useTexture(int program, boolean normal)
    {
        if (normal)
        {

            GLES20.glUseProgram(program);
            int normalUniformHandle = GLES20.glGetUniformLocation(program, "normalMap");


            GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + 2);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture[0]);
            GLES20.glUniform1i(normalUniformHandle, 2);

        }
    }
}