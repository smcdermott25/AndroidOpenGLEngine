package gd1.gdb1gameengine.GameObjects;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import java.util.ArrayList;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView;
import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.Shader;

/**
 * Created by mshaw on 28/11/2016.
 * This was a quick fire shadow attempt to get something workable for the demo, this is not yet functional
 * Created following and using code from this tutorial:
 * https://www.codeproject.com/articles/822380/shadow-mapping-with-android-opengl-es
 */

public class Shadow {

    public static final String SHADOW_POSITION_ATTRIBUTE = "aShadowPosition";
    public static final String MVP_MATRIX_UNIFORM = "uMVPMatrix";

    private float mShadowMapRatio = 1;
    private int mDisplayWidth, mDisplayHeight;
    private int mShadowMapWidth, mShadowMapHeight;
    private ArrayList<GameObject> mGameObjectsThatCastShadows;
    private LightSource mLightCastingShadow;
    private int shadow_positionAttribute;
    private int[] fboId;
    private int[] depthTextureId;
    private int[] renderTextureId;

    //private Shader  mPCFShadowProgram, mSimpleShadowDynamicBiasProgram, mPCFShadowDynamicBiasProgram;
    private Shader mDepthMapProgram;

    // Uniform locations for shadow render program
    private int shadow_mvpMatrixUniform;

    /**
     * MVP matrix used at rendering shadow map for the big cube in the center
     */
    private final float[] mLightMvpMatrix_dynamicShapes = new float[16];
    /**
     * View matrix of light source
     */
    private final float[] mLightViewMatrix = new float[16];
    /**
     * Projection matrix from point of light source
     */
    private final float[] mLightProjectionMatrix = new float[16];

    private final float[] mModelMatrix = new float[16];



    public Shadow(int displayWidth, int displayHeight, int shadowMapRatio, ArrayList<GameObject> gameObjectsThatCastShadows, LightSource lightCastingShadow) {
        mDisplayWidth = displayWidth;
        mDisplayHeight = displayHeight;
        mShadowMapRatio = shadowMapRatio;
        mGameObjectsThatCastShadows = gameObjectsThatCastShadows;
        mLightCastingShadow = lightCastingShadow;
        createShadowShaders();

    }

    public void createShadowShaders() {
        // OES_depth_texture is available -> shaders are simplier
        /*mSimpleShadowProgram = new Shader(
                GameEngineSurfaceView.context,
                R.raw.depth_tex_v_with_shadow,
                R.raw.depth_tex_f_with_simple_shadow, "simple shadow program");*/

       /* mPCFShadowProgram = new Shader(
                GameEngineSurfaceView.context,
                R.raw.depth_tex_v_with_shadow,
                R.raw.depth_tex_f_with_pcf_shadow);

        mSimpleShadowDynamicBiasProgram = new Shader(
                GameEngineSurfaceView.context,
                R.raw.depth_tex_v_with_shadow,
                R.raw.depth_tex_f_with_simple_shadow_dynamic_bias);

        mPCFShadowDynamicBiasProgram = new Shader(
                GameEngineSurfaceView.context,
                R.raw.depth_tex_v_with_shadow,
                R.raw.depth_tex_f_with_pcf_shadow_dynamic_bias);*/

        mDepthMapProgram = new Shader(
                GameEngineSurfaceView.context,
                R.raw.depth_tex_v_depth_map,
                R.raw.depth_tex_f_depth_map, "Depth map shader");
    }

    public void shadowDrawPreparations() {
        generateShadowFBO();
        Log.d("Shadow","We're not getting weird messages out of shadow FBO");
        calculateShadowProjectionMatrix();
        Log.d("Shadow", "We're not getting weird messages out of calculate Shadow projection Matrix");
        preShadowMapCalculations();
        Log.d("Shadow", "We're not getting errors out of preShadowMapCalculations");
        renderShadowMap();
        Log.d("Shadow", "We're not getting errors out of the renderShadowMap call");
    }


    public void generateShadowFBO() {
        Log.d("Debug", "Display width " + mDisplayWidth );
        Log.d("Debug", "Display height " + mDisplayHeight);
        mShadowMapWidth = Math.round(mDisplayWidth * mShadowMapRatio);
        mShadowMapHeight = Math.round(mDisplayHeight * mShadowMapRatio);

        fboId = new int[1];
        depthTextureId = new int[1];
        renderTextureId = new int[1];

        // create a framebuffer object
        GLES20.glGenFramebuffers(1, fboId, 0);

        // create render buffer and bind 16-bit depth buffer
        GLES20.glGenRenderbuffers(1, depthTextureId, 0);
        GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, depthTextureId[0]);
        GLES20.glRenderbufferStorage(GLES20.GL_RENDERBUFFER, GLES20.GL_DEPTH_COMPONENT16, mShadowMapWidth, mShadowMapHeight);

        // Try to use a texture depth component
        GLES20.glGenTextures(1, renderTextureId, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, renderTextureId[0]);

        // GL_LINEAR does not make sense for depth texture. However, next tutorial shows usage of GL_LINEAR and PCF. Using GL_NEAREST
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

        // Remove artifact on the edges of the shadowmap
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, fboId[0]);

        /*if (!mHasDepthTextureExtension) {
            GLES20.glTexImage2D( GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, mShadowMapWidth, mShadowMapHeight, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);

            // specify texture as color attachment
            GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, renderTextureId[0], 0);

            // attach the texture to FBO depth attachment point
            // (not supported with gl_texture_2d)
            GLES20.glFramebufferRenderbuffer(GLES20.GL_FRAMEBUFFER, GLES20.GL_DEPTH_ATTACHMENT, GLES20.GL_RENDERBUFFER, depthTextureId[0]);
        }
        else {*/
        // Use a depth texture
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_DEPTH_COMPONENT, mShadowMapWidth, mShadowMapHeight, 0,
                GLES20.GL_DEPTH_COMPONENT, GLES20.GL_UNSIGNED_INT, null);

        // Attach the depth texture to FBO depth attachment point
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_DEPTH_ATTACHMENT, GLES20.GL_TEXTURE_2D, renderTextureId[0], 0);
        //}

        // check FBO status
        int FBOstatus = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
        if (FBOstatus != GLES20.GL_FRAMEBUFFER_COMPLETE) {
            Log.e("Shadow Error", "GL_FRAMEBUFFER_COMPLETE failed, CANNOT use FBO, error is: " + FBOstatus);
            throw new RuntimeException("GL_FRAMEBUFFER_COMPLETE failed, CANNOT use FBO");
        }
    }

    public void renderShadowMap() {
        // bind the generated framebuffer
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, fboId[0]);

        GLES20.glViewport(0, 0, mShadowMapWidth,
                mShadowMapHeight);

        // Clear color and buffers
        GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);

        // Start using the shader
        GLES20.glUseProgram(mDepthMapProgram.getShaderProgram());

        float[] tempResultMatrix = new float[16];

        // Render all shapes on scene
        for (GameObject gameObjectThatCastsShadow : mGameObjectsThatCastShadows) {
            tempResultMatrix = gameObjectThatCastsShadow.getModelMatrix();
            // View matrix * Model matrix value is stored
            Matrix.multiplyMM(mLightMvpMatrix_dynamicShapes, 0, mLightViewMatrix, 0, tempResultMatrix, 0);

            // Model * view * projection matrix stored and copied for use at rendering from camera point of view
            Matrix.multiplyMM(tempResultMatrix, 0, mLightProjectionMatrix, 0, mLightMvpMatrix_dynamicShapes, 0);
            System.arraycopy(tempResultMatrix, 0, mLightMvpMatrix_dynamicShapes, 0, 16);

            // Pass in the combined matrix.
            GLES20.glUniformMatrix4fv(shadow_mvpMatrixUniform, 1, false, mLightMvpMatrix_dynamicShapes, 0);

            gameObjectThatCastsShadow.drawShadow(shadow_positionAttribute);
        }
    }

    public void preShadowMapCalculations() {
        int shadowMapProgram = mDepthMapProgram.getShaderProgram();
        shadow_mvpMatrixUniform = GLES20.glGetUniformLocation(shadowMapProgram, MVP_MATRIX_UNIFORM);
        shadow_positionAttribute = GLES20.glGetAttribLocation(shadowMapProgram, SHADOW_POSITION_ATTRIBUTE);

        //display texture program handles (for debugging depth texture)
        //texture_mvpMatrixUniform = GLES20.glGetUniformLocation(textureProgram, RenderConstants.MVP_MATRIX_UNIFORM);
        //texture_positionAttribute = GLES20.glGetAttribLocation(textureProgram, RenderConstants.POSITION_ATTRIBUTE);
        //texture_texCoordAttribute = GLES20.glGetAttribLocation(textureProgram, RenderConstants.TEX_COORDINATE);
        //texture_textureUniform = GLES20.glGetUniformLocation(textureProgram, RenderConstants.TEXTURE_UNIFORM);

        /*This code is for allowing the lightsource to be rotated, we don't currently support this but this could be easily added*/
        /*long elapsedMilliSec = SystemClock.elapsedRealtime();
        long rotationCounter = elapsedMilliSec % 12000L;

        float lightRotationDegree = (360.0f / 12000.0f) * ((int)rotationCounter);

        float[] rotationMatrix = new float[16];

        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.rotateM(rotationMatrix, 0, lightRotationDegree, 0.0f, 1.0f, 0.0f);

        Matrix.multiplyMV(mActualLightPosition, 0, rotationMatrix, 0, mLightPosModel, 0);*/

        Matrix.setIdentityM(mModelMatrix, 0);
        Matrix.setLookAtM(mLightViewMatrix, 0,
                //lightX, lightY, lightZ,
                mLightCastingShadow.getLightPosition().x, mLightCastingShadow.getLightPosition().y, mLightCastingShadow.getLightPosition().z,
                //lookX, lookY, lookZ,
                //look in direction -y
                mLightCastingShadow.getLightPosition().x, -mLightCastingShadow.getLightPosition().y, mLightCastingShadow.getLightPosition().z,
                //upX, upY, upZ
                //up vector in the direction of axisY
                // TODO: TRY THIS WITH ORIGINAL VALUEI changed this from -x, 0, -z
                -mLightCastingShadow.getLightPosition().x, 0, -mLightCastingShadow.getLightPosition().z);
    }


    public void setDisplayRatio(int width, int height) {
        mDisplayWidth = width;
        mDisplayHeight = height;
    }

    public void calculateShadowProjectionMatrix() {
        float ratio = (float) mDisplayWidth / mDisplayHeight;

        // this projection matrix is applied at rendering scene
        // in the onDrawFrame() method
        float bottom = -1.0f;
        float top = 1.0f;
        float near = 1.0f;
        float far = 100.0f;
        // this projection matrix is used at rendering shadow map
        Matrix.frustumM(mLightProjectionMatrix, 0, -1.1f * ratio, 1.1f * ratio, 1.1f * bottom, 1.1f * top, near, far);
    }

    public int getShadowMapWidth() {
        return mShadowMapWidth;
    }

    public int getShadowMapHeight() {
        return mShadowMapHeight;
    }

    public int getRenderTextureId() {
        return renderTextureId[0];
    }

    public float[] getmLightMvpMatrix()
    {
        return mLightMvpMatrix_dynamicShapes;
    }

}
