package gd1.gdb1gameengine.GameObjects;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;

import java.nio.ByteBuffer;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView;
import gd1.gdb1gameengine.Util.Shader;
import gd1.gdb1gameengine.Util.VertexArray;

/**
 * Created by wscam on 28/11/2016.
 */

public class Skybox
{

    protected Shader mShader;
    private float[] mModelViewMatrix;
    private float[] mMVPMatrix;
    private float[] mNormalMatrix;
    private float[] viewProjectionMatrix;
    private float[] mNormalMatrixInverse;
    private Orientation orientation;
    ByteBuffer indices;
    VertexArray vertices;
    private int textureId, uTextureUnitLocation, mMVPMatrixHandle;
    int[] texturesToLoad;



    public Skybox (int vertexShader, int fragmentShader, int[] texturesToLoad)
    {
        //vertices of cube
        vertices = new VertexArray(new float []
                {
                        -1, 1, 1,
                        1, 1, 1,
                        -1, -1, 1,
                        1, -1, 1,
                        -1, 1, -1,
                        1, 1, -1,
                        -1, -1, -1,
                        1, -1, -1,
                });

        //index array detailing the vertices as triangle faces
        indices = ByteBuffer.allocateDirect(6*6)
                .put(new byte[]{
                        //front face
                        1, 3, 0,
                        0, 3, 2,

                        //back face
                        4, 6, 5,
                        5, 6, 7,

                        //left face
                        0, 2, 4,
                        4, 2, 6,

                        //right face
                        5, 7, 1,
                        1, 7, 3,

                        //top face
                        5, 1, 4,
                        4, 1, 0,

                        //bottom face
                        6, 2, 7,
                        7, 2, 3
                });

        mShader = new Shader(GameEngineSurfaceView.context, vertexShader, fragmentShader, "Skybox");
        orientation = new Orientation();
        mModelViewMatrix=new float[16];
        mMVPMatrix=new float[16];
        mNormalMatrix=new float[16];

        textureId = loadCubeMap(texturesToLoad);
    }

    public void draw(float[] viewMatrix, float [] projectionMatrix){
        Matrix.setIdentityM(viewMatrix, 0);

        viewProjectionMatrix = new float[16];
        Matrix.multiplyMM(viewProjectionMatrix, 0, projectionMatrix, 0, viewMatrix, 0);



        useTexture(mShader.getShaderProgram());
        mShader.ActivateShader();
        indices.position(0);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, 32, GLES20.GL_UNSIGNED_BYTE, indices);
    }

    public int loadCubeMap(int[] cubeTextures) {
        final int[] textureObjectIds = new int[1];
        GLES20.glGenTextures(1, textureObjectIds, 0);
        if (textureObjectIds[0] == 0){
            //TODO log something
            return 0;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap[] cubeMap = new Bitmap[6];

        for (int i = 0; i < 6; i++) {
            cubeMap[i] = BitmapFactory.decodeResource(GameEngineSurfaceView.context.getResources(), cubeTextures[i], options);
            if (cubeMap[i] == null){
                //TODO log something
                //resource could not be decoded
                GLES20.glDeleteTextures(1, textureObjectIds, 0);
                return 0;
            }

        }

        //set up filtering for textures
        GLES20.glBindTexture(GLES20.GL_TEXTURE_CUBE_MAP, textureObjectIds[0]);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_CUBE_MAP, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_CUBE_MAP, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        //associate each texture with the correct face of the cube
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, cubeMap[0], 0);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, cubeMap[1], 0);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, cubeMap[2], 0);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, cubeMap[3], 0);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, cubeMap[4], 0);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, cubeMap[5], 0);

        //unbind the texture
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

        for (Bitmap bitmap : cubeMap){
            bitmap.recycle();
        }

        return textureObjectIds[0];
    }

    public void useTexture(int program)
    {
        mMVPMatrixHandle = GLES20.glGetUniformLocation(program, "u_Matrix");
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, viewProjectionMatrix, 0);
        uTextureUnitLocation = GLES20.glGetUniformLocation(program, "u_TextureUnit");
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_CUBE_MAP, textureId);
        //int textureUniformHandle = GLES20.glGetUniformLocation(program, "texture1");
        GLES20.glUniform1i(uTextureUnitLocation, 0);
        vertices.setVertexAttribPointer(0, GLES20.glGetAttribLocation(program, "a_Position"), 3, 0);
        vertices.getBuffer().position(0);
        //int textureHandle = GLES20.glGetAttribLocation(program, "A_POSITION");
        //GLES20.glEnableVertexAttribArray(textureHandle);

//        GLES20.glVertexAttribPointer(textureHandle, 2, GLES20.GL_FLOAT, false, 8, textureBuffer);

    }
//    public void bindData(Shader shader, int id){
//        //GLES20.glEnableVertexAttribArray();
//    }


}



