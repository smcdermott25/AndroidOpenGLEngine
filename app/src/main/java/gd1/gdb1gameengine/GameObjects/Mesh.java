package gd1.gdb1gameengine.GameObjects;

import android.opengl.GLES20;
import android.opengl.GLES30;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by 40101817 on 24/10/2016.
 */

public class Mesh
{

    public float[] mVertices;
    public float[] mNormals;
    public float[] mTextures;

    private FloatBuffer vertexBuffer;

    // Use to access and set the view transformation
    private int mMVPMatrixHandle;
    private int mPositionHandle;
    private int vertexCount;
    private int vertexStride;


    public Mesh(float[] vertices, float[] textures, float[] normals)
    {
        this.mVertices = vertices;
        this.mTextures = textures;
        this.mNormals = normals;

        vertexCount = vertices.length / 3;
        vertexStride = 3 * 4; // 4 bytes per vertex
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (number of coordinate values * 4 bytes per float)
                vertices.length * 4);
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        // add the coordinates to the FloatBuffer
        vertexBuffer.put(vertices);
        // set the buffer to read the first coordinate
        vertexBuffer.position(0);
    }


    public void draw(float[] mvpMatrix, int program)
    {



        GLES20.glUseProgram(program);

        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(program, "vPosition");

        // Enable a handle to the meshes vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Prepare the mesh coordinate data

        GLES20.glVertexAttribPointer(mPositionHandle, 3,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);


        ////////////////////
        // get handle to shape's transformation matrix
        mMVPMatrixHandle = GLES20.glGetUniformLocation(program, "uMVPMatrix");

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);

        // Draw the mesh

        // GLES20.glPolygonOffset();
        //   glPolygonMode(GL_FRONT_AND_BACK,GL_LINE)
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);

    }

    public void drawManyInstances(float[] mvpMatrix, int program, int howMany)
    {

        GLES20.glUseProgram(program);

        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(program, "vPosition");

        // Enable a handle to the meshes vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Prepare the mesh coordinate data

        GLES20.glVertexAttribPointer(mPositionHandle, 3,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);


        ////////////////////
        // get handle to shape's transformation matrix
        mMVPMatrixHandle = GLES20.glGetUniformLocation(program, "uMVPMatrix");

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);

        // Draw the mesh

        // GLES20.glPolygonOffset();
        //   glPolygonMode(GL_FRONT_AND_BACK,GL_LINE)
        GLES30.glDrawArraysInstanced(GLES20.GL_TRIANGLES, 0, vertexCount, howMany);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }

    public void drawShadow(int positionAttribute)
    {
        // Pass in the position information
        GLES20.glVertexAttribPointer(positionAttribute, 3, GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        GLES20.glEnableVertexAttribArray(positionAttribute);

        // Draw the cube.
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 36);
    }

    public FloatBuffer getVertexBuffer()
    {
        return vertexBuffer;
    }

    public int getVertexStride()
    {
        return vertexStride;
    }


}