package gd1.gdb1gameengine.GameObjects.InstancedObjects;

import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.Texture;
import gd1.gdb1gameengine.R;

/**
 * Created by rjlfi on 28/11/2016.
 */

public class Ground extends GameObject {

    private boolean draw;
    private int groundTexture;

    public Ground(int groundTexture)
    {
        // Pass in shader files for the triangle
        super(R.raw.texturetrianglevertex, R.raw.texturetrianglefragment, R.raw.rectangle, "Rectangle", "Ground");
        this.groundTexture = groundTexture;
        draw =false;
        loadTexture();
    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {

    }

    @Override
    public void draw(float [] viewMatrix, float[] projectionMatrix)
    {
        if(draw) {
            // Load program
            mShader.ActivateShader();

            // Change uniform variables etc

            // Calculate Matrices -> will be moved to update loop
            generateMatrices(viewMatrix, projectionMatrix);

            // Use texture
            diffTexture.useTexture(mShader.getShaderProgram());

            // Draw mesh
            mesh.draw(getMVPMatrix(), mShader.getShaderProgram());
        }

    }

    @Override
    public void loadTexture()
    {
        diffTexture = new Texture(new float[]

                {
                        0.0f, 0.0f,
                        0.0f, 4.0f,
                        4.0f, 4.0f,
                        4.0f, 4.0f,
                        4.0f, 0.0f,
                        0.0f, 0.0f
                },
                groundTexture);

    }

    public void setDraw(boolean draw) {
        this.draw = draw;
    }

}
