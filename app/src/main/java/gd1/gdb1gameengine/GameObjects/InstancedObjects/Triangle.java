
package gd1.gdb1gameengine.GameObjects.InstancedObjects;


import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.Texture;
import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.AssetManager;

import static android.opengl.GLES20.GL_COMPILE_STATUS;
import static android.opengl.GLES20.glDeleteShader;
import static android.opengl.GLES20.glGetShaderInfoLog;
import static android.opengl.GLES20.glGetShaderiv;

/**
 * Created by 40101817 on 24/10/2016.
 */
public class Triangle extends GameObject {

    public void setDraw(boolean draw) {
        this.draw = draw;
    }

    private boolean draw;
    public Triangle( )
    {
        // Pass in shader files for the triangle
        super(R.raw.texturetrianglevertex, R.raw.texturetrianglefragment, R.raw.triangle,"Triangle","Triangle");
        draw =false;
        loadTexture();
    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {

    }

    @Override
    public void draw(float [] viewMatrix, float[] projectionMatrix)
    {
        if(draw) {
            // Load program
            mShader.ActivateShader();

            // Change uniform variables etc

            // Calculate Matrices -> will be moved to update loop
            generateMatrices(viewMatrix, projectionMatrix);

            // Use texture
            diffTexture.useTexture(mShader.getShaderProgram());

            // Draw mesh
            mesh.draw(getMVPMatrix(), mShader.getShaderProgram());
        }

    }

    @Override
    public void loadTexture()
    {
        diffTexture = new Texture(new float[]

                {
                0.0f, 0.0f,
                1.0f, 0.0f,
                0.0f, 0.9f
                },
                R.drawable.texturetest);

    }

}
