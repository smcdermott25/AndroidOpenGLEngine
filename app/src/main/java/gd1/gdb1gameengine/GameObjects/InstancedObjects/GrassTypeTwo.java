package gd1.gdb1gameengine.GameObjects.InstancedObjects;

import android.opengl.GLES10Ext;
import android.opengl.GLES20;
import android.opengl.GLES30;

import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.Texture;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;

/**
 * Created by rjlfi on 28/11/2016.
 */

public class GrassTypeTwo extends GameObject
{
    private boolean draw;
    private int numberOfPatches = 256;


    public GrassTypeTwo(int fileName, int textureFile, String id)
    {
        super(R.raw.grassvertex, R.raw.grassfragment, fileName, id, "Grass");
        this.textureFile = textureFile;

        draw = false;

        // mShader.setUniformVariable("lightColor", lightColor);
        loadTexture();

    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {

    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {
        if(draw) {
            // Load program
            mShader.ActivateShader();

            // Load program
            mShader.ActivateShader();

            generateMatrices(viewMatrix, projectionMatrix);

            //  mShader.setUniformVariable("ambientStrength",0.1f);

            // Use texture
            if(mesh.mTextures!=null)
            {
                diffTexture.useTexture(mShader.getShaderProgram());
            }

            // Draw mesh
            mesh.drawManyInstances(getMVPMatrix(), mShader.getShaderProgram(), numberOfPatches);
        }

    }

    public void setDraw(boolean draw)
    {
        this.draw = draw;
    }

    @Override
    public void loadTexture()
    {
        if(mesh.mTextures!=null)
        {
            diffTexture = new Texture(mesh.mTextures,
                    textureFile);

        }

    }


}
