package gd1.gdb1gameengine.GameObjects.InstancedObjects;

import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;

/**
 * Created by Ryan on 22/11/2016.
 */

public class MeshWithoutTexture extends GameObject
{
    private Vector3D colour;
    private boolean rotate = false;

    public void setDraw(boolean draw)
    {
        this.draw = draw;
    }

    public void setColour(Vector3D colour)
    {
        this.colour = colour;
        mShader.setUniformVariable("objectColor", colour);
    }

    private boolean draw;

    public MeshWithoutTexture(int fileName, String id)
    {
        super(R.raw.trianglevertex, R.raw.testfragment, fileName, id,"UntexturedMesh");
        colour = new Vector3D(0.544f, 0.4564f, 0.4564f);

        draw = true;

        mShader.ActivateShader();
        mShader.setUniformVariable("objectColor", colour);

    }

    public void setRotate(boolean rotate)
    {
        this.rotate = rotate;
    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {
        if(rotate){
            orientation.setRotationAxis(new Vector3D(1,0,0));
            orientation.addRotation(20f);
        }

    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {

        if (draw)
        {
            // Load program
            mShader.ActivateShader();

            // Load program
            mShader.ActivateShader();

            generateMatrices(viewMatrix, projectionMatrix);

            //  mShader.setUniformVariable("ambientStrength",0.1f);

            // Draw mesh
            mesh.draw(getMVPMatrix(), mShader.getShaderProgram());
        }

    }

    @Override
    public void loadTexture()
    {

    }


}

