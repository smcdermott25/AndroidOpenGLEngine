package gd1.gdb1gameengine.GameObjects.InstancedObjects;


import android.opengl.GLES20;
import android.opengl.Matrix;

import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Vector3D;

import gd1.gdb1gameengine.R;
import gd1.gdb1gameengine.Util.AssetManager;

/**
 * Created by 40101817 on 24/10/2016.
 */
public class Cube extends GameObject
{


    private boolean draw;
    public Cube(LightSource lightSource)
    {
        // Pass in shader files for the cube -> same as triangle shaders at the minute

        super(R.raw.trianglevertex, R.raw.trianglefragment, R.raw.cube,"Cube","Cube");

        mShader.ActivateShader();

        draw =false;
        //TODO:
        // Call once instead of every draw cycle?
        mShader.setUniformVariable("ambientStrength", lightSource.getAmbientLightLevel());
        mShader.setUniformVariable("objectColor", new Vector3D(1f, 1f, 1f));
        mShader.setUniformVariable("lightColor", lightSource.getLightColor());


        //mShader.se

    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix) {

    }


    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {
        if(draw){
        // Load program
        mShader.ActivateShader();

        generateMatrices(viewMatrix,projectionMatrix);

        // Draw mesh
       mesh.draw(getMVPMatrix(), mShader.getShaderProgram());}

    }

    @Override
    public void loadTexture() {

    }
    public void setDraw(boolean draw) {
        this.draw = draw;
    }

}
