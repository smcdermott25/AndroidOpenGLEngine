package gd1.gdb1gameengine.GameObjects.InstancedObjects;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;

/**
 * Created by 40103899 on 30/10/2016.
 * Created to show ambient, diffuse and specular lighting of a cube.
 * Updated to allow multiple lights to take effect at one time.
 */

public class MultipleLitCube extends GameObject
{
    private int vertexHandle;
    private FloatBuffer vertexBuffer;

    public boolean isDraw()
    {
        return draw;
    }

    public void setDraw(boolean draw)
    {
        this.draw = draw;
    }

    private boolean draw;
    private LightSource[] lightSources;
    private TrackingCamera camera;
    private boolean rotate = false;
    private boolean move = true;


    private float[] vertices = new float[]
            {
                    // vertex positions     vertex Normals
                    -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
                    0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
                    0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
                    0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
                    -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
                    -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f,

                    -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
                    0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
                    0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
                    0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
                    -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
                    -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,

                    -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f,
                    -0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f,
                    -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f,
                    -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f,
                    -0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f,
                    -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f,

                    0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,
                    0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
                    0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
                    0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
                    0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f,
                    0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,

                    -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f,
                    0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f,
                    0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f,
                    0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f,
                    -0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f,
                    -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f,

                    -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
                    0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
                    0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
                    0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
                    -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
                    -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f


            };

    private final int vertexCount = vertices.length / 6;
    private final int vertexStride = 6 * 4; // 4 bytes per floating point number / vertex

    //TODO: do not pass light source in, just the parameters needed
    public MultipleLitCube(LightSource[] lightSources, TrackingCamera camera)
    {
        super(R.raw.multiple_lights_vertex, R.raw.multiplelights, "LightingCube");
        draw = false;

        this.lightSources = lightSources;
        this.camera = camera;

        // set up buffers
        // Initialise vertex byte buffer or shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // apace required, number of coords * 4 bytes for a float
                vertices.length * 4);

        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // Create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();

        // add the coordinates to the float buffer
        vertexBuffer.put(vertices);

        // set the position of the first variable to be read
        vertexBuffer.position(0);

        mShader.ActivateShader();
        mShader.setUniformVariable("ambientStrength", lightSources[0].getAmbientLightLevel());
        mShader.setUniformVariable("objectColour", new Vector3D(0.544f, 0.4564f, 0.4564f));
    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {

        if (rotate)
        {
            orientation.setRotationAxis(new Vector3D(1, 0, 1));
            orientation.addRotation(1f);
        }
    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {

        update(viewMatrix,projectionMatrix);
        if (draw == true)
        {
            /*
             * Update:
             * Should be in the Object Handler class, but it was not working there.
             * Placing it here for now.
             */

            for (int i = 0; i < lightSources.length; i++)
            {
                LightSource light = lightSources[i];
                Vector3D position = light.getLightPosition();
                Vector3D colour = light.getLightColor();


                    switch (i)
                    {

                        case 0:
                            if (move)
                            {
                                light.rotateRightAroundXAxis(1);
                            }
                            setLightColor(colour, "lightColours[0]");
                            updateLightPosition(position, "lightPositions[0]");
                            break;

                        case 1:
                            if (move)
                            {
                                light.rotateLeftAroundXAxis(1);
                            }
                            setLightColor(colour, "lightColours[1]");
                            updateLightPosition(position, "lightPositions[1]");
                            break;

                        case 2:
                            if (move)
                            {
                                light.rotateUpAroundYAxis(1);
                            }
                            setLightColor(colour, "lightColours[2]");
                            updateLightPosition(position, "lightPositions[2]");
                            break;

                        case 3:
                            if (move)
                            {
                                light.rotateDownAroundYAxis(1);
                            }
                            setLightColor(colour, "lightColours[3]");
                            updateLightPosition(position, "lightPositions[3]");

                }
            }

            updateCameraPosition(camera.getCameraPosition());


            // Draw
            mShader.ActivateShader();

            int modelMatrixHandle = GLES20.glGetUniformLocation(mShader.getShaderProgram(), "modelMatrix");
            GLES20.glUniformMatrix4fv(modelMatrixHandle, 1, false, getModelMatrix(), 0);

            // get handle to vertex shader's position member
            vertexHandle = GLES20.glGetAttribLocation(mShader.getShaderProgram(), "position");


            // Prepare the triangle coordinate data
            vertexBuffer.position(0);
            GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT, false,
                    vertexStride, vertexBuffer);

            GLES20.glEnableVertexAttribArray(vertexHandle);


            // Send normal data
            vertexBuffer.position(3);

            int normalHandle = GLES20.glGetAttribLocation(mShader.getShaderProgram(), "normal");
            GLES20.glEnableVertexAttribArray(normalHandle);

            GLES20.glVertexAttribPointer(normalHandle, 3, GLES20.GL_FLOAT, false,
                    vertexStride, vertexBuffer);


            // Calculate Matrices -> will be moved to update loop
            generateMatrices(viewMatrix, projectionMatrix);

            int mMVPMatrixHandle = GLES20.glGetUniformLocation(mShader.getShaderProgram(), "uMVPMatrix");

            // Pass the projection and view transformation to the shader
            GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, getMVPMatrix(), 0);

            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

            GLES20.glDisableVertexAttribArray(vertexHandle);
        }

    }

    @Override
    public void loadTexture()
    {

    }

    public void updateLightPosition(Vector3D lightPosition, String uniformName)
    {
        mShader.setUniformVariable(uniformName, lightPosition);
    }

    public void setLightColor(Vector3D lightColor, String uniformName)
    {
        mShader.ActivateShader();
        mShader.setUniformVariable(uniformName, lightColor);
    }

    public void updateCameraPosition(Vector3D cameraPosition)
    {
        // have to pass this in as lighting calculation done in world space
        mShader.setUniformVariable("viewPos", cameraPosition);
    }

    public void setRotate(boolean rotate)
    {
        this.rotate = rotate;
    }

    public void setMove(boolean move)
    {
        this.move = move;
    }

    public boolean isMove()
    {
        return move;
    }

    public boolean isRotate()
    {
        return rotate;
    }
}
