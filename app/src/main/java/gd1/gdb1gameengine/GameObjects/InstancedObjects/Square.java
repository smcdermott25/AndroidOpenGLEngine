package gd1.gdb1gameengine.GameObjects.InstancedObjects;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import java.util.ArrayList;

import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Shadow;
import gd1.gdb1gameengine.GameObjects.Texture;
import gd1.gdb1gameengine.R;

/**
 * Created by 40101817 on 24/10/2016.
 * This class was a rushed attempt to get something working for the demo
 */
public class Square extends GameObject {
    private boolean draw;
    private Shadow shadow;
    private final int DEFAULT_SHADOW_RATIO = 1;
    public static final String SHADOW_TEXTURE = "uShadowTexture";
    public static final String POSITION_ATTRIBUTE = "aPosition";
    public static final String SHADOW_X_PIXEL_OFFSET = "uxPixelOffset";
    public static final String SHADOW_Y_PIXEL_OFFSET = "uyPixelOffset";
    public static final String MVP_MATRIX_UNIFORM = "uMVPMatrix";
    public static final String MV_MATRIX_UNIFORM = "uMVMatrix";
    public static final String NORMAL_MATRIX_UNIFORM = "uNormalMatrix";
    public static final String LIGHT_POSITION_UNIFORM = "uLightPos";
    public static final String SHADOW_PROJ_MATRIX = "uShadowProjMatrix";

    private final float[] mMVMatrix = new float[16];
    private final float[] mNormalMatrix = new float[16];
    private final float[] mMVPMatrix = new float[16];
    private final float[] mLightPosInEyeSpace = new float[16];
    private final float[] mLightMvpMatrix_staticShapes = new float[16];
    private LightSource mLightSource;





    public Square(int displayWidth, int displayHeight, ArrayList<GameObject> gameObjectsThatCastShadows, LightSource lightSource) {
        // Pass in shader files for the triangle
        super(R.raw.depth_tex_v_with_shadow, R.raw.depth_tex_f_with_simple_shadow, R.raw.plane,"sqaure", "Square");
        draw = false;
        shadow = new Shadow(displayWidth,displayHeight, DEFAULT_SHADOW_RATIO, gameObjectsThatCastShadows, lightSource);
        mLightSource = lightSource;
        //loadTexture();
    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix) {

    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix) {
        if (draw) {
            //Retun until light source is created
            if (mLightSource == null)
            {
                Log.e("ShadowSquare", "Light Source not initialised");
                return;
            }
            //TODO:Call all the shadow functions
            generateMatrices(viewMatrix, projectionMatrix);
            shadow.shadowDrawPreparations();


            // Load program
            mShader.ActivateShader();

            int scene_positionAttribute = GLES20.glGetAttribLocation(mShader.getShaderProgram(), POSITION_ATTRIBUTE);
            int scene_textureUniform = GLES20.glGetUniformLocation(mShader.getShaderProgram(), SHADOW_TEXTURE);

            int scene_mapStepXUniform;
            int scene_mapStepYUniform;
            int scene_mvMatrixUniform;
            int scene_normalMatrixUniform;
            int scene_mvpMatrixUniform;
            int scene_lightPosUniform;
            int scene_schadowProjMatrixUniform;
            scene_mapStepXUniform = GLES20.glGetUniformLocation(mShader.getShaderProgram(), SHADOW_X_PIXEL_OFFSET);
            scene_mapStepYUniform = GLES20.glGetUniformLocation(mShader.getShaderProgram(), SHADOW_Y_PIXEL_OFFSET);
            scene_mvMatrixUniform = GLES20.glGetUniformLocation(mShader.getShaderProgram(), MV_MATRIX_UNIFORM);
            scene_normalMatrixUniform = GLES20.glGetUniformLocation(mShader.getShaderProgram(), NORMAL_MATRIX_UNIFORM);
            scene_mvpMatrixUniform = GLES20.glGetUniformLocation(mShader.getShaderProgram(), MVP_MATRIX_UNIFORM);
            scene_lightPosUniform = GLES20.glGetUniformLocation(mShader.getShaderProgram(), LIGHT_POSITION_UNIFORM);
            scene_schadowProjMatrixUniform = GLES20.glGetUniformLocation(mShader.getShaderProgram(), SHADOW_PROJ_MATRIX);
            //TODO:Adding my code here
            //pass stepsize to map nearby points properly to depth map texture - used in PCF algorithm
            GLES20.glUniform1f(scene_mapStepXUniform, (float) (1.0 / shadow.getShadowMapWidth()));
            GLES20.glUniform1f(scene_mapStepYUniform, (float) (1.0/ shadow.getShadowMapHeight()));

            float[] tempResultMatrix = new float[16];

            float bias[] = new float [] {
                    0.5f, 0.0f, 0.0f, 0.0f,
                    0.0f, 0.5f, 0.0f, 0.0f,
                    0.0f, 0.0f, 0.5f, 0.0f,
                    0.5f, 0.5f, 0.5f, 1.0f};

            float[] depthBiasMVP = new float[16];

            //calculate MV matrix
            Matrix.multiplyMM(tempResultMatrix, 0, viewMatrix, 0, getModelMatrix(), 0);
            System.arraycopy(tempResultMatrix, 0, mMVMatrix, 0, 16);

            //pass in MV Matrix as uniform
            GLES20.glUniformMatrix4fv(scene_mvMatrixUniform, 1, false, mMVMatrix, 0);

            //calculate Normal Matrix as uniform (invert transpose MV)
            Matrix.invertM(tempResultMatrix, 0, mMVMatrix, 0);
            Matrix.transposeM(mNormalMatrix, 0, tempResultMatrix, 0);

            //pass in Normal Matrix as uniform
            GLES20.glUniformMatrix4fv(scene_normalMatrixUniform, 1, false, mNormalMatrix, 0);

            //calculate MVP matrix
            Matrix.multiplyMM(tempResultMatrix, 0, projectionMatrix, 0, mMVMatrix, 0);
            System.arraycopy(tempResultMatrix, 0, mMVPMatrix, 0, 16);

            //pass in MVP Matrix as uniform
            GLES20.glUniformMatrix4fv(scene_mvpMatrixUniform, 1, false, mMVPMatrix, 0);
            float[] mActualLightPosition = {mLightSource.getLightPosition().x,mLightSource.getLightPosition().y, mLightSource.getLightPosition().z,0};
            Matrix.multiplyMV(mLightPosInEyeSpace, 0, viewMatrix, 0, mActualLightPosition, 0);
            //pass in light source position
            GLES20.glUniform3f(scene_lightPosUniform, mLightPosInEyeSpace[0], mLightPosInEyeSpace[1], mLightPosInEyeSpace[2]);


            Matrix.multiplyMM(depthBiasMVP, 0, bias, 0, shadow.getmLightMvpMatrix(), 0);
            System.arraycopy(depthBiasMVP, 0, shadow.getmLightMvpMatrix(), 0, 16);


            //MVP matrix that was used during depth map render
            GLES20.glUniformMatrix4fv(scene_schadowProjMatrixUniform, 1, false, shadow.getmLightMvpMatrix(), 0);


            //TODO: BORDER OF NEW CODE

            // Change uniform variables etc

            // Calculate Matrices -> will be moved to update loop

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,shadow.getRenderTextureId());
            GLES20.glUniform1i(scene_textureUniform, 0);

            // Use texture
            //texture.useTexture(mShader.getShaderProgram());

            // Draw mesh




            GLES20.glVertexAttribPointer(scene_positionAttribute, 3, GLES20.GL_FLOAT, false, mesh.getVertexStride(), mesh.getVertexBuffer());

            GLES20.glEnableVertexAttribArray(scene_positionAttribute);

            // Draw the cube.
            //GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 36);
            Log.d("Shadow", "We're probabl getting weird errors out here");



        }

    }



    @Override
    public void loadTexture() {
        /*texture = new Texture(new float[]

                {
                        0.0f, 0.0f,
                        1.0f, 0.0f,
                        0.0f, 0.9f
                },
                R.drawable.texturetest);*/

    }


    public void setDraw(boolean draw) {
        this.draw = draw;
    }

    public void setDisplayRatio(int displayWidth, int displayHeight)
    {
        shadow.setDisplayRatio(displayWidth, displayHeight);
    }
}
