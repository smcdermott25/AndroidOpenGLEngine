package gd1.gdb1gameengine.GameObjects.InstancedObjects;

import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.Texture;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;

/**
 * Created by Ryan on 03/11/2016.
 */

public class MeshWithTexture extends GameObject
{
    private Vector3D colour;
    private boolean rotate = false;
    private boolean scale = false;

    public void setDraw(boolean draw)
    {
        this.draw = draw;
    }

    private Vector3D mPrevSize;
    private int multiplier = 1;

    public void setColour(Vector3D colour)
    {
        this.colour = colour;
        mShader.setUniformVariable("objectColor", colour);
    }

    private boolean draw;

    public MeshWithTexture(int fileName, int textureFile, String id)
    {
        super(R.raw.texturetrianglevertex, R.raw.texturetrianglefragment, fileName, id, "TexturedMesh");
        colour = new Vector3D(0.544f, 0.4564f, 0.4564f);
        this.textureFile = textureFile;

        draw = false;
        mPrevSize = new Vector3D(0.08f, 0.08f, 0.08f);


        // lightPosition =  new Vector3D(0.9f, 0.5f, -1.0f);

        mShader.ActivateShader();
        mShader.setUniformVariable("objectColor", colour);

        // mShader.setUniformVariable("lightColor", lightColor);
        loadTexture();
    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {
        if (rotate)
        {

            orientation.addRotation(3f);
        }



            if (scale)
            {
                if (mPrevSize.x >= 0.08)
                {
                    multiplier = -1;
                } else if (mPrevSize.x <= 0.01)
                {
                    multiplier = 1;

                }
                Vector3D temp = new Vector3D(mPrevSize.x + (0.0005f * multiplier), mPrevSize.y + (0.0005f * multiplier), mPrevSize.z + (0.0005f * multiplier));
                getOrientation().setScale(temp);
                mPrevSize = temp;
            }





    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {

        if (draw)
        {

            // Load program
            mShader.ActivateShader();

            // Load program
            mShader.ActivateShader();

            generateMatrices(viewMatrix, projectionMatrix);

            //  mShader.setUniformVariable("ambientStrength",0.1f);

            // Use texture
            if (mesh.mTextures != null)
            {
                diffTexture.useTexture(mShader.getShaderProgram());
            }

            // Draw mesh
            mesh.draw(getMVPMatrix(), mShader.getShaderProgram());
        }

    }

    @Override
    public void loadTexture()
    {
        if (mesh.mTextures != null)
        {
            diffTexture = new Texture(mesh.mTextures,
                    textureFile);

        }

    }

    public void setRotate(boolean rotate)
    {
        this.rotate = rotate;
    }

    public boolean isRotate()
    {
        return rotate;
    }

    public boolean isScale()
    {
        return scale;
    }

    public void setScale(boolean scale)
    {

        this.scale = scale;

    }
}
