package gd1.gdb1gameengine.GameObjects.InstancedObjects;


import android.opengl.GLES20;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;


import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Texture;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;



public class PlaneNormalMap extends GameObject
{


    private int vertexHandle;
    private FloatBuffer vertexBuffer;
    private int normalHandle;

    private boolean draw;
    private LightSource lightSource;

    private Texture normalMap;

    private float[] vertices = new float[]
            {
                    // vertex positions     vertex Normals
                     -1f,-1f, -1f,    0.0f,  0.0f,  -1.0f,
                     1f,-1f, -1f,     0.0f,  0.0f,   -1.0f,
                     1f,1f, -1f,      0.0f,  0.0f,   -1.0f,
                     1f,1f, -1f,      0.0f,  0.0f,   -1.0f,
                    -1f,1f, -1f,      0.0f,  0.0f,  -1.0f,
                    -1f,-1f, -1f,     0.0f,  0.0f,  -1.0f,

            };

    private float[] textureCoords = new float[]
            {
                   1,3,0,
                   1,3,0,
                   1,3,0,
                   1,3,0,
                   1,3,0,
                   1,3,0


            };


    private final int vertexCount = vertices.length / 6;
    private final int vertexStride = 6 * 4; // 4 bytes per floating point number / vertex

    public PlaneNormalMap(LightSource lightSource,int textureFile, Vector3D cameraPosition)
    {
        super(R.raw.diffusevertex, R.raw.diffusefragment,"NormalMapPlane");

        this.lightSource = lightSource;

        this.textureFile=textureFile;

        // set up buffers
        // Initialise vertex byte buffer or shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // apace required, number of coords * 4 bytes for a float
                vertices.length * 4);

        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // Create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();

        // add the coordinates to the float buffer
        vertexBuffer.put(vertices);

        // set the position of the first variable to be read
        vertexBuffer.position(0);

        mShader.ActivateShader();
        //mShader.setUniformVariable("objectColor",new Vector3D(1,1,1));
        mShader.setUniformVariable("ambientStrength",0.1f);
        //updateLightPosition(lightSource.getLightPosition());
        updateCameraPosition(cameraPosition);
        loadTexture();
    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {

    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {

        if(draw==true)
        {
            mShader.ActivateShader();

            // get handle to vertex shader's position member
            vertexHandle = GLES20.glGetAttribLocation(mShader.getShaderProgram(), "position");


            // Prepare the triangle coordinate data
            vertexBuffer.position(0);
            GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT, false,
                    vertexStride, vertexBuffer);

            GLES20.glEnableVertexAttribArray(vertexHandle);


            // Send normal data
            vertexBuffer.position(3);

            normalHandle = GLES20.glGetAttribLocation(mShader.getShaderProgram(), "normal");
            GLES20.glEnableVertexAttribArray(normalHandle);

            GLES20.glVertexAttribPointer(normalHandle, 3, GLES20.GL_FLOAT, false,
                    vertexStride, vertexBuffer);


            mShader.setMatrix4Array("uMMatrix", 1, false, getModelMatrix(), 0);
            mShader.setMatrix4Array("uMVMatrix", 1, false, getModelViewMatrix(), 0);
            mShader.setMatrix4Array("uMVPMatrix", 1, false, getMVPMatrix(), 0);
            mShader.setMatrix4Array("uViewMatrix", 1, false, viewMatrix, 0);
            mShader.setMatrix4Array("uNormalMatrix", 1, false, getNormalMatrix(), 0);


            diffTexture.useTexture(mShader.getShaderProgram());
            normalMap.useTexture(mShader.getShaderProgram(),true);

            setLightColor(lightSource.getLightColor());

            updateLightPosition(lightSource.getLightPosition());
            mShader.setMatrix4Array("lightMMatrix",1,false,lightSource.getModelMatrix(),0);
            generateMatrices(viewMatrix, projectionMatrix);

            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

            GLES20.glDisableVertexAttribArray(vertexHandle);
        }

    }



    @Override
    public void loadTexture()
    {
        System.out.println("DIFFUSE TEXTURE");
        diffTexture = new Texture(textureCoords, textureFile);
        System.out.println("NORMAL TEXTURE") ;
        normalMap = new Texture(textureCoords,R.drawable.bricknormal);

    }

    public void updateLightPosition(Vector3D lightPosition)
    {
        mShader.setUniformVariable("lightPos", lightPosition);
    }

    public void setLightColor(Vector3D lightColor)
    {
        mShader.ActivateShader();
        mShader.setUniformVariable("lightColor", lightColor);
    }

    public void updateCameraPosition(Vector3D cameraPosition)
    {
        mShader.setUniformVariable("viewPos", cameraPosition);
    }


    public void setLightSource(LightSource lightSource)
    {
        this.lightSource=lightSource;
    }
    public boolean isDraw() {
        return draw;
    }

    public void setDraw(boolean draw) {
        this.draw = draw;
    }

}
