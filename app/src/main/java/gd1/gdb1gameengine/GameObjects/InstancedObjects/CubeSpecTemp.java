package gd1.gdb1gameengine.GameObjects.InstancedObjects;

import android.opengl.GLES20;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import gd1.gdb1gameengine.GameObjects.Camera;
import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.LightSource;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.R;

/**
 * Created by Shaun on 03/11/2016.
 */

public class CubeSpecTemp extends GameObject
{
    private int vertexHandle;
    private FloatBuffer vertexBuffer;

    public boolean isDraw() {
        return draw;
    }

    public void setDraw(boolean draw) {
        this.draw = draw;
    }

    private boolean draw;

    private float[] vertices= new float []
            {
                    // vertex positions     vertex Normals
                    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
                    0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
                    0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
                    0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
                    -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
                    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

                    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
                    0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
                    0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
                    0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
                    -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
                    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,

                    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
                    -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
                    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
                    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
                    -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
                    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

                    0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
                    0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
                    0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
                    0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
                    0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
                    0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

                    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
                    0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
                    0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
                    0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
                    -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
                    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

                    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
                    0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
                    0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
                    0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
                    -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
                    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f


            };

    private final int vertexCount = vertices.length / 6;
    private final int vertexStride = 6 * 4; // 4 bytes per floating point number / vertex

    public CubeSpecTemp(LightSource lightSource, Camera camera) {
        super(R.raw.specvertex, R.raw.specfragment,"SpecCube");
        draw =false;

        // set up buffers
        // Initialise vertex byte buffer or shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // apace required, number of coords * 4 bytes for a float
                vertices.length * 4);

        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // Create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();

        // add the coordinates to the float buffer
        vertexBuffer.put(vertices);

        // set the position of the first variable to be read
        vertexBuffer.position(0);

        mShader.ActivateShader();
        mShader.setUniformVariable("uAmbientStrength", lightSource.getAmbientLightLevel());
        mShader.setUniformVariable("uObjectColor", new Vector3D(0.544f, 0.4564f, 0.4564f));
        mShader.setUniformVariable("uLightColor", lightSource.getLightColor());
        mShader.setMatrix4Array("uLightMMatrix",0,false,lightSource.getModelMatrix(),0);
        mShader.setUniformVariable("uLightPos", lightSource.getLightPosition());
        mShader.setUniformVariable("uCamPosition", camera.getCameraPosition());
        mShader.setUniformVariable("uLightShininess",0.5f);
    }

    public void setLightColor(Vector3D lightColor)
    {
        mShader.ActivateShader();
        mShader.setUniformVariable("uLightColor", lightColor);
    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {

    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {
        if(draw) {
            mShader.ActivateShader();

            // get handle to vertex shader's vPosition member
            vertexHandle = GLES20.glGetAttribLocation(mShader.getShaderProgram(), "aPosition");


            // Prepare the triangle coordinate data
            vertexBuffer.position(0);
            GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT, false,
                    vertexStride, vertexBuffer);

            GLES20.glEnableVertexAttribArray(vertexHandle);


            // Send normal data
            vertexBuffer.position(3);

            int normalHandle = GLES20.glGetAttribLocation(mShader.getShaderProgram(), "aNormal");
            GLES20.glEnableVertexAttribArray(normalHandle);
            GLES20.glVertexAttribPointer(normalHandle, 3, GLES20.GL_FLOAT, false,
                    vertexStride, vertexBuffer);


            // Calculate Matrices -> will be moved to update loop

            generateMatrices(viewMatrix, projectionMatrix);
            mShader.setMatrix4Array("uMMatrix", 1, false, getModelMatrix(), 0);
            mShader.setMatrix4Array("uMVMatrix", 1, false, getModelViewMatrix(), 0);
            mShader.setMatrix4Array("uMVPMatrix", 1, false, getMVPMatrix(), 0);
            mShader.setMatrix4Array("uViewMatrix", 1, false, viewMatrix, 0);
            mShader.setMatrix4Array("uNormalMatrix", 1, false, getNormalMatrix(), 0);
            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

            GLES20.glDisableVertexAttribArray(vertexHandle);
        }

    }

    @Override
    public void loadTexture() {

    }
}