package gd1.gdb1gameengine.GameObjects.InstancedObjects;

import android.util.Log;

import gd1.gdb1gameengine.GameObjects.Camera;
import gd1.gdb1gameengine.GameObjects.GameObject;
import gd1.gdb1gameengine.GameObjects.Vector3D;

/**
 * Created by mshaw on 29/10/2016.
 */

public class TrackingCamera extends Camera {
    private float[] mTranslationMatrix;
    GameObject objectTracking;
    Vector3D offset;
    private boolean rotate = true;
    public TrackingCamera(boolean perspective, GameObject objectToTrack, Vector3D trackCameraPositionOffset, float ratio, float nearClippingPlane, float farClippingPlane){
        super(true,
                (calculateCameraPositionAfterOffset(trackCameraPositionOffset, objectToTrack.getPosition())), //Apply tanslation before passing in
                objectToTrack.getPosition(),
                objectToTrack.getUpVector(),
                ratio,
                nearClippingPlane,
                farClippingPlane
        );
        this.objectTracking = objectToTrack;
        this.offset = trackCameraPositionOffset;
    }

    /*public void trackObject(GameObject gameObjectToTrack)
    {
        super.
    }*/

    public void update()
    {

        if (objectTracking == null)
        {
            Log.e("Error", "Game object has been deleted");
            return;
        }
        super.setCameraPosition(calculateCameraPositionAfterOffset(offset, objectTracking.getPosition()));
        super.setCameraTarget(objectTracking.getPosition());

        if(rotate)
        {
            this.rotateAroundTarget(-2.5f);
        }
    }

    static public Vector3D calculateCameraPositionAfterOffset(Vector3D offset, Vector3D positionOfObjectToTrack)
    {
        return Vector3D.add(offset, positionOfObjectToTrack);
    }

    public boolean isRotate()
    {
        return rotate;
    }

    public void setRotate(boolean rotate)
    {
        this.rotate = rotate;
    }
}
