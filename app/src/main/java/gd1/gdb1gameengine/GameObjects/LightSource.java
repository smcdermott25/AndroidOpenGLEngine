package gd1.gdb1gameengine.GameObjects;


import java.util.Random;

import gd1.gdb1gameengine.R;

/**
 * Created by rjlfi on 27/10/2016.
 */

public class LightSource extends GameObject
{
    private Vector3D lightColor;

    private float ambientLightLevel = 0.1f;
    private boolean draw;
    private float rotation = 0;
    private float speedOfRotation = 0.05f;
    private boolean move = false;
    private boolean left = false;
    private Vector3D prevZ = new Vector3D(0, 0, 0);
    private Random mRandom;

    public LightSource(Vector3D color)
    {
        super(R.raw.trianglevertex, R.raw.lightfragment, R.raw.cube, "cube", "LightSource");
        draw = false;
        lightColor = color;
        orientation.setPosition(new Vector3D(0.9f, 1.2f, -1.0f));

        // this.scaleObject(new Vector3D(1f,1f,1f));
        mShader.ActivateShader();
        mShader.setUniformVariable("lightColor", lightColor);
        mRandom = new Random();
    }

    @Override
    public void update(float[] viewMatrix, float[] projectionMatrix)
    {
        if (move)
        {
            if (prevZ.y > 2.0f)
            {
                left = true;
                float var1 = mRandom.nextFloat();
                float var2 = mRandom.nextFloat();
                float var3 = mRandom.nextFloat();
                setLightColor(new Vector3D(var1,var2,var3));

            }
            if (prevZ.y < -2f)
            {
                left = false;
                float var1 = mRandom.nextFloat();
                float var2 = mRandom.nextFloat();
                float var3 = mRandom.nextFloat();
                setLightColor(new Vector3D(var1,var2,var3));
            }

            prevZ = orientation.getPosition();
            if (!left)
            {
                orientation.setPosition(new Vector3D(prevZ.x, prevZ.y + 0.01f, prevZ.z));
            } else
            {
                orientation.setPosition(new Vector3D(prevZ.x, prevZ.y - 0.01f, prevZ.z));
            }

        }

    }

    public void setMove(boolean move)
    {
        this.move = move;
    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix)
    {

        if (draw)
        {
            mShader.ActivateShader();
            mShader.setUniformVariable("lightColor", lightColor);
            // Load program
            mShader.ActivateShader();

            // Calculate Matrices -> will be moved to update loop
            generateMatrices(viewMatrix, projectionMatrix);

            // Draw mesh
            mesh.draw(getMVPMatrix(), mShader.getShaderProgram());
        }

    }

    @Override
    public void loadTexture()
    {


    }


    public float getAmbientLightLevel()
    {
        return ambientLightLevel;
    }

    public Vector3D getLightColor()
    {
        return lightColor;
    }

    public Vector3D getLightPosition()
    {
        return orientation.getPosition();
    }

    public void setLightPosition(Vector3D newPosition)
    {
        orientation.setPosition(newPosition);
    }

    public void setLightColor(Vector3D lightColor)
    {
        this.lightColor = lightColor;
        mShader.ActivateShader();
        mShader.setUniformVariable("lightColor", lightColor);
    }

    //TODO: Move these methods into the Orientation class
    public void rotateLeftAroundXAxis(float radius)
    {
        // Tied to the speed of execution at the minute - will need refactoring later
        rotation -= speedOfRotation;
        float xComponent = (float) Math.sin(rotation) * radius;
        float zComponent = (float) Math.cos(rotation) * radius;

        setLightPosition(new Vector3D(xComponent, getLightPosition().y, zComponent));
    }

    public void rotateRightAroundXAxis(float radius)
    {
        // Tied to the speed of execution at the minute - will need refactoring later
        rotation += speedOfRotation;
        float xComponent = (float) Math.sin(rotation) * radius;
        float zComponent = (float) Math.cos(rotation) * radius;

        setLightPosition(new Vector3D(xComponent, getLightPosition().y, zComponent));
    }

    public void rotateUpAroundYAxis(float radius)
    {
        // Tied to the speed of execution at the minute - will need refactoring later
        rotation += speedOfRotation;
        float yComponent = (float) Math.sin(rotation) * radius;
        float zComponent = (float) Math.cos(rotation) * radius;

        setLightPosition(new Vector3D(getLightPosition().x, 1f + yComponent, zComponent));
    }

    public void rotateDownAroundYAxis(float radius)
    {
        // Tied to the speed of execution at the minute - will need refactoring later
        rotation -= speedOfRotation;
        float yComponent = (float) Math.sin(rotation) * radius;
        float zComponent = (float) Math.cos(rotation) * radius;

        setLightPosition(new Vector3D(getLightPosition().x, 1f + yComponent, zComponent));
    }

    public void rotateDiagonallyLeft(float radius)
    {
        // Tied to the speed of execution at the minute - will need refactoring later
        rotation -= speedOfRotation;
        float yComponent = (float) Math.sin(rotation) * radius;
        float zComponent = (float) Math.cos(rotation) * radius;

        setLightPosition(new Vector3D(yComponent, yComponent, zComponent));
    }

    public void rotateDiagonallyRight(float radius)
    {
        // Tied to the speed of execution at the minute - will need refactoring later
        rotation -= speedOfRotation;
        float yComponent = (float) Math.sin(rotation) * radius;
        float zComponent = (float) Math.cos(rotation) * radius;

        setLightPosition(new Vector3D(-yComponent, yComponent, zComponent));
    }

    public boolean isDraw()
    {
        return draw;
    }

    public void setDraw(boolean draw)
    {
        this.draw = draw;
    }

    public boolean isMove()
    {
        return move;
    }
}
