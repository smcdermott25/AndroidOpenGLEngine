package gd1.gdb1gameengine.GameObjects;


import android.opengl.GLES20;
import android.opengl.Matrix;

import gd1.gdb1gameengine.CoreGameEngine.GameEngineMainActivity;
import gd1.gdb1gameengine.CoreGameEngine.GameEngineSurfaceView;
import gd1.gdb1gameengine.Util.Shader;

/**
 * Created by 40101817 on 24/10/2016.
 */
public abstract class GameObject
{

    protected Orientation orientation;
    protected Mesh mesh;
    protected Texture diffTexture;
    protected int textureFile;
    protected Shader mShader;

    protected int matrixHandle;


    //Matrices for use in the shaders
    private float[] mModelViewMatrix;
    private float[] mMVPMatrix;
    private float[] mNormalMatrix;
    private float[] mNormalMatrixInverse;

    private boolean bForDestroy;
    private String mObjectName;

    // Creates an object with a Shader and Mesh.
    public GameObject(int vertexShader, int fragmentShader, int meshRef,String meshName, String objectName)
    {
        orientation = new Orientation();
        mModelViewMatrix=new float[16];
        mMVPMatrix=new float[16];
        mNormalMatrix=new float[16];
        mObjectName=objectName;

        if(meshName!=null)
        {
            //  meshLoader = new MeshLoader();
            //   mesh = GameEngineMainActivity.manager.deployMesh(meshName);
            //   mesh = manager.deployMesh(meshName);

            mesh = GameEngineMainActivity.manager.deployMesh(meshName, meshRef);
        }
        mShader = new Shader(GameEngineSurfaceView.context, vertexShader, fragmentShader,objectName);

    }


    // Create a GameObject with a shader
    public GameObject(int vertexShader, int fragmentShader,String objectName)
    {
       this(vertexShader,fragmentShader,0,null,objectName);
    }



    //Intended to make any calculations necessary for the object, e.g. calculating matrices.
    public abstract void update(float[] viewMatrix, float[] projectionMatrix);


    // Version of draw for a game object that has a mesh etc
    public abstract void draw(float[] viewMatrix, float [] projectionMatrix);


    public abstract void loadTexture();

    public void setShader(Shader shader)
    {
        mShader=shader;
    }


    public void scaleObject(Vector3D scale)
    {
        orientation.setScale(scale);
    }



    //Translation wrapper methods
    public Orientation getOrientation() {
        return orientation;
    }


    public void translate(Vector3D vector)
    {
        orientation.setPosition(vector);
    }


    public void generateMatrices(float[] viewMatrix, float[] projectionMatrix) //should be called by update in subclasses
    {

        Matrix.multiplyMM(mModelViewMatrix,0,viewMatrix,0,orientation.getModelMatrix(),0);

        calculateNormalMatrix();

        Matrix.multiplyMM(mMVPMatrix,0,projectionMatrix,0,mModelViewMatrix,0);
    }


    private void calculateNormalMatrix()
    {

        Matrix.setIdentityM(mNormalMatrix,0);
        Matrix.invertM(mNormalMatrix,0,mModelViewMatrix,0);
        Matrix.transposeM(mNormalMatrix,0,mNormalMatrix,0);


    }
    public void forButtons( float[] orthoMatrix) //should be called by update in subclasses
    {

        Matrix.multiplyMM(mMVPMatrix,0,orthoMatrix,0,orientation.getModelMatrix(),0);

    }

    public void drawShadow(int positionAttribute)
    {
        //I may have to add a mesh.drawShadow()
        if(mesh == null)
        {
            //don't know what this will do
            return;
        }

        mesh.drawShadow(positionAttribute);

    }
    public Vector3D getPosition()
    {
        return orientation.getPosition();
    }
    public Vector3D getUpVector()
    {
        return orientation.getUpVector();
    }

    public float[] getModelMatrix()
    {
        return orientation.getModelMatrix();
    }

    public float[] getMVPMatrix()
    {
        return mMVPMatrix;
    }

    public float[] getModelViewMatrix()
    {
        return mModelViewMatrix;
    }

    public float[] getNormalMatrix()
    {
        return mNormalMatrix;
    }

    public boolean isForDestroy()
    {
        return bForDestroy;
    }



}
