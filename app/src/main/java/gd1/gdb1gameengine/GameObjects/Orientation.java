package gd1.gdb1gameengine.GameObjects;

import android.opengl.Matrix;

import java.util.Vector;

/**
 * Created by 40101817 on 24/10/2016.
 */
public class Orientation {

    private Vector3D mPosition;     // Object position
    private Vector3D mScale;        // Scalar value
    private Vector3D mForward;      // Local x
    private Vector3D mRight;        // Local z
    private Vector3D mUp;           // Local y

    private Vector3D mRotationAxis; // Axis about which rotations occur

    private float mRotationAngle;


    //Transformation matrices
    private float[] mPositionMatrix;
    private float[] mRotationMatrix;
    private float[] mScaleMatrix;
    private float[] mTempMatrix;
    private float[] mModelMatrix;


    public Orientation()
    {
        mPosition = new Vector3D(0,0,0);
        mScale = new Vector3D(0,0,0);

        mPositionMatrix = new float[16];
        mRotationMatrix = new float[16];
        mScaleMatrix = new float[16];
        mModelMatrix = new float[16];
        mTempMatrix = new float[16];

        mRotationAxis = new Vector3D(1,0,0);
        mRotationAngle = 0;

        mRight=new Vector3D(0,0,1);
        mUp=new Vector3D(0,1,0);
        mForward=new Vector3D(1,0,0);

        Matrix.setIdentityM(mModelMatrix, 0);
        Matrix.setIdentityM(mPositionMatrix, 0);
        Matrix.setIdentityM(mRotationMatrix, 0);
        Matrix.setIdentityM(mScaleMatrix, 0);
    }

    private void setPositionMatrix(Vector3D position)
    {
        Matrix.setIdentityM(mPositionMatrix,0);
        Matrix.translateM(mPositionMatrix, 0, position.x, position.y, position.z);
    }

    private void setScaleMatrix(Vector3D Scale)
    {
        Matrix.setIdentityM(mScaleMatrix, 0);
        Matrix.scaleM(mScaleMatrix, 0, Scale.x, Scale.y, Scale.z);
    }


    public void setPosition(Vector3D vector)
    {
        mPosition=vector;
        setPositionMatrix(mPosition);
        updateModelMatrix();
    }

    public void setPositionX(float x)
    {
        mPosition.x=x;
        setPositionMatrix(mPosition);
        updateModelMatrix();
    }

    public void setPositionY(float y)
    {
        mPosition.y=y;
        setPositionMatrix(mPosition);
        updateModelMatrix();
    }

    public void setPositionZ(float z)
    {
        mPosition.z=z;
        setPositionMatrix(mPosition);
        updateModelMatrix();
    }

    public void setScale(Vector3D vector)
    {
        mScale=vector;
        setScaleMatrix(mScale);
        updateModelMatrix();
    }

    public void setScaleX(float x)
    {
        mScale.x=x;
        setScaleMatrix(mScale);
        updateModelMatrix();
    }

    public void setScaleY(float y)
    {
        mScale.y=y;
        setScaleMatrix(mScale);
        updateModelMatrix();
    }

    public void setScaleZ(float z)
    {
        mScale.z=z;
        setScaleMatrix(mScale);
        updateModelMatrix();
    }


    public void setRotationAxis(Vector3D vector)
    {
        mRotationAxis=vector;
    }


    public void addRotation(float angleIncrementDegrees)
    {
        mRotationAngle += angleIncrementDegrees;

        Matrix.rotateM(mRotationMatrix, 0, angleIncrementDegrees, mRotationAxis.x, mRotationAxis.y, mRotationAxis.z);

        updateLocalAxis();
        updateModelMatrix();
    }

    public void setRotationAngle(float angleDegrees)
    {
        mRotationAngle = angleDegrees;

        Matrix.rotateM(mRotationMatrix, 0, angleDegrees, mRotationAxis.x, mRotationAxis.y, mRotationAxis.z);

        updateLocalAxis();
        updateModelMatrix();
    }


    private void updateLocalAxis()
    {

        //Recalculate local axis
        mRight=new Vector3D(0,0,1);
        mUp=new Vector3D(0,1,0);
        mForward=new Vector3D(1,0,0);

        mRight.rotateVector(mRotationAxis,mRotationAngle);
        mUp.rotateVector(mRotationAxis,mRotationAngle);
        mForward.rotateVector(mRotationAxis,mRotationAngle);


    }


    private float[] updateModelMatrix()
    {

        //Matrices are applied in the order: Scale, Rotation, Translation
        Matrix.multiplyMM(mTempMatrix, 0, mPositionMatrix, 0, mRotationMatrix, 0);

        Matrix.multiplyMM(mModelMatrix, 0, mTempMatrix, 0, mScaleMatrix, 0);

        return mModelMatrix;
    }

    public Vector3D getPosition()
    {
        return mPosition;
    }

    public Vector3D getUpVector()
    {
        return mUp;
    }

    public Vector3D getRightVector()
    {
        return mRight;
    }

    public Vector3D getForwardVector()
    {
        return mForward;
    }


    public float[] getModelMatrix()
    {
        return mModelMatrix;
    }

}
