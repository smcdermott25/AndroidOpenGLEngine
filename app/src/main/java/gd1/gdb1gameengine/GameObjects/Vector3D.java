package gd1.gdb1gameengine.GameObjects;


/**
 * Created by Shaun on 25/10/2016.
 */

public class Vector3D {
    public float x;
    public float y;
    public float z;

    // Vector3D constructor
    public Vector3D(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void normalize() // Normalises the length of the vector to 1, setting the co-ordinates to values in the range 0 - 1
    {
        float l = length();
        x = x/l;
        y = y/l;
        z = z/l;
    }

    public float length() // Calculates the magnitude of the vector
    {
        return (float)Math.sqrt(x*x+y*y+z*z);
    }

    public void set(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void set(Vector3D vec)
    {
        x=vec.x;
        y=vec.y;
        z=vec.z;
    }

    public void multiply(float scalar) // Multiplies the vector by a scale value
    {
        x*=scalar;
        y*=scalar;
        z*=scalar;
    }

    public void negate()
    {
        multiply(-1);
    }

    public float dotProduct(Vector3D vec)
    {
        return (x * vec.x) + (y * vec.y) + (z * vec.z);
    }


    public Vector3D crossProduct(Vector3D vec)
    {
        return new Vector3D((y*vec.z) - (z*vec.y),(z*vec.x) - (x*vec.z), (x*vec.y) - (y*vec.x));
    }


    public void rotateVector(Vector3D axis, float theta)
    {

        float u, v, w;

        u = axis.x;
        v = axis.y;
        w = axis.z;
        double xPrime = u * (u * x + v * y + w * z) * (1d - Math.cos(theta))
                + x * Math.cos(theta)
                + (-w * y + v * z) * Math.sin(theta);
        double yPrime = v * (u * x + v * y + w * z) * (1d - Math.cos(theta))
                + y * Math.cos(theta)
                + (w * x - u * z) * Math.sin(theta);
        double zPrime = w * (u * x + v * y + w * z) * (1d - Math.cos(theta))
                + z * Math.cos(theta)
                + (-v * x + u * y) * Math.sin(theta);

        x=(float)xPrime;

        y=(float)yPrime;

        z=(float)zPrime;

    }

    public static Vector3D add(Vector3D vec1,Vector3D vec2) // A static method for adding two Vectors
    {
        Vector3D result = new Vector3D(0,0,0);
        result.x = vec1.x + vec2.x;
        result.y = vec1.y + vec2.y;
        result.z = vec1.z + vec2.z;
        return result;
    }

}
