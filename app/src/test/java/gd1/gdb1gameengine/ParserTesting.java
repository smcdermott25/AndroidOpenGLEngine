package gd1.gdb1gameengine;

import org.junit.Test;

import java.util.ArrayList;

import gd1.gdb1gameengine.GameObjects.Mesh;
import gd1.gdb1gameengine.GameObjects.Vector3D;
import gd1.gdb1gameengine.Graphics.Parser;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

/**
 * Created by Ryan on 21/11/2016.
 */

public class ParserTesting
{

    Parser parser = new Parser();

    ArrayList<Vector3D>answers = new ArrayList<>();

    @Test
    public void meshVerticeValueCheck()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v 0.02 0.002 0.21");
        testData.add("f 1 1 1");
        Mesh meshTest = parser.parse(testData);
        assertEquals(0.02f, meshTest.mVertices[0]);

    }
    @Test
    public void meshVerticeSizeCheck()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v 0.02 0.002 0.21");
        testData.add("f 1 1 1");
        Mesh meshTest = parser.parse(testData);
        assertEquals(9, meshTest.mVertices.length);

    }

    @Test
    public void meshNormalValueCheck()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v 0.02 0.002 0.21");
        testData.add("vn 0.02 0.002 0.21");
        testData.add("vt 0.02 0.002 0.21");
        testData.add("f 1/1/1 1/1/1 1/1/1");
        Mesh meshTest = parser.parse(testData);
        assertEquals(0.21f, meshTest.mNormals[2]);

    }

    @Test
    public void meshNormalSizeCheck()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v 0.02 0.002 0.21");
        testData.add("vn 0.02 0.002 0.21");
        testData.add("vt 0.02 0.002 0.21");
        testData.add("f 1/1/1 1/1/1 1/1/1");
        Mesh meshTest = parser.parse(testData);
        assertEquals(9,meshTest.mNormals.length);

    }

    @Test
    public void meshTextureValueCheck()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v 0.01 0.02 0.03");
        testData.add("vn 0.04 0.05 0.06");
        testData.add("vt 0.07 0.08 ");
        testData.add("f 1/1/1 1/1/1 1/1/1");
        Mesh meshTest = parser.parse(testData);
        assertEquals(0.07f,meshTest.mTextures[2]);

    }

    @Test
    public void meshTextureSizeCheck()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v 0.02 0.002 0.21");
        testData.add("vn 0.02 0.002 0.21");
        testData.add("vt 0.02 0.002 ");
        testData.add("vt 0.02 0.002 ");
        testData.add("vt 0.02 0.002 ");
        testData.add("vt 0.02 0.002 ");
        testData.add("f 1/1/1 1/1/1 1/1/1");
        testData.add("f 1/1/1 1/2/1 1/1/1");
        testData.add("f 1/1/1 1/3/1 1/1/1");
        Mesh meshTest = parser.parse(testData);
        assertEquals(18,meshTest.mTextures.length);

    }
    @Test
    public void withoutNormalsData()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v .2 .3 .5");
        testData.add("f 1 1 1");
        Mesh meshTest = parser.parse(testData);
        assertNull(meshTest.mNormals);
    }
    @Test
    public void withoutTextureData()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v .2 .3 .5");
        testData.add("f 1 1 1");
        Mesh meshTest = parser.parse(testData);
        assertNull(meshTest.mTextures);
    }
    @Test (expected=IndexOutOfBoundsException.class)
    public void withoutVerticeData()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("vn .4 .4 .6 ");
        testData.add("f 1 1 1");
        parser.parse(testData);

    }
    @Test (expected=NumberFormatException.class)
    public void erronousInput()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v .4.4 .6 ");
        testData.add("f 1 1 1");
        parser.parse(testData);

    }
    @Test (expected=NumberFormatException.class)
    public void erronousFaceInfo()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v 0.4 0.4 0.6 ");
        testData.add("f 1 1");
        parser.parse(testData);

    }

    @Test
    public void faceTestingVerticeInfo()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v  0.1 0.2 0.3 ");
        testData.add("v  0.2 0.3 0.4 ");
        testData.add("v  0.5 0.6 0.7 ");
        testData.add("v  0.8 0.9 0.1 ");
        testData.add("v  0.11 0.12 0.13 ");
        testData.add("v  0.14 0.15 0.16 ");
        testData.add("v  0.17 0.18 0.19 ");
        testData.add("v  0.20 0.21 0.22 ");
        testData.add("v  0.23 0.24 0.25 ");
        testData.add("vn 0.26 0.27 0.28 ");
        testData.add("vn 0.29 0.30 0.31 ");
        testData.add("vn 0.32 0.33 0.34");
        testData.add("vn 0.35 0.36 0.37 ");
        testData.add("vn 0.38 0.39 0.40 ");
        testData.add("vt 0.41 0.42 0.43 ");
        testData.add("vt 0.44 0.45 0.46 ");
        testData.add("vt 0.47 0.48 0.49 ");
        testData.add("vt 0.50 0.51 0.52 ");

        testData.add("f 5/1/2 3/4/4 2/3/1");

        Mesh meshTest = parser.parse(testData);
        ArrayList<float []> verticeTest = new ArrayList<>();
        verticeTest.add(new float[]{0.11f, 0.12f, 0.13f, 0.5f, 0.6f, 0.7f,0.2f, 0.3f, 0.4f});
        assertEquals(9,meshTest.mVertices.length);
        for(int i =0;i<verticeTest.get(0).length;i++)
        {
            assertEquals(verticeTest.get(0)[i],meshTest.mVertices[i]);
        }
    }

    @Test
    public void faceTestingNormalInfo()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v  0.1 0.2 0.3 ");
        testData.add("v  0.2 0.3 0.4 ");
        testData.add("v  0.5 0.6 0.7 ");
        testData.add("v  0.8 0.9 0.1 ");
        testData.add("v  0.11 0.12 0.13 ");
        testData.add("v  0.14 0.15 0.16 ");
        testData.add("v  0.17 0.18 0.19 ");
        testData.add("v  0.20 0.21 0.22 ");
        testData.add("v  0.23 0.24 0.25 ");
        testData.add("vn 0.26 0.27 0.28 ");
        testData.add("vn 0.29 0.30 0.31 ");
        testData.add("vn 0.32 0.33 0.34");
        testData.add("vn 0.35 0.36 0.37 ");
        testData.add("vn 0.38 0.39 0.40 ");
        testData.add("vt 0.41 0.42 0.43 ");
        testData.add("vt 0.44 0.45 0.46 ");
        testData.add("vt 0.47 0.48 0.49 ");
        testData.add("vt 0.50 0.51 0.52 ");

        testData.add("f 5/1/2 3/4/4 2/3/1");

        Mesh meshTest = parser.parse(testData);
        ArrayList<float []> normalTest = new ArrayList<>();
        normalTest.add(new float[]{0.29f, 0.30f, 0.31f, 0.35f, 0.36f, 0.37f, 0.26f, 0.27f, 0.28f });
        assertEquals(normalTest.get(0).length,meshTest.mNormals.length);
        for(int i =0;i<normalTest.get(0).length;i++)
        {
            assertEquals(normalTest.get(0)[i],meshTest.mNormals[i]);
        }

    }

    @Test
    public void faceTestingTextureInfo()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v  0.1 0.2 0.3 ");
        testData.add("v  0.2 0.3 0.4 ");
        testData.add("v  0.5 0.6 0.7 ");
        testData.add("v  0.8 0.9 0.1 ");
        testData.add("v  0.11 0.12 0.13 ");
        testData.add("v  0.14 0.15 0.16 ");
        testData.add("v  0.17 0.18 0.19 ");
        testData.add("v  0.20 0.21 0.22 ");
        testData.add("v  0.23 0.24 0.25 ");
        testData.add("vn 0.26 0.27 0.28 ");
        testData.add("vn 0.29 0.30 0.31 ");
        testData.add("vn 0.32 0.33 0.34");
        testData.add("vn 0.35 0.36 0.37 ");
        testData.add("vn 0.38 0.39 0.40 ");
        testData.add("vt 0.41 0.42 0.43 ");
        testData.add("vt 0.44 0.45 0.46 ");
        testData.add("vt 0.47 0.48 0.49 ");
        testData.add("vt 0.50 0.51 0.52 ");

        testData.add("f 5/1/2 3/2/4 2/3/1");

        Mesh meshTest = parser.parse(testData);
        ArrayList<float []> textureTest = new ArrayList<>();
        textureTest.add(new float[]{0.41f, 0.42f, 0.44f, 0.45f, 0.47f, 0.48f});
        assertEquals(textureTest.get(0).length,meshTest.mTextures.length);
        for(int i =0;i<textureTest.get(0).length;i++)
        {
            assertEquals(textureTest.get(0)[i],meshTest.mTextures[i]);
        }

    }

    @Test
    public void testToEnsureVerticeArraySizeProportionalityToNumberOfFaces()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v  0.1 0.2 0.3 ");
        testData.add("v  0.2 0.3 0.4 ");
        testData.add("v  0.5 0.6 0.7 ");
        testData.add("v  0.8 0.9 0.1 ");
        testData.add("v  0.11 0.12 0.13 ");
        testData.add("v  0.14 0.15 0.16 ");
        testData.add("v  0.17 0.18 0.19 ");
        testData.add("v  0.20 0.21 0.22 ");
        testData.add("v  0.23 0.24 0.25 ");
        testData.add("vn 0.26 0.27 0.28 ");
        testData.add("vn 0.29 0.30 0.31 ");
        testData.add("vn 0.32 0.33 0.34");
        testData.add("vn 0.35 0.36 0.37 ");
        testData.add("vn 0.38 0.39 0.40 ");
        testData.add("vt 0.41 0.42 0.43 ");
        testData.add("vt 0.44 0.45 0.46 ");
        testData.add("vt 0.47 0.48 0.49 ");
        testData.add("vt 0.50 0.51 0.52 ");

        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");

        Mesh meshTest = parser.parse(testData);
        assertEquals(99,meshTest.mVertices.length);
    }
    @Test
    public void testToEnsureNormalArraySizeProportionalityToNumberOfFaces()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v  0.1 0.2 0.3 ");
        testData.add("v  0.2 0.3 0.4 ");
        testData.add("v  0.5 0.6 0.7 ");
        testData.add("v  0.8 0.9 0.1 ");
        testData.add("v  0.11 0.12 0.13 ");
        testData.add("v  0.14 0.15 0.16 ");
        testData.add("v  0.17 0.18 0.19 ");
        testData.add("v  0.20 0.21 0.22 ");
        testData.add("v  0.23 0.24 0.25 ");
        testData.add("vn 0.26 0.27 0.28 ");
        testData.add("vn 0.29 0.30 0.31 ");
        testData.add("vn 0.32 0.33 0.34");
        testData.add("vn 0.35 0.36 0.37 ");
        testData.add("vn 0.38 0.39 0.40 ");
        testData.add("vt 0.41 0.42 0.43 ");
        testData.add("vt 0.44 0.45 0.46 ");
        testData.add("vt 0.47 0.48 0.49 ");
        testData.add("vt 0.50 0.51 0.52 ");

        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");


        Mesh meshTest = parser.parse(testData);
        assertEquals(63,meshTest.mNormals.length);

    }
    @Test
    public void testToEnsureTextureArraySizeProportionalityToNumberOfFaces()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v  0.1 0.2 0.3 ");
        testData.add("v  0.2 0.3 0.4 ");
        testData.add("v  0.5 0.6 0.7 ");
        testData.add("v  0.8 0.9 0.1 ");
        testData.add("v  0.11 0.12 0.13 ");
        testData.add("v  0.14 0.15 0.16 ");
        testData.add("v  0.17 0.18 0.19 ");
        testData.add("v  0.20 0.21 0.22 ");
        testData.add("v  0.23 0.24 0.25 ");
        testData.add("vn 0.26 0.27 0.28 ");
        testData.add("vn 0.29 0.30 0.31 ");
        testData.add("vn 0.32 0.33 0.34");
        testData.add("vn 0.35 0.36 0.37 ");
        testData.add("vn 0.38 0.39 0.40 ");
        testData.add("vt 0.41 0.42 0.43 ");
        testData.add("vt 0.44 0.45 0.46 ");
        testData.add("vt 0.47 0.48 0.49 ");
        testData.add("vt 0.50 0.51 0.52 ");

        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        Mesh meshTest = parser.parse(testData);

        assertEquals(90,meshTest.mTextures.length);

    }
    @Test
    public void testEndOfVerticeArray()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v  0.1 0.2 0.3 ");
        testData.add("v  0.2 0.3 0.4 ");
        testData.add("v  0.5 0.6 0.7 ");
        testData.add("v  0.8 0.9 0.1 ");
        testData.add("v  0.11 0.12 0.13 ");
        testData.add("v  0.14 0.15 0.16 ");
        testData.add("v  0.17 0.18 0.19 ");
        testData.add("v  0.20 0.21 0.22 ");
        testData.add("v  0.23 0.24 0.25 ");
        testData.add("vn 0.26 0.27 0.28 ");
        testData.add("vn 0.29 0.30 0.31 ");
        testData.add("vn 0.32 0.33 0.34");
        testData.add("vn 0.35 0.36 0.37 ");
        testData.add("vn 0.38 0.39 0.40 ");
        testData.add("vt 0.41 0.42 0.43 ");
        testData.add("vt 0.44 0.45 0.46 ");
        testData.add("vt 0.47 0.48 0.49 ");
        testData.add("vt 0.50 0.51 0.52 ");

        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 1/1/2 8/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 9/3/1");

        Mesh meshTest = parser.parse(testData);
        assertEquals(0.25f,meshTest.mVertices[(meshTest.mVertices.length-1)]);

    }
    @Test
    public void testEndOfTextureArray()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v  0.1 0.2 0.3 ");
        testData.add("v  0.2 0.3 0.4 ");
        testData.add("v  0.5 0.6 0.7 ");
        testData.add("v  0.8 0.9 0.1 ");
        testData.add("v  0.11 0.12 0.13 ");
        testData.add("v  0.14 0.15 0.16 ");
        testData.add("v  0.17 0.18 0.19 ");
        testData.add("v  0.20 0.21 0.22 ");
        testData.add("v  0.23 0.24 0.25 ");
        testData.add("vn 0.26 0.27 0.28 ");
        testData.add("vn 0.29 0.30 0.31 ");
        testData.add("vn 0.32 0.33 0.34");
        testData.add("vn 0.35 0.36 0.37 ");
        testData.add("vn 0.38 0.39 0.40 ");
        testData.add("vt 0.41 0.42 0.43 ");
        testData.add("vt 0.44 0.45 0.46 ");
        testData.add("vt 0.47 0.48 0.49 ");
        testData.add("vt 0.50 0.51 0.52 ");

        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 1/1/2 8/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 9/4/1");

        Mesh meshTest = parser.parse(testData);
        assertEquals(0.51f,meshTest.mTextures[(meshTest.mTextures.length-1)]);
    }

    @Test
    public void testEndOfNormalArray()
    {
        ArrayList<String> testData = new ArrayList<>();
        testData.add("v  0.1 0.2 0.3 ");
        testData.add("v  0.2 0.3 0.4 ");
        testData.add("v  0.5 0.6 0.7 ");
        testData.add("v  0.8 0.9 0.1 ");
        testData.add("v  0.11 0.12 0.13 ");
        testData.add("v  0.14 0.15 0.16 ");
        testData.add("v  0.17 0.18 0.19 ");
        testData.add("v  0.20 0.21 0.22 ");
        testData.add("v  0.23 0.24 0.25 ");
        testData.add("vn 0.26 0.27 0.28 ");
        testData.add("vn 0.29 0.30 0.31 ");
        testData.add("vn 0.32 0.33 0.34");
        testData.add("vn 0.35 0.36 0.37 ");
        testData.add("vn 0.38 0.39 0.40 ");
        testData.add("vt 0.41 0.42 0.43 ");
        testData.add("vt 0.44 0.45 0.46 ");
        testData.add("vt 0.47 0.48 0.49 ");
        testData.add("vt 0.50 0.51 0.52 ");

        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 1/1/2 8/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 2/3/1");
        testData.add("f 5/1/2 3/4/4 9/4/5");

        Mesh meshTest = parser.parse(testData);
        assertEquals(0.40f,meshTest.mNormals[(meshTest.mNormals.length-1)]);


    }
}
