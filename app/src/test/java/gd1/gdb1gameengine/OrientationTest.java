package gd1.gdb1gameengine;

import org.junit.Test;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

import gd1.gdb1gameengine.GameObjects.Orientation;

/**
 * Created by Shaun on 22/11/2016.
 */

public class OrientationTest
{

    @Test
    public void localAxisRotationTest()
    {

        Orientation o = new Orientation();

        o.addRotation(45);

        assertEquals(o.getForwardVector().dotProduct(o.getRightVector()),1);

    }


}
